![Alt text](https://bitbucket.org/anchorwatchstudios/chaos-2.x/avatar/512/)

# Chaos 2.x #

Fractal mathematics and geometry are useful for applications in science, engineering, and art, but acquiring the tools to explore and graph fractals can be frustrating. Tools available online have limited fractals, rendering methods, and shaders. They often fail to abstract these concepts in a reusable way. This means that multiple programs and interfaces must be learned and used to fully explore the topic. Chaos is an abstract fractal geometry rendering program created to solve this problem. This application builds off previous work done by myself and others [1] to create an extensible, abstract solution to rendering fractals. 

Chaos is capable of rendering fractals with different rendering methods and shaders applied dynamically. The program is extensible so new fractals, shaders, and rendering methods can be added. These fractals can be saved to and loaded from files. Each file has keyframes and video settings. Chaos uses these keyframes and settings to produce .mp4 video using linear interpolation between keyframes. Chaos also supports exporting .png images. Fractals currently implemented include the Cantor set, Julia, Juliabulb, Mandelbox, Mandelbrot, Mandelbulb, Newton Basin, and trees. Chaos can render fractals using either Java2D or OpenGL.

[1] Tom Beddard's Fractal Lab [http://sub.blue/fractal-lab](http://sub.blue/fractal-lab)


### Setup ###

* Clone the repo
* Add the project in your IDE
* Use Maven to download dependencies
* Run the program (Chaos.java)

### Contribution guidelines ###

This is a single contributor project.

### Who do I talk to? ###

* [Anthony Atella](https://bitbucket.org/AnthonyAtella/)