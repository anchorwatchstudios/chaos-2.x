package com.anchorwatchstudios.chaos.jogl;

import com.jogamp.opengl.GL;

/**
 * An abstract vertex buffer object to be used with
 * implementations of OpenGL.
 * 
 * @author Anthony Atella
 */
public interface JOGLVBO {

	/**
	 * Initialize the VBO.
	 * 
	 * @param gl The GL context
	 */
	void init(GL gl);
	
	/**
	 * Returns the id of this VBO.
	 * 
	 * @return The id of this VBO
	 */
	int getId();
	
	/**
	 * Binds this VBO to the given GL context.
	 * 
	 * @param gl The GL context
	 */
	void bind(GL gl);
	
	/**
	 * Unbinds this VBO from the given GL context.
	 * 
	 * @param gl The GL context
	 */
	void unbind(GL gl);
	
}
