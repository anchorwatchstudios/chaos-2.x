package com.anchorwatchstudios.chaos.jogl;

import com.jogamp.opengl.GL;

/**
 * An abstract vertex array object to be used with
 * implementations of OpenGL.
 * 
 * @author Anthony Atella
 */
public interface JOGLVAO {

	/**
	 * Returns the id of this VAO.
	 * 
	 * @return The id of this VAO
	 */
	int getId();
	
	/**
	 * Binds this VAO to the given GL context.
	 * 
	 * @param gl The GL context
	 */
	void bind(GL gl);
	
	/**
	 * Unbinds this VAO from the given GL context.
	 * 
	 * @param gl The GL context
	 */
	void unbind(GL gl);
	
}
