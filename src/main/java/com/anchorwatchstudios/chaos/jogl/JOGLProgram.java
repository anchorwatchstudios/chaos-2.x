package com.anchorwatchstudios.chaos.jogl;

import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.gl.GLShader;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

/**
 * An OpenGL shader program in JOGL.
 * 
 * @author Anthony Atella
 */
public interface JOGLProgram extends GLEventListener, DocumentManagerObserver {

	/**
	 * Returns the program id.
	 * 
	 * @return The program id
	 */
	int getId();
	
	/**
	 * Attaches the given shader to this program.
	 * 
	 * @param drawable The GL context
	 * @param glShader The shader to attach
	 */
	void attachShader(GLAutoDrawable drawable, GLShader glShader);
	
	/**
	 * Links and validates this program. Returns true
	 * if successful, false otherwise.
	 * 
	 * @param drawable The GL context
	 * @return True if successful, false otherwise
	 */
	boolean linkAndValidate(GLAutoDrawable drawable);
	
	/**
	 * Returns this programs log from the given GL context.
	 * 
	 * @param drawable The GL context
	 * @return This programs log
	 */
	String getLog(GLAutoDrawable drawable);
	
	/**
	 * Reloads this program.
	 */
	void reload();
	
	/**
	 * Deletes this program from the given context.
	 * 
	 * @param drawable The GL context
	 */
	void delete(GLAutoDrawable drawable);
	
}
