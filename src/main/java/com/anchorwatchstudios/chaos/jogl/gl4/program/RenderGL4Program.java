package com.anchorwatchstudios.chaos.jogl.gl4.program;

import java.io.IOException;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.KeyFrame;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.document.shader.Color;
import com.anchorwatchstudios.chaos.document.shader.DichromaticShader;
import com.anchorwatchstudios.chaos.document.shader.MinDistanceShader;
import com.anchorwatchstudios.chaos.document.shader.MonochromaticShader;
import com.anchorwatchstudios.chaos.document.shader.RandomShader;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.GL4FloatVAO;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.GL4Shader;
import com.anchorwatchstudios.chaos.math.Vec2i;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * An abstract framework for rendering things in OpenGL4. Shaders,
 * geometry, and render settings are provided with abstract methods.
 * 
 * @author Anthony Atella
 */
public abstract class RenderGL4Program extends GL4Program {

	private static final float[] VERTICES = {
			-1.0f,  1.0f, 0.0f,   // Top left
			-1.0f, -1.0f, 0.0f,   // Bottom left
			1.0f, -1.0f, 0.0f,    // Bottom right
			1.0f,  1.0f, 0.0f     // Top right
	};
	private static final int VERTEX_SIZE = 3;
	private GL4FloatVAO positionVAO;
	private boolean reload;
	private Shader shader;
	private String shaderClass;
	private RenderMethod renderMethod;
	private Vec2i resolution;
	
	/**
	 * Returns the current shader.
	 * 
	 * @return The current shader
	 */
	protected Shader getShader() {
		
		return shader;
		
	}

	/**
	 * Returns the current render method.
	 * 
	 * @return The current render method
	 */
	protected RenderMethod getRenderMethod() {
		
		return renderMethod;
		
	}

	/**
	 * Returns the current resolution.
	 * 
	 * @return The current resolution
	 */
	protected Vec2i getResolution() {
		
		return resolution;
		
	}

	/**
	 * Passes uniform values to the shader program.
	 * 
	 * @param gl The GL4 context
	 */
	protected abstract void passUniforms(GL4 gl);
	
	/**
	 * Passes uniform values to the shader program related
	 * to the render method.
	 * 
	 * @param gl The GL4 context
	 * @param renderMethod The render method
	 */
	protected abstract void passRenderUniforms(GL4 gl, RenderMethod renderMethod);
	
	/**
	 * Passes uniforms to the shader related to color. Common for
	 * all render methods.
	 * 
	 * @param gl4 The GL4 context
	 * @param shader The shader being used
	 */
	protected void passShaderUniforms(GL4 gl4, Shader shader) {
		
		if(shader instanceof DichromaticShader) {
			
			DichromaticShader dichromatic = (DichromaticShader) shader;
			
			Color c1 = dichromatic.getColor1();
			Color c2 = dichromatic.getColor2();
			int color1 = gl4.glGetUniformLocation(getId(), "color1");
			int color2 = gl4.glGetUniformLocation(getId(), "color2");
			gl4.glUniform4f(color1, (float) c1.getRed() / 255, (float) c1.getGreen() / 255, (float) c1.getBlue() / 255, (float) c1.getAlpha() / 255);
			gl4.glUniform4f(color2, (float) c2.getRed() / 255, (float) c2.getGreen() / 255, (float) c2.getBlue() / 255, (float) c2.getAlpha() / 255);

			int inverted = gl4.glGetUniformLocation(getId(), "inverted");
			gl4.glUniform1i(inverted, boolToInt(dichromatic.isInverted()));
			
		}
		else if(shader instanceof MonochromaticShader) {
			
			MonochromaticShader mono = (MonochromaticShader) shader;
			
			Color col = mono.getColor1();
			int color1 = gl4.glGetUniformLocation(getId(), "color1");
			gl4.glUniform4f(color1, (float) col.getRed() / 255, (float) col.getGreen() / 255, (float) col.getBlue() / 255, (float) col.getAlpha() / 255);
			
			int inverted = gl4.glGetUniformLocation(getId(), "inverted");
			gl4.glUniform1i(inverted, boolToInt(mono.isInverted()));
			
		}
		else if(shader instanceof RandomShader) {
			
			RandomShader random = (RandomShader) shader;
			int inverted = gl4.glGetUniformLocation(getId(), "inverted");
			gl4.glUniform1i(inverted, boolToInt(random.isInverted()));
			
		}
		else if(shader instanceof MinDistanceShader) {
			
			MinDistanceShader artistic = (MinDistanceShader) shader;
			
			Color c1 = artistic.getColor1();
			Color c2 = artistic.getColor2();
			Color c3 = artistic.getColor3();
			int color1 = gl4.glGetUniformLocation(getId(), "color1");
			int color2 = gl4.glGetUniformLocation(getId(), "color2");
			int color3 = gl4.glGetUniformLocation(getId(), "color3");
			gl4.glUniform4f(color1, (float) c1.getRed() / 255, (float) c1.getGreen() / 255, (float) c1.getBlue() / 255, (float) c1.getAlpha() / 255);
			gl4.glUniform4f(color2, (float) c2.getRed() / 255, (float) c2.getGreen() / 255, (float) c2.getBlue() / 255, (float) c2.getAlpha() / 255);
			gl4.glUniform4f(color3, (float) c3.getRed() / 255, (float) c3.getGreen() / 255, (float) c3.getBlue() / 255, (float) c3.getAlpha() / 255);

			int inverted = gl4.glGetUniformLocation(getId(), "inverted");
			gl4.glUniform1i(inverted, boolToInt(artistic.isInverted()));
			
		}
		
	}
	
	/**
	 * Returns the shaders that this program will use.
	 * 
	 * @return The shaders this program will use
	 * @throws IOException 
	 */
	protected abstract GL4Shader[] getShaders(GLAutoDrawable drawable) throws IOException;
	
	/**
	 * Loads the necessary shaders from files.
	 * 
	 * @param drawable The GL context
	 * @throws IOException If the files cannot be found
	 */
	private void loadShaders(GLAutoDrawable drawable) throws IOException {
		
		GL4Shader[] shaders = getShaders(drawable);

		for(int i = 0;i < shaders.length;i++) {
			
			shaders[i].compile();
			attachShader(drawable, shaders[i]);
			
		}

		linkAndValidate(drawable);
		
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {

		super.init(drawable);

		try {

			loadShaders(drawable);
			
		}
		catch (IOException e) {

			e.printStackTrace();
		
		}
		
		positionVAO = new GL4FloatVAO(drawable.getGL(), getId(), "position", VERTICES, VERTEX_SIZE);

	}

	@Override
	public void display(GLAutoDrawable drawable) {

		GL gl = drawable.getGL();
		GL4 gl4 = gl.getGL4();

		// Reload if necessary
		if(reload) {

			delete(drawable);
			super.init(drawable);

			try {
			
				loadShaders(drawable);
				
			}
			catch (IOException e) {
			
				e.printStackTrace();
			
			}
			
			reload = false;
			
		}
		
		// Clear the color buffer
		gl4.glClearColor(0f, 0f, 0f, 0f);
		gl4.glClear(GL.GL_COLOR_BUFFER_BIT);

		// Use the program
		gl4.glUseProgram(getId());

		// Pass uniforms to the shader
		// Fractal
		passUniforms(gl4);
		
		// Shader
		passShaderUniforms(gl4, shader);
		
		// Render
		passRenderUniforms(gl4, renderMethod);
		
		// Draw the fullscreen quad
		positionVAO.bind(gl);
		gl4.glDrawArrays(GL4.GL_QUADS, 0, VERTICES.length / VERTEX_SIZE);
		positionVAO.unbind(gl);
		
	}
	
	@Override
	public void dispose(GLAutoDrawable drawable) {

		delete(drawable);
		
	}
	
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

	}

	@Override
	public void reload() {

		reload = true;
		
	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			KeyFrame keyframe = document.getCurrentKeyFrame();
			shader = keyframe.getShader();
			renderMethod = keyframe.getRenderMethod();
			resolution = document.getSettings().getResolution();
			String m = keyframe.getShader().toString();
			
			if(shaderClass != m) {
				
				shaderClass = m;
				reload();
				
			}	
			
		}
		
	}

}
