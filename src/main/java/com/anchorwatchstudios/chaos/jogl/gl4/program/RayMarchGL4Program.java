package com.anchorwatchstudios.chaos.jogl.gl4.program;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.RayMarch;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.math.Vec2i;
import com.anchorwatchstudios.chaos.math.Vec3d;
import com.jogamp.opengl.GL4;

/**
 * Renders geometry with raymarching.
 * 
 * @author Anthony Atella
 */
public abstract class RayMarchGL4Program extends RenderGL4Program {

	private int rayMarchIterations;

	/**
	 * Returns the current number of raymarch iterations.
	 * 
	 * @return The current number of raymarch iterations
	 */
	public int getRayMarchIterations() {
		
		return rayMarchIterations;
		
	}

	@Override
	protected void passRenderUniforms(GL4 gl4, RenderMethod rm) {

		if(rm instanceof RayMarch) {
		
			Vec2i resolution = getResolution();
			RayMarch renderMethod = (RayMarch) rm;
			int res = gl4.glGetUniformLocation(getId(), "resolution");
			gl4.glUniform2f(res, (float) resolution.getX(), (float) resolution.getY());

			double r = renderMethod.getRayThreshold();
			int rayThreshold = gl4.glGetUniformLocation(getId(), "rayThreshold");
			gl4.glUniform1f(rayThreshold, (float) r);
			
			Vec3d cOrigin = renderMethod.getCameraOrigin();
			int cameraOrigin = gl4.glGetUniformLocation(getId(), "cameraOrigin");
			gl4.glUniform3f(cameraOrigin, (float) cOrigin.getX(), (float) cOrigin.getY(), (float) cOrigin.getZ());
			
			Vec3d cUp = renderMethod.getCameraUp();
			int cameraUp = gl4.glGetUniformLocation(getId(), "cameraUp");
			gl4.glUniform3f(cameraUp, (float) cUp.getX(), (float) cUp.getY(), (float) cUp.getZ());
			
			Vec3d cLookAt = renderMethod.getCameraLookAt();
			int cameraLookAt = gl4.glGetUniformLocation(getId(), "cameraLookAt");
			gl4.glUniform3f(cameraLookAt, (float) cLookAt.getX(), (float) cLookAt.getY(), (float) cLookAt.getZ());
			
		}
		
	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
		
			RayMarch rm = (RayMarch) getRenderMethod();

			if(rm.getIterations() != rayMarchIterations) {

				rayMarchIterations = rm.getIterations();
				reload();
				
			}
				
		}
		
	}
		
}
