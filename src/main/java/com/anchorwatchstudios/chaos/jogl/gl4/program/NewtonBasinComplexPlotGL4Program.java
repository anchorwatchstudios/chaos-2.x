package com.anchorwatchstudios.chaos.jogl.gl4.program;

import java.io.IOException;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.NewtonBasin;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.GL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.NewtonBasinComplexPlotGL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.VertexGL4Shader;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Renders a Newton basin on the complex plane using OpenGL4.
 * 
 * @author Anthony Atella
 */
public class NewtonBasinComplexPlotGL4Program extends ComplexPlotGL4Program {

	private NewtonBasin newton;
	private int iterations;
	
	/**
	 * Constructs a new <code>NewtonBasinComplexPlotGL4Program</code>.
	 */
	public NewtonBasinComplexPlotGL4Program() {

		super();
		newton = new NewtonBasin();
		iterations = newton.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		Fractal fractal = document.getCurrentKeyFrame().getFractal();
		
		if(document != null && fractal instanceof NewtonBasin) {
			
			newton = (NewtonBasin) fractal;
			
			if(newton.getIterations() != iterations) {

				iterations = newton.getIterations();
				reload();
				
			}
				
		}
		
	}
	

	@Override
	protected void passUniforms(GL4 gl4) {

		int alpha = gl4.glGetUniformLocation(getId(), "alpha");
		gl4.glUniform2f(alpha, (float) newton.getAlpha().getA(), (float) newton.getAlpha().getB());
		
		int n = gl4.glGetUniformLocation(getId(), "n");
		gl4.glUniform2f(n, (float) newton.getN().getA(), (float) newton.getN().getB());
		
		int fractalThreshold = gl4.glGetUniformLocation(getId(), "fractalThreshold");
		gl4.glUniform1f(fractalThreshold, (float) newton.getThreshold());
		
	}
	

	@Override
	protected GL4Shader[] getShaders(GLAutoDrawable drawable) throws IOException {

		return new GL4Shader[] { 
				new VertexGL4Shader(drawable),
				new NewtonBasinComplexPlotGL4Shader(drawable, newton.getIterations(), getShader().toString())
		};
		
	}
	
}
