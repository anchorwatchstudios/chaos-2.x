package com.anchorwatchstudios.chaos.jogl.gl4;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import com.anchorwatchstudios.chaos.jogl.JOGLVBO;
import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;

/**
 * OpenGL4 float array implementation of <code>GLVBO</code>. Encapsulates
 * some of the boiler plate to create OGL4 VBOs.
 * 
 * @author Anthony Atella
 */
public class GL4FloatVBO implements JOGLVBO {

	private float[] data;
	private int id;
	
	/**
	 * Constructs a new <code>GL4FloatVBO</code> with the given values.
	 * 
	 * @param gl The GL context
	 * @param data The float data
	 */
	public GL4FloatVBO(GL gl, float[] data) {
		
		this.data = data;
		init(gl);
		
	}
	
	@Override
	public void init(GL gl) {

		IntBuffer buffer = ByteBuffer.allocateDirect(1 * Integer.BYTES).asIntBuffer();
		gl.glGenBuffers(1, buffer);
		id = buffer.get(0);
		FloatBuffer fb = Buffers.newDirectFloatBuffer(data);
		gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, id);
		gl.glBufferData(GL4.GL_ARRAY_BUFFER, data.length * Float.BYTES, fb, GL4.GL_STATIC_DRAW);
		
	}

	@Override
	public int getId() {

		return id;
		
	}

	@Override
	public void bind(GL gl) {

		gl.getGL4().glBindVertexArray(id);
		
	}

	@Override
	public void unbind(GL gl) {

		gl.getGL4().glBindVertexArray(0);
		
	}

}
