package com.anchorwatchstudios.chaos.jogl.gl4.shader;

import java.io.IOException;

import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Loads the shaders necessary to run the full-screen image program.
 * 
 * @author Anthony Atella
 */
public class ImageGL4Shader extends GL4Shader {

	/**
	 * Constructs a new <code>ImageGL4Shader</code>.
	 * 
	 * @param drawable The GL context
	 * @throws IOException If the shader files cannot be found
	 */
	public ImageGL4Shader(GLAutoDrawable drawable) throws IOException {
	
		super(drawable, ImageGL4Shader.class.getResourceAsStream("/shader/render/image.glsl"), GL4.GL_FRAGMENT_SHADER);
		
	}
	
}
