package com.anchorwatchstudios.chaos.jogl.gl4;

import com.anchorwatchstudios.chaos.jogl.JOGLVAO;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;

/**
 * OpenGL4 float array implementation of <code>GLVAO</code>. Encapsulates
 * some of the boiler plate to create OGL4 VAOs.
 * 
 * @author Anthony Atella
 */
public class GL4FloatVAO implements JOGLVAO {

	private int id;
	
	/**
	 * Constructs a new <code>GL4FloatVAO</code> with the given values.
	 * 
	 * @param gl The GL context
	 * @param programId The program id that this VAO will belong to
	 * @param attribute The attribute name to link to
	 * @param data The float data
	 * @param dataSize The stride of the data
	 */
	public GL4FloatVAO(GL gl, int programId, String attribute, float[] data, int dataSize) {
		
		GL4FloatVBO vbo = new GL4FloatVBO(gl, data);
		int[] buffer = new int[1];
		GL4 gl4 = gl.getGL4();
		gl4.glGenVertexArrays(1, buffer, 0);
		id = buffer[0];
		gl4.glBindVertexArray(id);
		vbo.bind(gl);
		int position = gl4.glGetAttribLocation(programId, attribute);
		gl4.glVertexAttribPointer(position, dataSize, GL4.GL_FLOAT, false, 0, 0);
		gl4.glEnableVertexAttribArray(position);
		
	}

	@Override
	public int getId() {

		return id;
		
	}

	@Override
	public void bind(GL gl) {

		gl.getGL4().glBindVertexArray(id);
		
	}

	@Override
	public void unbind(GL gl) {

		gl.getGL4().glBindVertexArray(0);

	}
	
}
