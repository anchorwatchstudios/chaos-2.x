package com.anchorwatchstudios.chaos.jogl.gl4.program;

import java.io.IOException;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Julia;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.GL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.JuliaComplexPlotGL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.VertexGL4Shader;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Renders a Julia set with OpenGL4.
 * 
 * @author Anthony Atella
 */
public class JuliaComplexPlotGL4Program extends ComplexPlotGL4Program {

	private Julia julia;
	private int iterations;
	
	/**
	 * Constructs a new <code>JuliaComplexPlotGL4Program</code>.
	 */
	public JuliaComplexPlotGL4Program() {

		super();
		julia = new Julia();
		iterations = julia.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Julia) {
				
				julia = (Julia) fractal;
				
				if(julia.getIterations() != iterations) {

					iterations = julia.getIterations();
					reload();
					
				}
				
			}
			
		}
		
	}

	@Override
	protected void passUniforms(GL4 gl) {

		int c = gl.glGetUniformLocation(getId(), "c");
		gl.glUniform2f(c, (float) julia.getC().getA(), (float) julia.getC().getB());
		
		int discrete = gl.glGetUniformLocation(getId(), "discrete");
		gl.glUniform1i(discrete, GL4Program.boolToInt(julia.getDiscrete()));
		
		int fractalThreshold = gl.glGetUniformLocation(getId(), "fractalThreshold");
		gl.glUniform1f(fractalThreshold, (float) julia.getThreshold());
		
	}
	
	@Override
	protected GL4Shader[] getShaders(GLAutoDrawable drawable) throws IOException {

		return new GL4Shader[] { 
				new VertexGL4Shader(drawable), 
				new JuliaComplexPlotGL4Shader(drawable, iterations, getShader().toString())
		};
		
	}
	
}
