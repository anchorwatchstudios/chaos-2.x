package com.anchorwatchstudios.chaos.jogl.gl4.program;

import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.math.Vec2i;
import com.jogamp.opengl.GL4;

/**
 * Renders geometry on the complex plane.
 * 
 * @author Anthony Atella
 */
public abstract class ComplexPlotGL4Program extends RenderGL4Program {
	
	@Override
	public void passRenderUniforms(GL4 gl4, RenderMethod renderMethod) {
		
		if(renderMethod instanceof ComplexPlot) {
		
			int res = gl4.glGetUniformLocation(getId(), "resolution");
			Vec2i resolution = getResolution();
			gl4.glUniform2f(res, (float) resolution.getX(), (float) resolution.getY());
			
			Complex c0 = ((ComplexPlot) renderMethod).getMin();
			Complex c1 = ((ComplexPlot) renderMethod).getMax();
			int cameraPos = gl4.glGetUniformLocation(getId(), "cameraPos");
			gl4.glUniformMatrix2fv(cameraPos, 1, false, new float[] { (float) c0.getA(), (float) c0.getB(), (float) c1.getA(), (float) c1.getB() }, 0);
			
			ComplexPlot complexPlot = (ComplexPlot) renderMethod;
			int rotation = gl4.glGetUniformLocation(getId(), "rotation");
			gl4.glUniform1f(rotation, (float) complexPlot.getRotation());
			
		}
		
	}
	
}
