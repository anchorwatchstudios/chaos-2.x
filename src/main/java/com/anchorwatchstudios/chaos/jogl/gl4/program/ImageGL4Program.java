package com.anchorwatchstudios.chaos.jogl.gl4.program;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.jogl.gl4.GL4FloatVAO;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.GL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.ImageGL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.VertexGL4Shader;
import com.anchorwatchstudios.chaos.math.Vec2i;
import com.jogamp.opengl.FBObject;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.util.awt.AWTGLReadBufferUtil;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

/**
 * Draws a full-screen quad image of the output of a given program with OpenGL4.
 * Uses a frame buffer object to draw the image first.
 * 
 * @author Anthony Atella
 *
 */
public class ImageGL4Program extends GL4Program {

	private static final float[] VERTICES = {
			-1.0f,  1.0f, 0.0f,   // Top left
			-1.0f, -1.0f, 0.0f,   // Bottom left
			1.0f, -1.0f, 0.0f,    // Bottom right
			1.0f,  1.0f, 0.0f     // Top right
	};
	private static final int VERTEX_SIZE = 3;
	private GL4FloatVAO positionVAO;
	private Texture texture;
	private Vec2i resolution;
	private Vec2i canvasSize;
	private FBObject fbo;
	private GL4Program program;
	private boolean reload;
	private BufferedImage image;
	private boolean export;
	private boolean reloadTexture;

	/**
	 * Constructs a new <code>ImageGL4Program</code> with the given
	 * sub-program.
	 * 
	 * @param program The sub-program to run
	 */
	public ImageGL4Program(GL4Program program) {

		super();
		reload = false;
		this.program = program;
		resolution = new Vec2i(800, 800);
		canvasSize = new Vec2i(800, 800);
		image = null;
		setExport(false);
		reloadTexture = false;

	}

	/**
	 * Sets the canvas size to the given dimension.
	 * 
	 * @param canvasSize The size of the canvas
	 */
	public void setCanvasSize(Vec2i canvasSize) {
		
		this.canvasSize = canvasSize;
		
	}

	/**
	 * Returns true if this program is set to export an image on the
	 * next render.
	 * 
	 * @return True if set to export
	 */
	public boolean isExport() {

		return export;

	}

	/**
	 * Sets the program to export to image on the next render.
	 * 
	 * @param export Whether or not to export
	 */
	public void setExport(boolean export) {

		this.export = export;

	}

	/**
	 * Returns the resolution.
	 * 
	 * @return The resolution
	 */
	public Vec2i getResolution() {

		return resolution;

	}

	/**
	 * Sets the resolution to the given resolution.
	 * 
	 * @param resolution The new resolution
	 */
	public void setResolution(Vec2i resolution) {

		this.resolution = resolution;

	}

	/**
	 * When export has been set to true and render has been called
	 * this will return an image of the original sized buffer (the fbo).
	 * 
	 * @return The original sized image from the frame buffer object, or null
	 * if export was never set or render was never called
	 */
	public BufferedImage getImage() {

		return image;

	}

	@Override
	public void display(GLAutoDrawable drawable) {

		GL4 gl = drawable.getGL().getGL4();

		if(program != null) {

			if(reload) {

				// Reload not necessary
				reload = false;

			}

			if(reloadTexture) {

				texture = initializeTexture(gl);
				reloadTexture = false;

			}

			if(export) {

				// Read the fbo and create an image.
				fbo.bind(gl);
				image = new AWTGLReadBufferUtil(gl.getGLProfile(), true).readPixelsToBufferedImage(gl, 0, 0, fbo.getWidth(), fbo.getHeight(), true);
				fbo.unbind(gl);
				export = false;

			}
			else {

				// Draw on the fbo with the program. Note: the 0 is super important or we can't glReadPixels. No multi-sampling!
				fbo.bind(gl);
				program.display(drawable);
				fbo.unbind(gl);

				// Clear the color buffer
				float[] rgba = Color.GRAY.getRGBComponents(null);
				gl.glClearColor(rgba[0], rgba[1], rgba[2], rgba[3]);
				gl.glClear(GL.GL_COLOR_BUFFER_BIT);

				// Use the image program to transfer the texture to the buffer
				gl.glUseProgram(getId());
				gl.glActiveTexture(GL4.GL_TEXTURE0);
				texture.bind(gl);

				// Pass uniforms to the shader
				int tex = gl.glGetUniformLocation(getId(), "tex");
				gl.glUniform1i(tex, 0);
				int r = gl.glGetUniformLocation(getId(), "resolution");
				gl.glUniform2f(r, (float) resolution.getX(), (float) resolution.getY());
				int c = gl.glGetUniformLocation(getId(), "canvasSize");
				gl.glUniform2f(c, (float) canvasSize.getX(), (float) canvasSize.getY());
				
				// Draw the fullscreen quad
				positionVAO.bind(gl);
				gl.glDrawArrays(GL4.GL_QUADS, 0, VERTICES.length / VERTEX_SIZE);
				positionVAO.unbind(gl);

			}

		}

	}
	
	@Override
	public void init(GLAutoDrawable drawable) {

		super.init(drawable);

		if(program != null) {

			program.init(drawable);

		}

		GL gl = drawable.getGL();

		try {

			GL4Shader[] shaders = new GL4Shader[] { new VertexGL4Shader(drawable), new ImageGL4Shader(drawable) };
			shaders[0].compile();
			shaders[1].compile();
			attachShader(drawable, shaders[0]);
			attachShader(drawable, shaders[1]);
			linkAndValidate(drawable);
			positionVAO = new GL4FloatVAO(gl, getId(), "position", VERTICES, VERTEX_SIZE);
			texture = initializeTexture(gl.getGL4());

		}
		catch (GLException | IOException e) {

			e.printStackTrace();

		}

	}

	@Override
	public void dispose(GLAutoDrawable drawable) {

		delete(drawable);

	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

		canvasSize = new Vec2i(width, height);
		
	}

	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Vec2i r = document.getSettings().getResolution();

			if(resolution.getX() != r.getX() || resolution.getY() != r.getY()) {

				resolution = (Vec2i) r.clone();
				reloadTexture = true;

			}

			if(program != null) {

				program.notify(documentManager);

			}
			
		}

	}

	@Override
	public void reload() {

		reload = true;

	}

	@Override
	public void delete(GLAutoDrawable drawable) {

		drawable.getGL().getGL4().glDeleteProgram(getId());

	}
	
	/**
	 * Initializes a new OpenGL4 texture.
	 * 
	 * @param gl4 The GL context
	 * @return A new <code>Texture</code>
	 */
	private Texture initializeTexture(GL4 gl4) {

		Texture t = null;
		t = AWTTextureIO.newTexture(gl4.getGLProfile(), new BufferedImage((int) resolution.getX(), (int) resolution.getY(), BufferedImage.TYPE_INT_ARGB), false);
		t.setTexParameteri(gl4, GL4.GL_TEXTURE_MIN_FILTER, GL4.GL_LINEAR);
		t.setTexParameteri(gl4, GL4.GL_TEXTURE_MAG_FILTER, GL4.GL_LINEAR);
		t.setTexParameteri(gl4, GL4.GL_TEXTURE_WRAP_S, GL4.GL_CLAMP_TO_EDGE);
		t.setTexParameteri(gl4, GL4.GL_TEXTURE_WRAP_T, GL4.GL_CLAMP_TO_EDGE);

		if(fbo != null) {

			fbo.destroy(gl4);

		}

		fbo = new FBObject();
		fbo.init(gl4, t.getWidth(), t.getHeight(), 0);
		gl4.glFramebufferTexture2D(GL4.GL_FRAMEBUFFER, GL4.GL_COLOR_ATTACHMENT0, GL4.GL_TEXTURE_2D, t.getTextureObject(), 0);
		fbo.unbind(gl4);
		return t;

	}

}
