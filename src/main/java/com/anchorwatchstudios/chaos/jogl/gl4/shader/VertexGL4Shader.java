package com.anchorwatchstudios.chaos.jogl.gl4.shader;

import java.io.IOException;

import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Loads the shaders necessary to run the default vertex program.
 * 
 * @author Anthony Atella
 */
public class VertexGL4Shader extends GL4Shader {

	/**
	 * Constructs a new <code>VertexGL4Shader</code> with the given values.
	 * 
	 * @param drawable The GL context
	 * @throws IOException If the shader files cannot be found
	 */
	public VertexGL4Shader(GLAutoDrawable drawable) throws IOException {
	
		super(drawable, VertexGL4Shader.class.getResourceAsStream("/shader/vertex/vertex.glsl"), GL4.GL_VERTEX_SHADER);
		
	}
	
}
