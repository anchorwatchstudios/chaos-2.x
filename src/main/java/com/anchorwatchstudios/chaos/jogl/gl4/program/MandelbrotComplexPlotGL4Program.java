package com.anchorwatchstudios.chaos.jogl.gl4.program;

import java.io.IOException;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbrot;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.GL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.MandelbrotComplexPlotGL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.VertexGL4Shader;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Renders a Mandelbrot on the complex plane using OpenGL4.
 * 
 * @author Anthony Atella
 */
public class MandelbrotComplexPlotGL4Program extends ComplexPlotGL4Program {

	private Mandelbrot mandelbrot;
	private int iterations;
	
	/**
	 * Constructs a new <code>MandelbrotComplexPlotGL4Program</code>.
	 */
	public MandelbrotComplexPlotGL4Program() {

		super();
		mandelbrot = new Mandelbrot();
		iterations = mandelbrot.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			mandelbrot = (Mandelbrot) document.getCurrentKeyFrame().getFractal();
			
			if(mandelbrot.getIterations() != iterations) {

				iterations = mandelbrot.getIterations();
				reload();
				
			}
			
		}
		
	}

	

	@Override
	protected void passUniforms(GL4 gl4) {

		int z = gl4.glGetUniformLocation(getId(), "z");
		gl4.glUniform2f(z, (float) mandelbrot.getZ().getA(), (float) mandelbrot.getZ().getB());
		
		int discrete = gl4.glGetUniformLocation(getId(), "discrete");
		gl4.glUniform1i(discrete, boolToInt(mandelbrot.getDiscrete()));
		
		int fractalThreshold = gl4.glGetUniformLocation(getId(), "fractalThreshold");
		gl4.glUniform1f(fractalThreshold, (float) mandelbrot.getThreshold());
		
	}
	

	@Override
	protected GL4Shader[] getShaders(GLAutoDrawable drawable) throws IOException {

		return new GL4Shader[] { 
				new VertexGL4Shader(drawable),
				new MandelbrotComplexPlotGL4Shader(drawable, mandelbrot.getIterations(), getShader().toString())
		};

	}
		
}
