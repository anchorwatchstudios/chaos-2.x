package com.anchorwatchstudios.chaos.jogl.gl4.shader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Loads the shaders necessary to run the mandelbulb program.
 * 
 * @author Anthony Atella
 */
public class MandelbulbRayMarchGL4Shader extends GL4Shader {

	/**
	 * Constructs a new <code>MandelbulbRayMarchGL4Shader</code> with the given values.
	 * 
	 * @param drawable The GL context
	 * @param iterations The number of fractal iterations
	 * @param rayMarchIterations The number of ray march iterations
	 * @param fragShaderName The unqualified name of the fragment shader
	 * @throws IOException If the shader files cannot be found
	 */
	public MandelbulbRayMarchGL4Shader(GLAutoDrawable drawable, int iterations, int rayMarchIterations, String fragShaderName) throws IOException {

		super(drawable, new String[] {  }, GL4.GL_FRAGMENT_SHADER);

		InputStream[] isa = new InputStream[] { 
				GL4Shader.class.getResourceAsStream("/shader/render/raymarch.glsl"),
				GL4Shader.class.getResourceAsStream("/shader/color/" + fragShaderName +".glsl"),
				GL4Shader.class.getResourceAsStream("/shader/math/triplex.glsl"),
				GL4Shader.class.getResourceAsStream("/shader/geom/mandelbulb.glsl")
		};
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0;i < isa.length;i++) {

			InputStreamReader isr = new InputStreamReader(isa[i]);
			BufferedReader br = new BufferedReader(isr);
			String line;

			while((line = br.readLine()) != null) {

				if(line.contains("#define FRACTAL_ITERATIONS")) {

					sb.append("#define FRACTAL_ITERATIONS " + iterations + "\n");

				}
				else if(line.contains("#define MARCH_ITERATIONS")) {
					
					sb.append("#define MARCH_ITERATIONS " + rayMarchIterations + "\n");
					
				}
				else {

					sb.append(line + "\n");

				}			

			}

			isr.close();
			br.close();

		}
		
		setProgram(new String[] { sb.toString() });

	}

}
