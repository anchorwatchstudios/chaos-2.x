package com.anchorwatchstudios.chaos.jogl.gl4.program;

import java.io.IOException;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Juliabulb;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.GL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.JuliabulbRayMarchGL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.VertexGL4Shader;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Renders a Juliabulb in quaternion space using OpenGL4.
 * 
 * @author Anthony Atella
 */
public class JuliabulbRayMarchGL4Program extends RayMarchGL4Program {

	private Juliabulb juliabulb;
	private int iterations;
	
	/**
	 * Constructs a new <code>JuliabulbRayMarchGL4Program</code>.
	 */
	public JuliabulbRayMarchGL4Program() {

		super();
		juliabulb = new Juliabulb();
		iterations = juliabulb.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Juliabulb) {
				
				juliabulb = (Juliabulb) fractal;

				if(juliabulb.getIterations() != iterations) {
					
					iterations = juliabulb.getIterations();
					reload();
					
				}
				
			}
			
		}
		
	}

	@Override
	protected GL4Shader[] getShaders(GLAutoDrawable drawable) throws IOException {

		return new GL4Shader[] {
				new VertexGL4Shader(drawable),
				new JuliabulbRayMarchGL4Shader(drawable, iterations, getRayMarchIterations(), getShader().toString())
		};

	}

	@Override
	protected void passUniforms(GL4 gl) {

		int c = gl.glGetUniformLocation(getId(), "c");
		gl.glUniform3f(c, (float) juliabulb.getC().getA(), (float) juliabulb.getC().getB(), (float) juliabulb.getC().getC());
		
		int n = gl.glGetUniformLocation(getId(), "n");
		gl.glUniform1f(n, (float) juliabulb.getN());
		
		int discrete = gl.glGetUniformLocation(getId(), "discrete");
		gl.glUniform1i(discrete, GL4Program.boolToInt(juliabulb.getDiscrete()));
		
		int fractalThreshold = gl.glGetUniformLocation(getId(), "fractalThreshold");
		gl.glUniform1f(fractalThreshold, (float) juliabulb.getThreshold());
		
	}
	
}

