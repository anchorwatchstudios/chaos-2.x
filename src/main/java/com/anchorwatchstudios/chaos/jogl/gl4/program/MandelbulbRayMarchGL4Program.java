package com.anchorwatchstudios.chaos.jogl.gl4.program;

import java.io.IOException;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbulb;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.GL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.MandelbulbRayMarchGL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.VertexGL4Shader;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Renders a Mandelbulb in quaternion space using OpenGL4.
 * 
 * @author Anthony Atella
 */
public class MandelbulbRayMarchGL4Program extends RayMarchGL4Program {

	private Mandelbulb mandelbulb;
	private int iterations;
	
	/**
	 * Constructs a new <code>MandelbulbRayMarchGL4Program</code>.
	 */
	public MandelbulbRayMarchGL4Program() {

		super();
		mandelbulb = new Mandelbulb();
		iterations = mandelbulb.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Mandelbulb) {
				
				mandelbulb = (Mandelbulb) fractal;

				if(mandelbulb.getIterations() != iterations) {
					
					iterations = mandelbulb.getIterations();
					reload();
					
				}
				
			}
			
		}
		
	}

	@Override
	protected GL4Shader[] getShaders(GLAutoDrawable drawable) throws IOException {

		return new GL4Shader[] {
				new VertexGL4Shader(drawable),
				new MandelbulbRayMarchGL4Shader(drawable, iterations, getRayMarchIterations(), getShader().toString())
		};

	}

	@Override
	protected void passUniforms(GL4 gl) {

		int z = gl.glGetUniformLocation(getId(), "z");
		gl.glUniform3f(z, (float) mandelbulb.getZ().getA(), (float) mandelbulb.getZ().getB(), (float) mandelbulb.getZ().getC());
		
		int n = gl.glGetUniformLocation(getId(), "n");
		gl.glUniform1f(n, (float) mandelbulb.getN());
		
		int discrete = gl.glGetUniformLocation(getId(), "discrete");
		gl.glUniform1i(discrete, GL4Program.boolToInt(mandelbulb.getDiscrete()));
		
		int fractalThreshold = gl.glGetUniformLocation(getId(), "fractalThreshold");
		gl.glUniform1f(fractalThreshold, (float) mandelbulb.getThreshold());
		
	}
	
}

