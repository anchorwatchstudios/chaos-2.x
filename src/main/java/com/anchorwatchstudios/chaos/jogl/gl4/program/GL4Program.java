package com.anchorwatchstudios.chaos.jogl.gl4.program;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import com.anchorwatchstudios.chaos.gl.GLShader;
import com.anchorwatchstudios.chaos.jogl.JOGLProgram;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * An abstraction of an OpenGL4 program. Contains common
 * boilerplate implementation of OpenGL4 shader program calls.
 * 
 * @author Anthony Atella
 */
public abstract class GL4Program implements JOGLProgram {

	private static final int LOG_LENGTH = 2048;
	private int id;
	
	/**
	 * Constructs a new <code>GL4Program</code> with an id of -1.
	 */
	public GL4Program() {
		
		id = -1;
		
	}
	
	/**
	 * Constructs a new <code>GL4Program</code> and attaches the given
	 * shaders.
	 * 
	 * @param drawable The GL context
	 * @param shaders The shader array
	 */
	public GL4Program(GLAutoDrawable drawable, GLShader[] shaders) {
		
		this();
		
		for(int i = 0;i < shaders.length;i++) {
			
			attachShader(drawable, shaders[i]);
			
		}
		
		init(drawable);
		
	}
	
	/**
	 * Converts a boolean to a 0 or a 1.
	 * 
	 * @param bool The boolean
	 * @return 0 if false, 1 if true
	 */
	public static int boolToInt(boolean bool) {
		
		if(bool) {
			
			return 1;
			
		}
		
		return 0;
		
	}
	
	@Override
	public int getId() {
		
		return id;
		
	}

	@Override
	public void attachShader(GLAutoDrawable drawable, GLShader glShader) {
		
		drawable.getGL().getGL4().glAttachShader(id, glShader.getId());
		
	}
	
	@Override
	public boolean linkAndValidate(GLAutoDrawable drawable) {
		
		GL4 gl = drawable.getGL().getGL4();
		gl.glLinkProgram(id);
	    gl.glValidateProgram(id);
	    int[] buffer = new int[1];
	    gl.glGetProgramiv(getId(), GL4.GL_LINK_STATUS, buffer, 0);
	    return buffer[0] == GL.GL_TRUE;
		
	}
	
	@Override
	public String getLog(GLAutoDrawable drawable) {

		ByteBuffer buffer = ByteBuffer.allocateDirect(1024 * 10);
		buffer.order(ByteOrder.nativeOrder());
		ByteBuffer tmp = ByteBuffer.allocateDirect(4);
		tmp.order(ByteOrder.nativeOrder());
		IntBuffer intBuffer = tmp.asIntBuffer();
		drawable.getGL().getGL4().glGetProgramInfoLog(getId(), LOG_LENGTH, intBuffer, buffer);
		int numBytes = intBuffer.get(0);
		byte[] bytes = new byte[numBytes];
		buffer.get(bytes);
		return new String(bytes);

	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		
		id = drawable.getGL().getGL4().glCreateProgram();
		
	}
	
	@Override
	public void delete(GLAutoDrawable drawable) {

		drawable.getGL().getGL4().glDeleteProgram(getId());
		id = -1;
		
	}
		
}
