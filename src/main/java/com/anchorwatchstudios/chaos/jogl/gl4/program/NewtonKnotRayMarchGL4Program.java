package com.anchorwatchstudios.chaos.jogl.gl4.program;

import java.io.IOException;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.NewtonKnot;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.GL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.NewtonKnotRayMarchGL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.VertexGL4Shader;
import com.anchorwatchstudios.chaos.math.Triplex;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Renders a Mandelbox in quaternion space using OpenGL4.
 * 
 * @author Anthony Atella
 */
public class NewtonKnotRayMarchGL4Program extends RayMarchGL4Program {

	private NewtonKnot newton;
	private int iterations;
	
	/**
	 * Constructs a new <code>MandelboxRayMarchGL4Program</code>.
	 */
	public NewtonKnotRayMarchGL4Program() {

		super();
		newton = new NewtonKnot();
		iterations = newton.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof NewtonKnot) {
				
				newton = (NewtonKnot) fractal;

				if(newton.getIterations() != iterations) {
					
					iterations = newton.getIterations();
					reload();
					
				}
				
			}
			
		}
		
	}

	@Override
	protected GL4Shader[] getShaders(GLAutoDrawable drawable) throws IOException {

		return new GL4Shader[] {
				new VertexGL4Shader(drawable),
				new NewtonKnotRayMarchGL4Shader(drawable, iterations, getRayMarchIterations(), getShader().toString())
		};

	}

	@Override
	protected void passUniforms(GL4 gl) {

		int fractalThreshold = gl.glGetUniformLocation(getId(), "fractalThreshold");
		gl.glUniform1f(fractalThreshold, (float) newton.getThreshold());
		
		int a = gl.glGetUniformLocation(getId(), "alpha");
		Triplex alpha = newton.getAlpha();
		gl.glUniform3f(a, (float) alpha.getA(), (float) alpha.getB(), (float) alpha.getC());
		
		int n = gl.glGetUniformLocation(getId(), "n");
		Triplex pow = newton.getN();
		gl.glUniform3f(n, (float) pow.getA(), (float) pow.getB(), (float) pow.getC());
		
	}
	
}
