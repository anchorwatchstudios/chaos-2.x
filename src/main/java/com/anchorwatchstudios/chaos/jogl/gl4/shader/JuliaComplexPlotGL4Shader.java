package com.anchorwatchstudios.chaos.jogl.gl4.shader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Loads the shaders necessary to run the julia program.
 * 
 * @author Anthony Atella
 */
public class JuliaComplexPlotGL4Shader extends GL4Shader {

	/**
	 * Constructs a new <code>JuliaComplexPlotGL4Shader</code> with the given values.
	 * 
	 * @param drawable The GL context
	 * @param iterations The number of fractal iterations
	 * @param fragShaderName The unqualified name of the fragment shader
	 * @throws IOException If the shader files cannot be found
	 */
	public JuliaComplexPlotGL4Shader(GLAutoDrawable drawable, int iterations, String fragShaderName) throws IOException {
	
		super(drawable, new InputStream[] {
				GL4Shader.class.getResourceAsStream("/shader/render/complex-plot.glsl"),
				GL4Shader.class.getResourceAsStream("/shader/math/complex.glsl"),
				GL4Shader.class.getResourceAsStream("/shader/color/" + fragShaderName +".glsl")
		}, GL4.GL_FRAGMENT_SHADER);
		InputStreamReader isr = new InputStreamReader(GL4Shader.class.getResourceAsStream("/shader/geom/julia.glsl"));
		BufferedReader br = new BufferedReader(isr);
		StringBuilder sb = new StringBuilder();
		String line;

		while((line = br.readLine()) != null) {

			if(line.contains("#define FRACTAL_ITERATIONS")) {
			
				sb.append("#define FRACTAL_ITERATIONS " + iterations + "\n");
				
			}
			else {
				
				sb.append(line + "\n");

			}			

		}
		
		isr.close();
		br.close();
		
		setProgram(new String[] { getProgram()[0] + sb.toString() });

	}
	
}
