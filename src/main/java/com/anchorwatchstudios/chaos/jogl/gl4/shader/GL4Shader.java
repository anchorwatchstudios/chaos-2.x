package com.anchorwatchstudios.chaos.jogl.gl4.shader;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import com.anchorwatchstudios.chaos.gl.SimpleGLShader;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * An OpenGL4 implementation of <code>SimpleShader</code>. Handles
 * the boilerplate involved in OpenGL4 shader programs.
 * 
 * @author Anthony Atella
 */
public class GL4Shader extends SimpleGLShader {

	private static final int LOG_LENGTH = 2048;
	private GL4 gl;
	private int type;

	/**
	 * Constructs a new <code>GL4Shader</code> with the given values.
	 * 
	 * @param drawable The GL context
	 * @param program The program string array
	 * @param type GL shader type. Possible values are GL4.GL_FRAGMENT_SHADER,
	 * GL4.GL_VERTEX_SHADER ... etc
	 */
	public GL4Shader(GLAutoDrawable drawable, String[] program, int type) {

		super(program);
		this.type = type;
		this.gl = drawable.getGL().getGL4();

	}

	/**
	 * Constructs a new <code>GL4Shader</code> with the given values.
	 * 
	 * @param drawable The GL context
	 * @param is The input stream
	 * @param type GL shader type. Possible values are GL4.GL_FRAGMENT_SHADER,
	 * GL4.GL_VERTEX_SHADER ... etc
	 * @throws IOException If an IO error occurs
	 */
	public GL4Shader(GLAutoDrawable drawable, InputStream is, int type) throws IOException {

		super(is);
		this.type = type;
		this.gl = drawable.getGL().getGL4();

	}

	/**
	 * Constructs a new <code>GL4Shader</code> with the given values.
	 * 
	 * @param drawable The GL context
	 * @param is the input stream array
	 * @param type GL shader type. Possible values are GL4.GL_FRAGMENT_SHADER,
	 * GL4.GL_VERTEX_SHADER ... etc
	 * @throws IOException if an IO error occurs
	 */
	public GL4Shader(GLAutoDrawable drawable, InputStream[] is, int type) throws IOException {
		
		super(is);
		this.type = type;
		this.gl = drawable.getGL().getGL4();

	}

	/**
	 * Returns the GL4 shader type.
	 * 
	 * @return The GL4 shader type
	 */
	public int getType() {

		return type;

	}

	/**
	 * Sets the GL4 shader type.
	 * 
	 * @param type The new GL4 shader type
	 */
	public void setType(int type) {

		this.type = type;

	}

	@Override
	public boolean compile() {

		setId(gl.glCreateShader(type));
		gl.glShaderSource(getId(), getProgram().length, getProgram(), new int[] { getProgram()[0].length() }, 0);
		gl.glCompileShader(getId());
		int[] buffer = new int[1];
		gl.glGetShaderiv(getId(), GL4.GL_COMPILE_STATUS, buffer, 0);
		return buffer[0] == GL.GL_TRUE;

	}

	@Override
	public String getLog() {

		ByteBuffer buffer = ByteBuffer.allocateDirect(1024 * 10);
		buffer.order(ByteOrder.nativeOrder());
		ByteBuffer tmp = ByteBuffer.allocateDirect(4);
		tmp.order(ByteOrder.nativeOrder());
		IntBuffer intBuffer = tmp.asIntBuffer();
		gl.glGetShaderInfoLog(getId(), LOG_LENGTH, intBuffer, buffer);
		int numBytes = intBuffer.get(0);
		byte[] bytes = new byte[numBytes];
		buffer.get(bytes);
		return new String(bytes);

	}

}
