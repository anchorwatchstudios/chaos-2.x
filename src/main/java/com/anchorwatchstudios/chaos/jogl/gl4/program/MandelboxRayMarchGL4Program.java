package com.anchorwatchstudios.chaos.jogl.gl4.program;

import java.io.IOException;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbox;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.GL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.MandelboxRayMarchGL4Shader;
import com.anchorwatchstudios.chaos.jogl.gl4.shader.VertexGL4Shader;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Renders a Mandelbox in quaternion space using OpenGL4.
 * 
 * @author Anthony Atella
 */
public class MandelboxRayMarchGL4Program extends RayMarchGL4Program {

	private Mandelbox mandelBox;
	private int iterations;
	
	/**
	 * Constructs a new <code>MandelboxRayMarchGL4Program</code>.
	 */
	public MandelboxRayMarchGL4Program() {

		super();
		mandelBox = new Mandelbox();
		iterations = mandelBox.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Mandelbox) {
				
				mandelBox = (Mandelbox) fractal;

				if(mandelBox.getIterations() != iterations) {
					
					iterations = mandelBox.getIterations();
					reload();
					
				}
				
			}
			
		}
		
	}

	@Override
	protected GL4Shader[] getShaders(GLAutoDrawable drawable) throws IOException {

		return new GL4Shader[] {
				new VertexGL4Shader(drawable),
				new MandelboxRayMarchGL4Shader(drawable, iterations, getRayMarchIterations(), getShader().toString())
		};

	}

	@Override
	protected void passUniforms(GL4 gl) {

		int fractalThreshold = gl.glGetUniformLocation(getId(), "fractalThreshold");
		gl.glUniform1f(fractalThreshold, (float) mandelBox.getThreshold());
		
		int foldLimit = gl.glGetUniformLocation(getId(), "foldLimit");
		gl.glUniform1f(foldLimit, (float) mandelBox.getFoldLimit());
		
		int minRadius = gl.glGetUniformLocation(getId(), "minRadius");
		gl.glUniform1f(minRadius, (float) mandelBox.getMinRadius());
		
		int fixedRadius = gl.glGetUniformLocation(getId(), "fixedRadius");
		gl.glUniform1f(fixedRadius, (float) mandelBox.getFixedRadius());
		
		int scale = gl.glGetUniformLocation(getId(), "scale");
		gl.glUniform1f(scale, (float) mandelBox.getScale());
		
	}
	
}
