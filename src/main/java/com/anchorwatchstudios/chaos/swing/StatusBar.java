package com.anchorwatchstudios.chaos.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import com.anchorwatchstudios.chaos.task.Task;
import com.anchorwatchstudios.chaos.task.TaskManager;
import com.anchorwatchstudios.chaos.task.TaskManagerObserver;

/**
 * Displays the status and progress of the current task.
 * 
 * @author Anthony Atella
 */
public class StatusBar extends JPanel implements TaskManagerObserver {

	private static final long serialVersionUID = 1L;
	private String status;
	private JLabel statusLabel;
	private double progress;
	private JProgressBar progressBar;
	
	/**
	 * Constructs a new <code>StatusBar</code>.
	 */
	public StatusBar() {
		
		Font font = new Font("Arial", Font.PLAIN, 24);
		status = "";
		progress = 100;
		setLayout(new BorderLayout());
		Border border = BorderFactory.createBevelBorder(BevelBorder.RAISED);
		Border insets = BorderFactory.createEmptyBorder(10, 10, 10, 10);
		setBorder(BorderFactory.createCompoundBorder(border, insets));
		statusLabel = new JLabel(status);
		statusLabel.setFont(font);
		add(statusLabel, BorderLayout.WEST);
		progressBar = new JProgressBar();
		progressBar.setFont(font);
		progressBar.setForeground(new Color(0, 175, 0));
		progressBar.setPreferredSize(new Dimension(240, 24));
		setProgress(progress);
		add(progressBar, BorderLayout.EAST);
		
	}
	
	/**
	 * Returns the status.
	 * 
	 * @return The status
	 */
	public String getStatus() {
		
		return status;
		
	}
	
	/**
	 * Sets the status.
	 * 
	 * @param status The new status
	 */
	public void setStatus(String status) {
		
		this.status = status;
		statusLabel.setText(status);
		
	}
	
	/**
	 * Returns the progress in percent.
	 * 
	 * @return The progress in percent
	 */
	public double getProgress() {
		
		return progress;
		
	}
	
	/**
	 * Sets the progress.
	 * 
	 * @param progress The new progress
	 */
	public void setProgress(double progress) {
		
		this.progress = progress;
		progressBar.setValue((int) progress);
		progressBar.setString((int) progress + "%");
		
		if(progress < 100.0) {
			
			progressBar.setStringPainted(true);
			
		}
		else {
			
			progressBar.setStringPainted(false);
			
		}
		
	}

	@Override
	public void notify(TaskManager taskManager) {

		Task task = taskManager.getCurrent();
		
		if(task != null) {
		
			setProgress(task.getProgress() * 100.0);
			setStatus(task.getMessage());
			
		}
		
	}
	
}