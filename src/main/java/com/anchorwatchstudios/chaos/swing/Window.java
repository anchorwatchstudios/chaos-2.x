package com.anchorwatchstudios.chaos.swing;

import java.awt.BorderLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.swing.event.Chaos2WindowListener;

/**
 * A <code>JFrame</code> that contains all of the GUI components.
 * Capable of full-screen.
 * 
 * @author Anthony Atella
 */
public class Window extends JFrame {

	private static final int DEFAULT_WIDTH = 1400;
	private static final int DEFAULT_HEIGHT = 900;
	private static final long serialVersionUID = 1L;
	private ImageIcon icon;
	private JSplitPane splitPane;
	private Workspace workspace;
	private Timeline timeline;
	private MenuBar menuBar;
	private ToolBar toolBar;
	private StatusBar statusBar;
	private Rollout rollout;
	private DebugArea debugArea;
	private double dividerLocation;
	
	/**
	 * Constructs a new <code>Window</code>.
	 * 
	 * @param context The program context
	 */
	public Window(Chaos context) {
	
		super();
		initUI(context);
		initWindow(context);
		
	}

	/**
	 * Constructs a new <code>Window</code> with the given title.
	 * 
	 * @param context The program context
	 * @param windowTitle The window title
	 */
	public Window(Chaos context, String windowTitle) {

		super(windowTitle);
		initUI(context);
		initWindow(context);
		
	}

	/**
	 * Returns the <code>Workspace</code>.
	 * 
	 * @return The <code>Workspace</code>
	 */
	public Workspace getWorkspace() {
		
		return workspace;
		
	}
	
	/**
	 * Returns the <code>Rollout</code>.
	 * 
	 * @return The <code>Rollout</code>
	 */
	public Rollout getRollout() {
		
		return rollout;
		
	}
	
	/**
	 * Returns the <code>JSplitPane</code>.
	 * 
	 * @return The <code>JSplitPane</code>
	 */
	public JSplitPane getSplitPane() {

		return splitPane;

	}

	/**
	 * Returns the <code>JMenuBar</code>.
	 * 
	 * @return The <code>JMenuBar</code>
	 */
	public MenuBar getChaosMenuBar() {
		
		return menuBar;
		
	}
	
	/**
	 * Returns the <code>JToolBar</code>.
	 * 
	 * @return The <code>JToolBar</code>
	 */
	public ToolBar getToolBar() {
		
		return toolBar;
		
	}
	
	/**
	 * Returns the <code>JStatusBar</code>.
	 * 
	 * @return The <code>JStatusBar</code>
	 */
	public StatusBar getStatusBar() {

		return statusBar;

	}
	
	/**
	 * Initializes and returns the <code>JSplitPane</code>.
	 * 
	 * @return The <code>JSplitPane</code>
	 */
	private JSplitPane initSplitPane() {
		
		JSplitPane splitPane = new JSplitPane();
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(workspace, BorderLayout.CENTER);
		JPanel rightPanel = new JPanel(new BorderLayout());
		rightPanel.add(rollout, BorderLayout.CENTER);
		rightPanel.add(debugArea, BorderLayout.SOUTH);
		panel.add(timeline, BorderLayout.SOUTH);
		splitPane.setLeftComponent(panel);
		splitPane.setRightComponent(rightPanel);
		splitPane.setDividerLocation(dividerLocation);
		splitPane.setEnabled(false);
		return splitPane;
		
	}
	
	/**
	 * Returns the location of the <code>JSplitPane</code>
	 * divider as a percentage.
	 * 
	 * @return The location of the <code>JSplitPane</code>
	 * divider as a percentage.
	 */
	public double getDividerLocation() {
		
		return dividerLocation;
		
	}
	
	/**
	 * Sets the location of the <code>JSplitPane</code>
	 * divider.
	 * 
	 * @param dividerLocation The location of the 
	 * <code>JSplitPane</code> divider as a percentage.
	 */
	public void setDividerLocation(double dividerLocation) {
		
		this.dividerLocation = dividerLocation;
		
	}

	/**
	 * Returns the <code>Timeline</code>.
	 * 
	 * @return The <code>Timeline</code>
	 */
	public Timeline getTimeline() {

		return timeline;

	}
	
	/**
	 * Returns the <code>getDebugArea</code>.
	 * 
	 * @return The <code>getDebugArea</code>
	 */
	public DebugArea getDebugArea() {

		return debugArea;
		
	}
	
	/**
	 * Initializes the GUI.
	 * 
	 * @param context The program context
	 */
	private void initUI(Chaos context) {

		rollout = new Rollout(context);
		workspace = new Workspace(context);
		debugArea = new DebugArea(context);
		timeline = new Timeline(context);
		splitPane = initSplitPane();
		dividerLocation = 0.85;
		menuBar = new MenuBar(context);
		statusBar = new StatusBar();
		context.getMultiTaskManager().registerObserver(statusBar);
		toolBar = new ToolBar(context);
		toolBar.setOrientation(JToolBar.VERTICAL);
		add(splitPane, BorderLayout.CENTER);
		add(toolBar, BorderLayout.WEST);
		add(statusBar, BorderLayout.SOUTH);

	}
	
	/**
	 * Initializes the <code>JFrame</code>.
	 * 
	 * @param context The program context
	 */
	private void initWindow(Chaos context) {
		
		
		setTitle(Strings.WINDOW_TITLE);
		Chaos2WindowListener windowListener = new Chaos2WindowListener(context);
		addWindowListener(windowListener);
		addComponentListener(windowListener);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		setJMenuBar(menuBar);
		icon = new ImageIcon(this.getClass().getResource("/icon_64x64.png"));
		setIconImage(icon.getImage());
		setLocationRelativeTo(null);
		setVisible(true);
		
	}

	/**
	 * Returns the window icon.
	 * 
	 * @return The window icon
	 */
	public Icon getIcon() {

		return icon;
	
	}
	
}
