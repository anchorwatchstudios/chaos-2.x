package com.anchorwatchstudios.chaos.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import org.kordamp.ikonli.fontawesome.FontAwesome;
import org.kordamp.ikonli.swing.FontIcon;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.reflect.ReflectionUtils;
import com.anchorwatchstudios.chaos.swing.event.Action;

/**
 * A dockable shortcut button list.
 * 
 * @author Anthony Atella
 */
public class ToolBar extends JToolBar implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private JToggleButton debugButton;
	private Chaos context;
	private JButton exportImageButton;
	private JButton exportVideoButton;
	private JButton saveButton;
	private JPopupMenu popupMenu;

	/**
	 * Constructs a new <code>ToolBar</code>.
	 * 
	 * @param context The program context
	 */
	public ToolBar(Chaos context) {
		
		this.context = context;
		context.getDocumentManager().registerObserver(this);
		int size = 48;
		Dimension buttonSize = new Dimension(64, 64);
		Dimension separatorSize = new Dimension(size / 2, size / 2);
		
		initPopupMenu(36);
		
		FontIcon fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FILE_O);
		fontIcon.setIconSize(size);
		fontIcon.setIconColor(Color.DARK_GRAY);
		final JButton newButton = new JButton(fontIcon);
		newButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				popupMenu.show(newButton, newButton.getX() + newButton.getWidth() / 2, newButton.getY());
				
			}
			
		});
		newButton.setMinimumSize(buttonSize);
		newButton.setMaximumSize(buttonSize);
		newButton.setPreferredSize(buttonSize);
		add(newButton);
		
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FOLDER_O);
		fontIcon.setIconSize(size);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JButton button = new JButton(fontIcon);
		button.addActionListener(new Action.Open(context));
		button.setMinimumSize(buttonSize);
		button.setMaximumSize(buttonSize);
		button.setPreferredSize(buttonSize);
		add(button);
		
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FLOPPY_O);
		fontIcon.setIconSize(48);
		fontIcon.setIconColor(Color.DARK_GRAY);
		saveButton = new JButton(fontIcon);
		saveButton.addActionListener(new Action.Save(context));
		saveButton.setMinimumSize(buttonSize);
		saveButton.setMaximumSize(buttonSize);
		saveButton.setPreferredSize(buttonSize);
		add(saveButton);
		
		addSeparator(separatorSize);
		
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FILE_IMAGE_O);
		fontIcon.setIconSize(size);
		fontIcon.setIconColor(Color.DARK_GRAY);
		exportImageButton = new JButton(fontIcon);
		exportImageButton.addActionListener(new Action.ExportImage(context));
		exportImageButton.setMinimumSize(buttonSize);
		exportImageButton.setMaximumSize(buttonSize);
		exportImageButton.setPreferredSize(buttonSize);
		add(exportImageButton);
		
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FILE_VIDEO_O);
		fontIcon.setIconSize(size);
		fontIcon.setIconColor(Color.DARK_GRAY);
		exportVideoButton = new JButton(fontIcon);
		exportVideoButton.addActionListener(new Action.ExportVideo(context));
		exportVideoButton.setMinimumSize(buttonSize);
		exportVideoButton.setMaximumSize(buttonSize);
		exportVideoButton.setPreferredSize(buttonSize);
		add(exportVideoButton);
		
		addSeparator(separatorSize);
		
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.BUG);
		fontIcon.setIconSize(size);
		fontIcon.setIconColor(Color.DARK_GRAY);
		debugButton = new JToggleButton(fontIcon);
		debugButton.addActionListener(new Action.ToggleDebug(context));
		debugButton.setMinimumSize(buttonSize);
		debugButton.setMaximumSize(buttonSize);
		debugButton.setPreferredSize(buttonSize);
		add(debugButton);
		
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.GAMEPAD);
		fontIcon.setIconSize(size);
		fontIcon.setIconColor(Color.DARK_GRAY);
		button = new JButton(fontIcon);
		button.addActionListener(new Action.ShowControls(context));
		button.setMinimumSize(buttonSize);
		button.setMaximumSize(buttonSize);
		button.setPreferredSize(buttonSize);
		add(button);
		
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.QUESTION_CIRCLE);
		fontIcon.setIconSize(size);
		fontIcon.setIconColor(Color.DARK_GRAY);
		button = new JButton(fontIcon);
		button.addActionListener(new Action.ShowHelp(context));
		button.setMinimumSize(buttonSize);
		button.setMaximumSize(buttonSize);
		button.setPreferredSize(buttonSize);
		add(button);
		notify(context.getDocumentManager());
		
	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document == null) {

			saveButton.setEnabled(false);
			exportImageButton.setEnabled(false);
			exportVideoButton.setEnabled(false);

		}
		else {

			saveButton.setEnabled(true);
			exportImageButton.setEnabled(true);
			exportVideoButton.setEnabled(true);
			
			if(context.getWindow().getDebugArea().isVisible()) {
				
				debugButton.setSelected(true);
				
			}
			else {

				debugButton.setSelected(false);
				
			}
			
		}
		
	}

	/**
	 * Initializes the new fractal pop-up menu.
	 * 
	 * @param iconSize The icon size in pixels
	 */
	private void initPopupMenu(int iconSize) {
		
		popupMenu = new JPopupMenu();
		Class<?>[] fractals = (Class<?>[]) ReflectionUtils.sortByName(ReflectionUtils.getSubClasses("com.anchorwatchstudios.chaos.document.fractal", Fractal.class));
		
		for(int i = 0;i < fractals.length;i++) {
			
			FontIcon fontIcon = new FontIcon();
			fontIcon.setIkon(FontAwesome.FILE_O);
			fontIcon.setIconSize(iconSize);
			fontIcon.setIconColor(Color.DARK_GRAY);
			JMenuItem menuItem = new JMenuItem(fractals[i].getSimpleName(), fontIcon);
			
			try {
				
				menuItem.addActionListener(new Action.New(context, (Fractal) fractals[i].newInstance()));
				popupMenu.add(menuItem);
			
			}
			catch (InstantiationException | IllegalAccessException e) {
			
				// Fail silently on interfaces
				
			}
			
		}
		
	}
	
}
