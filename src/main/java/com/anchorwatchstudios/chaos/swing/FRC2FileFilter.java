package com.anchorwatchstudios.chaos.swing;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class FRC2FileFilter extends FileFilter {

	@Override
	public boolean accept(File f) {

		return f.getName().endsWith(".frc2") || f.isDirectory();
		
	}

	@Override
	public String getDescription() {

		return "Chaos file (.frc2)";
		
	}

}
