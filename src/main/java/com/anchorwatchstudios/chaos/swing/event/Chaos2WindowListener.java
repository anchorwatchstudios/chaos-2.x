package com.anchorwatchstudios.chaos.swing.event;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import com.anchorwatchstudios.chaos.swing.Chaos;
import com.anchorwatchstudios.chaos.swing.Window;

/**
 * Listens for changes in the window and resizes the <code>SplitPane</code> position
 * accordingly.
 * 
 * @author Anthony Atella
 */
public class Chaos2WindowListener implements WindowListener, ComponentListener {
	
	private Chaos context;
	
	/**
	 * Constructs a new <code>Chaos2WindowListener</code> with the given context.
	 * 
	 * @param context The program context
	 */
	public Chaos2WindowListener(Chaos context) {
		
		this.context = context;
		
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		
	}

	@Override
	public void componentResized(ComponentEvent e) {
		
		Window window = context.getWindow();
		window.getSplitPane().setDividerLocation(window.getDividerLocation());
		
	}

	@Override
	public void componentShown(ComponentEvent e) {
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		
	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {

		new Action.Exit(context).actionPerformed(null);
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		
	}

}
