package com.anchorwatchstudios.chaos.swing.event;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.swing.Chaos;

/**
 * Listens for key presses and manipulates the complex
 * plot accordingly.
 * 
 * @author Anthony Atella
 */
public class ComplexPlotKeyListener implements KeyListener {

	private static final double DEFAULT_SPEED = 0.01;
	private Chaos context;
	
	/**
	 * Constructs a new <code>ComplexPlotKeyListener</code> with the given context.
	 * 
	 * @param context The program context
	 */
	public ComplexPlotKeyListener(Chaos context) {
	
		this.context = context;
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {

		ComplexPlot settings = (ComplexPlot) ((FRC2Document) context.getDocumentManager().getCurrent()).getCurrentKeyFrame().getRenderMethod();

		switch(arg0.getKeyCode()) {
		
		case 37:
			
			// Left
			settings.pan(DEFAULT_SPEED, 0);
			break;
			
		case 38:
			
			// Up
			settings.pan(0, DEFAULT_SPEED);
			break;
			
		case 39:
			
			// Right
			settings.pan(-DEFAULT_SPEED, 0);
			break;
			
		case 40:
			
			// Down
			settings.pan(0, -DEFAULT_SPEED);
			break;
		
		}
		
		context.getDocumentManager().notifyObservers();

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}

}
