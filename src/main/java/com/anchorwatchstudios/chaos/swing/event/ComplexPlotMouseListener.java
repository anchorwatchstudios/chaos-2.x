package com.anchorwatchstudios.chaos.swing.event;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import com.anchorwatchstudios.chaos.document.Document;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.math.Vec2i;
import com.anchorwatchstudios.chaos.swing.Chaos;

/**
 * Listens for mouse events and manipulates the complex plane
 * accordingly.
 * 
 * @author Anthony Atella
 */
public class ComplexPlotMouseListener implements MouseListener, MouseMotionListener, MouseWheelListener {

	private static final double SENSITIVITY = 0.1;
	private Chaos context;
	private int oldX, oldY;
	private boolean leftButtonPressed;
	private boolean wheelPressed;

	/**
	 * Constructs a new <code>ComplexPlotMouseListener</code> with the given context.
	 * 
	 * @param context The program context
	 */
	public ComplexPlotMouseListener(Chaos context) {

		this.context = context;

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {

		arg0.getComponent().requestFocus();

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

		oldX = arg0.getX();
		oldY = arg0.getY();
		
		if(arg0.getButton() == MouseEvent.BUTTON1) {
			
			leftButtonPressed = true;
			
		}
		else if(arg0.getButton() == MouseEvent.BUTTON2) {
			
			wheelPressed = true;
			
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		
		if(arg0.getButton() == MouseEvent.BUTTON1) {
			
			leftButtonPressed = false;
			
		}
		else if(arg0.getButton() == MouseEvent.BUTTON2) {
			
			wheelPressed = false;
			
		}
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {

		int x = arg0.getX();
		int y = arg0.getY();
		FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();
		RenderMethod renderMethod = document.getCurrentKeyFrame().getRenderMethod();

		if(renderMethod instanceof ComplexPlot) {

			ComplexPlot plot = (ComplexPlot) renderMethod;
			Dimension componentSize = arg0.getComponent().getSize();
			Vec2i resolution = document.getSettings().getResolution();
			double xDiff = (oldX - x) / componentSize.getWidth();
			double yDiff = (oldY - y) / componentSize.getHeight();
			
			if(leftButtonPressed) {
				
				// TODO: Something is wrong with rotation when images are not square
				double compRatio = componentSize.getWidth() / componentSize.getHeight();
				double resRatio = resolution.getX() / resolution.getY();
				double ratio = compRatio / resRatio;
				xDiff *= ratio;
				Complex c = new Complex(xDiff, yDiff).rotate(plot.getRotation());
				plot.pan(c.getA(), c.getB());
				document.setEdited(true);
				
			}
			else if(wheelPressed) {
				
				plot.setRotation((yDiff * Math.PI * 2) + plot.getRotation());
				document.setEdited(true);
				
			}

		}
		
		oldX = x;
		oldY = y;

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {

		Document document = context.getDocumentManager().getCurrent();
		RenderMethod renderMethod = ((FRC2Document) document).getCurrentKeyFrame().getRenderMethod();

		if(renderMethod instanceof ComplexPlot) {

			ComplexPlot plot = (ComplexPlot) renderMethod;
			plot.zoom(SENSITIVITY * e.getWheelRotation());
			document.setEdited(true);

		}

	}

}
