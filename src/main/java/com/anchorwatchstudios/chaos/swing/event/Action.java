package com.anchorwatchstudios.chaos.swing.event;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import org.apache.commons.io.FilenameUtils;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.ArrayListFRC2Document;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.GSONFRC2DocumentParser;
import com.anchorwatchstudios.chaos.document.KeyFrame;
import com.anchorwatchstudios.chaos.document.Settings;
import com.anchorwatchstudios.chaos.document.fractal.Cantor;
import com.anchorwatchstudios.chaos.document.fractal.EscapeTimeFractal;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Julia;
import com.anchorwatchstudios.chaos.document.fractal.Juliabulb;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbox;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbrot;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbulb;
import com.anchorwatchstudios.chaos.document.fractal.NewtonBasin;
import com.anchorwatchstudios.chaos.document.fractal.NewtonKnot;
import com.anchorwatchstudios.chaos.document.fractal.Tree2D;
import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.document.render.DefaultRenderMethod;
import com.anchorwatchstudios.chaos.document.render.RayMarch;
import com.anchorwatchstudios.chaos.document.render.RenderImplementation;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.document.shader.DichromaticShader;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.math.Vec3d;
import com.anchorwatchstudios.chaos.swing.Chaos;
import com.anchorwatchstudios.chaos.swing.DebugArea;
import com.anchorwatchstudios.chaos.swing.FRC2FileFilter;
import com.anchorwatchstudios.chaos.swing.MP4FileFilter;
import com.anchorwatchstudios.chaos.swing.PNGFileFilter;
import com.anchorwatchstudios.chaos.swing.StatusBar;
import com.anchorwatchstudios.chaos.swing.Timeline;
import com.anchorwatchstudios.chaos.swing.Window;
import com.anchorwatchstudios.chaos.swing.Workspace;
import com.anchorwatchstudios.chaos.swing.WorkspaceTab;
import com.anchorwatchstudios.chaos.swing.canvas.MultiCanvas;
import com.anchorwatchstudios.chaos.swing.task.VideoExportTask;
import com.anchorwatchstudios.chaos.task.MultiTaskManager;
import com.anchorwatchstudios.chaos.task.Task;

/**
 * Contains all of the AWT <code>ActionListener</code>s as subclasses.
 * This makes locating actions very easy and verbose.
 * 
 * @author Anthony Atella
 */
public class Action {

	/**
	 * Sets the current <code>Document</code>s <code>RenderImplementation</code> 
	 * to the given <code>RenderImplementation</code>.
	 * 
	 * @author Anthony Atella
	 */
	public static class SetRenderImplementation extends Chaos2ActionListener {

		private RenderImplementation imp;

		/**
		 * Constructs a new <code>SetRenderImplementation</code> with the given values.
		 * 
		 * @param context The program context
		 * @param imp The new <code>RenderImplementation</code>
		 */
		public SetRenderImplementation(Chaos context, RenderImplementation imp) {

			super(context);
			this.imp = imp;

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

			if(document != null) {

				document.getSettings().setRenderImplementation(imp);
				document.setEdited(true);

			}

		}

	}

	/**
	 * Removes the current <code>KeyFrame</code> from the current <code>Document</code>.
	 * 
	 * @author Anthony Atella
	 */
	public static class RemoveKeyFrame extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>RemoveKeyFrame</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public RemoveKeyFrame(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

			if(document != null && document.getKeyFrameCount() > 1 && document.getKeyFrameIndex() != 0) {

				document.removeCurrentKeyFrame();
				document.notifyObservers();

			}

		}

	}

	/**
	 * Adds a <code>KeyFrame</code> to the current <code>Document</code> after the
	 * current <code>KeyFrame</code>.
	 * 
	 * @author Anthony Atella
	 */
	public static class AddKeyFrame extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>AddKeyFrame</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public AddKeyFrame(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

			if(document != null) {

				document.addKeyFrame(document.getCurrentKeyFrame().clone());
				document.setEdited(true);

			}

		}

	}

	/**
	 * Adds a task of the given type to the <code>MultiTaskManager</code>.
	 * 
	 * @author Anthony Atella
	 */
	public static class AddTask extends Chaos2ActionListener {

		private Task task;
		private int type;

		/**
		 * Constructs a new <code>AddTask</code> with the given values.
		 * 
		 * @param context The program context
		 * @param task The task to be added
		 * @param type The task type. Possible values are MultiTaskManager.SINGLE,
		 * MultiTaskManager.SYNC, and MultiTaskManager.ASYNC
		 */
		public AddTask(Chaos context, Task task, int type) {

			super(context);
			this.task = task;
			this.type = type;

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			context.getMultiTaskManager().addTask(task, type);

		}

	}

	/**
	 * Sets the current <code>Document</code>s keyframe index to the
	 * given index.
	 * 
	 * @author Anthony Atella
	 */
	public static class SetKeyFrame extends Chaos2ActionListener {

		private int index;

		/**
		 * Constructs a new <code>SetKeyFrame</code> with the given
		 * values.
		 * 
		 * @param context The program context
		 * @param index The new keyframe index
		 */
		public SetKeyFrame(Chaos context, int index) {

			super(context);
			this.index = index;

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

			if(document != null) {

				document.setKeyFrameIndex(index);
				document.setEdited(true);

			}

		}

	}

	/**
	 * Toggles the debug area display.
	 * 
	 * @author Anthony Atella
	 */
	public static class ToggleDebug extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>ToggleDebug</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public ToggleDebug(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			DocumentManager documentManager = context.getDocumentManager();
			Window window = context.getWindow();
			DebugArea debugArea = context.getWindow().getDebugArea();
			debugArea.setVisible(!debugArea.isVisible());
			window.getToolBar().notify(documentManager);
			window.getChaosMenuBar().notify(documentManager);

		}

	}

	/**
	 * Sets the current <code>Document</code>s <code>RenderMethod</code>.
	 * 
	 * @author Anthony Atella
	 */
	public static class SetRenderMethod extends Chaos2ActionListener {

		private RenderMethod renderMethod;

		/**
		 * Constructs a new <code>SetRenderMethod</code> with the given values.
		 * 
		 * @param context The program context
		 * @param renderMethod The new <code>RenderMethod</code>
		 */
		public SetRenderMethod(Chaos context, RenderMethod renderMethod) {

			super(context);
			this.renderMethod = renderMethod;

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();
			document.getCurrentKeyFrame().setRenderMethod(renderMethod);
			document.notifyObservers();

		}

	}

	/**
	 * Sets the current <code>Document</code>s <code>Fractal</code>.
	 * 
	 * @author Anthony Atella
	 */
	public static class SetFractal extends Chaos2ActionListener {

		private String selection;

		public SetFractal(Chaos context, String selection) {

			super(context);
			this.selection = selection;

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

			if(selection.equals(Julia.class.getSimpleName())) {

				document.getCurrentKeyFrame().setFractal(new Julia());

			}
			else if(selection.equals(Mandelbrot.class.getSimpleName())) {

				document.getCurrentKeyFrame().setFractal(new Mandelbrot());

			}
			else if(selection.equals(NewtonBasin.class.getSimpleName())) {

				document.getCurrentKeyFrame().setFractal(new NewtonBasin());

			}
			else if(selection.equals(Tree2D.class.getSimpleName())) {

				document.getCurrentKeyFrame().setFractal(new Tree2D());

			}
			else if(selection.equals(Cantor.class.getSimpleName())) {

				document.getCurrentKeyFrame().setFractal(new Cantor());

			}
			else if(selection.equals(Mandelbulb.class.getSimpleName())) {

				document.getCurrentKeyFrame().setFractal(new Mandelbulb());

			}
			else if(selection.equals(Juliabulb.class.getSimpleName())) {

				document.getCurrentKeyFrame().setFractal(new Juliabulb());

			}

			document.setEdited(true);

		}

	}	

	/**
	 * Sets the current <code>Document</code>s <code>Shader</code>.
	 * 
	 * @author Anthony Atella
	 */
	public static class SetShader extends Chaos2ActionListener {

		private Shader shader;

		public SetShader(Chaos context, Shader shader) {

			super(context);
			this.shader = shader;

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			DocumentManager documentManager = context.getDocumentManager();
			KeyFrame keyframe = ((FRC2Document) documentManager.getCurrent()).getCurrentKeyFrame();
			keyframe.setShader(shader);
			documentManager.getCurrent().setEdited(true);

		}

	}

	/**
	 * Opens a <code>Document</code>.
	 * 
	 * @author Anthony Atella
	 */
	public static class Open extends Chaos2ActionListener {

		/**
		 * Creates a new <code>Open</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public Open(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			JFileChooser fileChooser = context.getFileChooser();
			fileChooser.resetChoosableFileFilters();
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(new FRC2FileFilter());

			if(fileChooser.showOpenDialog(context.getWindow()) == JFileChooser.APPROVE_OPTION) {

				FRC2Document document = new GSONFRC2DocumentParser().load(fileChooser.getSelectedFile());

				if(document != null) {

					context.getDocumentManager().add(document);
					MultiCanvas canvas = new MultiCanvas(context);
					Workspace workspace = context.getWindow().getWorkspace();
					workspace.add(FilenameUtils.removeExtension(document.getFile().getName()), canvas);
					workspace.setSelectedComponent(canvas);
					document.notifyObservers();

				}

			}

		}

	}

	/**
	 * Closes the current <code>Document</code>
	 * 
	 * @author Anthony Atella
	 */
	public static class Close extends Chaos2ActionListener {

		private boolean cancelled;
		
		/**
		 * Constructs a new <code>Close</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public Close(Chaos context) {

			super(context);
			cancelled = false;

		}
		
		public boolean isCancelled() {
			
			return cancelled;
			
		}

		@Override
		public void actionPerformed(ActionEvent e) {

			DocumentManager documentManager = context.getDocumentManager();

			if(documentManager.getCurrent() != null) {

				Workspace workspace = context.getWindow().getWorkspace();

				if(documentManager.getCurrent().isEdited()) {

					int answer = JOptionPane.showConfirmDialog(context.getWindow(), Strings.CONFIRM_CLOSE);

					if(answer == JOptionPane.YES_OPTION) {
						
						new Save(context).actionPerformed(e);
						
					}
					
					if(answer != JOptionPane.CANCEL_OPTION) {

						int index = documentManager.getIndex();
						workspace.remove(index);
						workspace.setSelectedIndex(index - 1);
						documentManager.remove(index);

					}
					else {

						cancelled = true;
						
					}
					
				}
				else {
					
					int index = documentManager.getIndex();
					workspace.remove(index);
					workspace.setSelectedIndex(index - 1);
					documentManager.remove(index);
					
				}

			}

		}

	}

	/**
	 * Closes the program.
	 * 
	 * @author Anthony Atella
	 */
	public static class Exit extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>Exit</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public Exit(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			DocumentManager dm = context.getDocumentManager();
			int count = dm.getDocumentCount();
			boolean cancelled = false;
			
			for(int i = count - 1;i > -1;i--) {
				
				dm.setIndex(i);
				Close close = new Action.Close(context);
				close.actionPerformed(e);
				
				if(close.isCancelled()) {
					
					cancelled = true;
					break;
					
				}
					
			}
			
			if(!cancelled) {

				System.exit(0);

			}

		}

	}

	/**
	 * Exports a video using the current <code>Document</code>.
	 * 
	 * @author Anthony Atella
	 */
	public static class ExportVideo extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>ExportVideo</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public ExportVideo(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			JFileChooser fileChooser = context.getFileChooser();
			fileChooser.resetChoosableFileFilters();
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(new MP4FileFilter());
			String fileName = context.getDocumentManager().getCurrent().getFile().getName();
			fileName = FilenameUtils.removeExtension(fileName);
			fileChooser.setSelectedFile(new File(fileName));
			
			if(fileChooser.showSaveDialog(context.getWindow()) == JFileChooser.APPROVE_OPTION) {

				File outputFile = context.getFileChooser().getSelectedFile();

				if(!outputFile.getPath().endsWith(".mp4")) {

					outputFile = new File(outputFile.getPath() + ".mp4");

				}
				
				Settings settings = ((FRC2Document) context.getDocumentManager().getCurrent()).getSettings();
				context.getMultiTaskManager().addTask(new VideoExportTask(context, outputFile, settings.getFPK(), settings.getFPS()), MultiTaskManager.SYNC);

			}

		}

	}

	/**
	 * Exports an image using the current <code>Document</code>.
	 * 
	 * @author Anthony Atella
	 */
	public static class ExportImage extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>ExportImage</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public ExportImage(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			JFileChooser fileChooser = context.getFileChooser();
			fileChooser.resetChoosableFileFilters();
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(new PNGFileFilter());
			String fileName = context.getDocumentManager().getCurrent().getFile().getName();
			fileName = FilenameUtils.removeExtension(fileName);
			fileChooser.setSelectedFile(new File(fileName));
			
			if(fileChooser.showSaveDialog(context.getWindow()) == JFileChooser.APPROVE_OPTION) {
				
				BufferedImage image = context.getWindow().getWorkspace().getImage();
				File outputFile = context.getFileChooser().getSelectedFile();

				if(!outputFile.getPath().endsWith(".png")) {

					outputFile = new File(outputFile.getPath() + ".png");

				}
				
				try {
					
					ImageIO.write(image, "png", outputFile);
					
				} catch (IOException e1) {

					StatusBar statusBar = context.getWindow().getStatusBar();
					statusBar.setStatus(Strings.IO_ERROR);
					statusBar.setProgress(1);
					e1.printStackTrace();
					
				}

			}

		}

	}

	/**
	 * Saves the current <code>Document</code> after asking
	 * the user where they would like to save it.
	 * 
	 * @author Anthony Atella
	 */
	public static class SaveAs extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>SaveAs</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public SaveAs(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			JFileChooser fileChooser = context.getFileChooser();
			FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();
			fileChooser.resetChoosableFileFilters();
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(new FRC2FileFilter());
			String fileName = context.getDocumentManager().getCurrent().getFile().getName();
			fileName = FilenameUtils.removeExtension(fileName);
			fileChooser.setSelectedFile(new File(fileName));

			if(fileChooser.showSaveDialog(context.getWindow()) == JFileChooser.APPROVE_OPTION) {

				File outputFile = fileChooser.getSelectedFile();
				
				if(!outputFile.getPath().endsWith(".frc2")) {

					outputFile = new File(outputFile.getPath() + ".frc2");

				}
				
				document.setFile(outputFile);
				new GSONFRC2DocumentParser().save(document);
				int index = context.getDocumentManager().getIndex();
				String name = FilenameUtils.removeExtension(context.getDocumentManager().getCurrent().getFile().getName());
				((WorkspaceTab) context.getWindow().getWorkspace().getTabComponentAt(index)).setTitle(name);
				
			}

		}

	}

	/**
	 * Saves the current <code>Document</code>. Queries the user
	 * for file location if the file does not exist.
	 * 
	 * @author Anthony Atella
	 */
	public static class Save extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>Save</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public Save(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

			if(document != null) {

				if(document.getFile().exists()) {

					new GSONFRC2DocumentParser().save(document);
					int index = context.getDocumentManager().getIndex();
					String name = FilenameUtils.removeExtension(context.getDocumentManager().getCurrent().getFile().getName());
					((WorkspaceTab) context.getWindow().getWorkspace().getTabComponentAt(index)).setTitle(name);
					
				}
				else {

					new SaveAs(context).actionPerformed(e);

				}

			}

		}

	}

	/**
	 * Launches a browser and shows the Chaos landing page.
	 * 
	 * @author Anthony Atella
	 */
	public static class ShowAbout extends Chaos2ActionListener {
		
		/**
		 * Constructs a new <code>ShowAbout</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public ShowAbout(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			JOptionPane.showMessageDialog(context.getWindow(), Strings.ABOUT_MESSAGE, Strings.ABOUT_MESSAGE_TITLE, JOptionPane.INFORMATION_MESSAGE, context.getWindow().getIcon());
			
		}

	}
	
	/**
	 * Shows the controls in a popup window.
	 * 
	 * @author Anthony Atella
	 */
	public static class ShowControls extends Chaos2ActionListener {
		
		/**
		 * Constructs a new <code>ShowControls</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public ShowControls(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			ImageIcon icon = new ImageIcon(this.getClass().getResource("/input.png"));
			JOptionPane.showMessageDialog(context.getWindow(), "", Strings.CONTROLS_MESSAGE_TITLE, JOptionPane.INFORMATION_MESSAGE, icon);
			
		}

	}

	/**
	 * Launches a browser and shows the online help section.
	 * 
	 * @author Anthony Atella
	 */
	public static class ShowHelp extends Chaos2ActionListener {

		public ShowHelp(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			if(Desktop.isDesktopSupported()) {
				
				try {
					
					Desktop.getDesktop().browse(new URI(Chaos.URL));
					
				} catch (IOException e1) {

					e1.printStackTrace();
					
				} catch (URISyntaxException e1) {

					e1.printStackTrace();
					
				}
			  
			}
			else {
				
				context.getWindow().getStatusBar().setStatus(Strings.BROWSER_ERROR);
				
			}
			
		}

	}

	/**
	 * Toggles the toolbar lock. When locked the toolbar cannot be moved.
	 * 
	 * @author Anthony Atella
	 */
	public static class ToggleToolbarLock extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>ToggleToolbarLock</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public ToggleToolbarLock(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			JToolBar toolbar = context.getWindow().getToolBar();
			toolbar.setEnabled(!toolbar.isEnabled());

		}

	}

	/**
	 * Toggles the status bar visibility.
	 * 
	 * @author Anthony Atella
	 */
	public static class ToggleStatusBar extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>ToggleStatusBar</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public ToggleStatusBar(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			StatusBar statusBar = context.getWindow().getStatusBar();
			statusBar.setVisible(!statusBar.isVisible());
			SwingUtilities.updateComponentTreeUI(context.getWindow());

		}

	}

	/**
	 * Toggles the rollout visibility.
	 * 
	 * @author Anthony Atella
	 */
	public static class ToggleRollout extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>ToggleRollout</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public ToggleRollout(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			JSplitPane splitPane = context.getWindow().getSplitPane();
			Component rollout = splitPane.getRightComponent();
			rollout.setVisible(!rollout.isVisible());
			splitPane.setDividerLocation(context.getWindow().getDividerLocation());
			SwingUtilities.updateComponentTreeUI(context.getWindow());

		}

	}

	/**
	 * Toggles the timeline visibility.
	 * 
	 * @author Anthony Atella
	 */
	public static class ToggleTimeline extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>ToggleTimeline</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public ToggleTimeline(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			Timeline timeline = context.getWindow().getTimeline();
			timeline.setVisible(!timeline.isVisible());
			SwingUtilities.updateComponentTreeUI(context.getWindow());

		}

	}

	/**
	 * Toggles the toolbar visibility.
	 * 
	 * @author Anthony Atella
	 */
	public static class ToggleToolbar extends Chaos2ActionListener {

		/**
		 * Constructs a new <code>ToggleToolbar</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public ToggleToolbar(Chaos context) {

			super(context);

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			JToolBar toolbar = context.getWindow().getToolBar();
			toolbar.setVisible(!toolbar.isVisible());
			SwingUtilities.updateComponentTreeUI(context.getWindow());

		}

	}

	/**
	 * Creates a new <code>Document</code> with the given <code>Fractal</code>.
	 * 
	 * @author Anthony Atella
	 */
	public static class New extends Chaos2ActionListener {

		private Fractal fractal;

		/**
		 * Constructs a new <code>New</code> with the given values.
		 * 
		 * @param context The program context
		 * @param fractal The fractal
		 */
		public New(Chaos context, Fractal fractal) {

			super(context);
			this.fractal = fractal;

		}

		@Override
		public void actionPerformed(ActionEvent e) {

			Shader shader = new DichromaticShader();
			RenderMethod renderSettings = null;

			if(fractal instanceof Cantor) {

				renderSettings = new DefaultRenderMethod();

			}
			else if(fractal instanceof Tree2D) {

				renderSettings = new DefaultRenderMethod();

			}
			else if(fractal instanceof EscapeTimeFractal<?>){

				EscapeTimeFractal<?> etf = (EscapeTimeFractal<?>) fractal;

				if(etf.getType().equals(Complex.class)) {

					renderSettings = new ComplexPlot();
					
					if(etf instanceof Mandelbrot) {
						
						ComplexPlot c = (ComplexPlot) renderSettings;
						c.setMin(new Complex(-2.25, -1.5));
						c.setMax(new Complex(0.75, 1.5));
						
					}

				}
				else {

					renderSettings = new RayMarch();
					
					if(etf instanceof Mandelbox) {
						
						RayMarch rm = (RayMarch) renderSettings;
						rm.setCameraOrigin(new Vec3d(7.5, 5, 7.5));
						rm.setCameraLookAt(new Vec3d(-1, 0, -1));
						rm.setIterations(125);
						
					}
					else if(etf instanceof NewtonKnot) {
						
						RayMarch rm = (RayMarch) renderSettings;
						rm.setRayThreshold(0.05);
						rm.setIterations(10);
						
					}

				}

			}

			FRC2Document document = new ArrayListFRC2Document(fractal.clone(), renderSettings, new Settings().clone(), shader);
			document.getSettings().setRenderImplementation(renderSettings.getCapabilities()[0]);
			DocumentManager documentManager = context.getDocumentManager();
			documentManager.add(document);
			MultiCanvas canvas = new MultiCanvas(context);
			Workspace workspace = context.getWindow().getWorkspace();
			workspace.add(FilenameUtils.removeExtension(document.getFile().getName()), canvas);
			workspace.setSelectedComponent(canvas);
			document.notifyObservers();

		}

	}

	/**
	 * The base class for listeners that take a program context as an
	 * argument.
	 * 
	 * @author Anthony Atella
	 */
	private abstract static class Chaos2ActionListener implements ActionListener {

		protected Chaos context;

		/**
		 * Constructs a new <code>Chaos2ActionListener</code> with the given context.
		 * 
		 * @param context The program context
		 */
		public Chaos2ActionListener(Chaos context) {

			this.context = context;

		}

	}

}
