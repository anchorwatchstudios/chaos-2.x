package com.anchorwatchstudios.chaos.swing.event;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.RayMarch;
import com.anchorwatchstudios.chaos.math.Vec3d;
import com.anchorwatchstudios.chaos.swing.Chaos;

/**
 * Listens for mouse events and manipulates a ray marcher
 * accordingly.
 * 
 * @author Anthony Atella
 */
public class RayMarchMouseListener implements MouseListener, MouseMotionListener, MouseWheelListener {

	private static final Vec3d WORLD_UP = new Vec3d(0, 1, 0);
	private Chaos context;
	private int oldX, oldY;
	private boolean leftButtonPressed;
	private double pitch;
	private double rotation;

	/**
	 * Constructs a new <code>RayMarchMouseListener</code> with the given context.
	 * 
	 * @param context The program context
	 */
	public RayMarchMouseListener(Chaos context) {

		this.context = context;
		this.rotation = Math.PI * 1.5;
		this.pitch = Math.PI / 2;

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {

		arg0.getComponent().requestFocus();

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

		oldX = arg0.getX();
		oldY = arg0.getY();
		
		if(arg0.getButton() == MouseEvent.BUTTON1) {

			leftButtonPressed = true;

		}

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

		if(arg0.getButton() == MouseEvent.BUTTON1) {

			leftButtonPressed = false;

		}

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {

		if(leftButtonPressed) {

			FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();
			RayMarch renderMethod = (RayMarch) document.getCurrentKeyFrame().getRenderMethod();
			double xDiff = (oldX - arg0.getX()) / (double) arg0.getComponent().getWidth();
			double yDiff = (oldY - arg0.getY()) / (double) arg0.getComponent().getHeight();
			rotation += xDiff * Math.PI * 2;
			
			if((pitch < Math.PI && yDiff > 0) || (pitch > 0 && yDiff < 0)) {
				
				pitch += yDiff * Math.PI;
			
			}
			
			Vec3d lookAt = new Vec3d(Math.cos(rotation), Math.cos(pitch), Math.sin(rotation)).normalize();
			renderMethod.setCameraLookAt(lookAt);
			Vec3d right = lookAt.cross(WORLD_UP);
			Vec3d up = right.cross(lookAt);
			renderMethod.setCameraUp(up);
			oldX = arg0.getX();
			oldY = arg0.getY();
			document.notifyObservers();
			
		}

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {



	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {

	}

}
