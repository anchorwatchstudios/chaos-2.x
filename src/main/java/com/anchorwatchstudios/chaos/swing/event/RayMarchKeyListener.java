package com.anchorwatchstudios.chaos.swing.event;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.RayMarch;
import com.anchorwatchstudios.chaos.math.Vec3d;
import com.anchorwatchstudios.chaos.swing.Chaos;

/**
 * Listens for key events and manipulates a ray marcher
 * accordingly.
 * 
 * @author Anthony Atella
 */
public class RayMarchKeyListener implements KeyListener {

	private static final double SCALE = 0.01;
	private static final double CTRL_SCALE = 0.03;
	private Chaos context;
	private boolean spaceIsPressed;
	private boolean shiftIsPressed;
	private boolean sIsPressed;
	private boolean dIsPressed;
	private boolean wIsPressed;
	private boolean aIsPressed;
	private boolean ctrlIsPressed;
	
	/**
	 * Constructs a new <code>RayMarchKeyListener</code> with the given context.
	 * 
	 * @param context The program context
	 */
	public RayMarchKeyListener(Chaos context) {
	
		this.context = context;
		spaceIsPressed = false;
		shiftIsPressed = false;
		ctrlIsPressed = false;
		sIsPressed = false;
		dIsPressed = false;
		wIsPressed = false;
		aIsPressed = false;
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {

		switch(arg0.getKeyCode()) {

		case 65:
			
			aIsPressed = true;
			break;
			
		case 87:
			
			wIsPressed = true;
			break;
			
		case 68:
			
			dIsPressed = true;
			break;
			
		case 83:
			
			sIsPressed = true;
			break;
			
		case 16:
			
			shiftIsPressed = true;
			break;
			
		case 32:
			
			spaceIsPressed = true;
			break;
			
		case 17:
			
			ctrlIsPressed = true;
			break;
		
		}
		
		update();

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
		switch(arg0.getKeyCode()) {

		case 65:
			
			aIsPressed = false;
			break;
			
		case 87:
			
			wIsPressed = false;
			break;
			
		case 68:
			
			dIsPressed = false;
			break;
			
		case 83:
			
			sIsPressed = false;
			break;
			
		case 16:
			
			shiftIsPressed = false;
			break;
			
		case 32:
			
			spaceIsPressed = false;
			break;
			
		case 17:
			
			ctrlIsPressed = false;
			break;
		
		}
						
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}
	
	private void update() {
		
		RayMarch rmc = (RayMarch) ((FRC2Document) context.getDocumentManager().getCurrent()).getCurrentKeyFrame().getRenderMethod();
		Vec3d lookAt = rmc.getCameraLookAt().normalize();
		Vec3d origin = rmc.getCameraOrigin();
		Vec3d up = rmc.getCameraUp().normalize();
		double scale = SCALE;
		boolean workPerformed = false;
		
		if(ctrlIsPressed) {
		
			scale = CTRL_SCALE;
			
		}
		
		if(aIsPressed) {
			
			Vec3d left = up.cross(lookAt);
			origin = origin.add(left.scalar(scale));
			workPerformed = true;
			
		}
		
		if(wIsPressed) {
			
			origin = origin.add(lookAt.scalar(scale));
			workPerformed = true;

		}
		
		if(dIsPressed) {
			
			Vec3d right = lookAt.cross(up);
			origin = origin.add(right.scalar(scale));
			workPerformed = true;
			
		}
		
		if(sIsPressed) {
			
			origin = origin.subtract(lookAt.scalar(scale));
			workPerformed = true;

		}
		
		if(shiftIsPressed) {
			
			origin = origin.subtract(up.scalar(scale));
			workPerformed = true;

		}
		
		if(spaceIsPressed) {
			
			origin = origin.add(up.scalar(scale));
			workPerformed = true;

		}
		
		if(workPerformed) {
		
			rmc.setCameraOrigin(origin);
			context.getDocumentManager().notifyObservers();
			
		}
		
	}

}
