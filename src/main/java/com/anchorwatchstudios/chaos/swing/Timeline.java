package com.anchorwatchstudios.chaos.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;

import org.kordamp.ikonli.fontawesome.FontAwesome;
import org.kordamp.ikonli.swing.FontIcon;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.swing.event.Action;

/**
 * Displays a list of <code>KeyFrame</code> buttons with add and remove
 * functionality.
 * 
 * @author Anthony Atella
 *
 */
public class Timeline extends JPanel implements DocumentManagerObserver {

	// TODO: The view isn't updating or it's collapsing or something sometimes.
	private static final long serialVersionUID = 1L;
	private Chaos context;
	private JPanel keyFramePanel;
	private ButtonGroup buttonGroup;
	private JButton removeButton;
	private JButton addButton;
	
	/**
	 * Constructs a new <code>Timeline</code>.
	 * 
	 * @param context The program context
	 */
	public Timeline(Chaos context) {
		
		this.context = context;
		context.getDocumentManager().registerObserver(this);
		Font font = new Font("Arial", Font.PLAIN, 24);
		buttonGroup = new ButtonGroup();
		setLayout(new BorderLayout());
		JLabel label = new JLabel(Strings.TIMELINE);
		label.setFont(font);
		add(label, BorderLayout.NORTH);
		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.LEFT);
		keyFramePanel = new JPanel(layout);
		keyFramePanel.setBackground(Color.WHITE);
		layout = new FlowLayout();
		layout.setAlignment(FlowLayout.RIGHT);
		JPanel buttonPanel = new JPanel(layout);
		JScrollPane scrollPane = new JScrollPane(keyFramePanel);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		add(scrollPane, BorderLayout.CENTER);
		
		int size = 18;
		FontIcon fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.MINUS);
		fontIcon.setIconSize(size);
		fontIcon.setIconColor(Color.DARK_GRAY);
		
		removeButton = new JButton(fontIcon);
		removeButton.setEnabled(false);
		removeButton.addActionListener(new Action.RemoveKeyFrame(context));
		buttonPanel.add(removeButton);
		
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.PLUS);
		fontIcon.setIconSize(size);
		fontIcon.setIconColor(Color.DARK_GRAY);
		
		addButton = new JButton(fontIcon);
		addButton.setEnabled(false);
		addButton.addActionListener(new Action.AddKeyFrame(context));
		buttonPanel.add(addButton);
		add(buttonPanel, BorderLayout.SOUTH);
		
	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document == null) {
			
			addButton.setEnabled(false);
			removeButton.setEnabled(false);
			keyFramePanel.removeAll();

		}
		else {
			
			if(document.getKeyFrameIndex() == 0) {
				
				removeButton.setEnabled(false);
				
			}
			else {
				
				removeButton.setEnabled(true);
				
			}
			
			if(document.getKeyFrameCount() != buttonGroup.getButtonCount()) {
			
				updateButtons(document);
				
			}
			
			addButton.setEnabled(true);
			
		}

		revalidate();
		repaint();

	}
	
	/**
	 * Adds a single <code>KeyFrame</code> button.
	 * 
	 * @param document The <code>Document</code>
	 */
	private void addKeyFrame(FRC2Document document) {
		
		int id = buttonGroup.getButtonCount() + 1;
		JToggleButton button = new JToggleButton(Integer.toString(id));
		button.addActionListener(new Action.SetKeyFrame(context, id - 1));
		buttonGroup.add(button);
		
		if(document.getKeyFrameIndex() == id - 1) {
			
			button.setSelected(true);
			
		}

		button.setFont(new Font("Arial", Font.BOLD, 24));
		keyFramePanel.add(button);

	}
	
	/**
	 * Updates the buttons based on the given <code>Document</code>.
	 * 
	 * @param document The <code>Document</code>
	 */
	private void updateButtons(FRC2Document document) {

		int count = document.getKeyFrameCount();
		keyFramePanel.removeAll();
		buttonGroup = new ButtonGroup();
		
		for(int i = 0;i < count;i++) {
			
			addKeyFrame(document);
			
		}
		
	}

}
