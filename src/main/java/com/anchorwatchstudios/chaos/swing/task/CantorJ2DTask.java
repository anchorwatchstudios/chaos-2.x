package com.anchorwatchstudios.chaos.swing.task;

import java.awt.Graphics2D;

import com.anchorwatchstudios.chaos.document.Settings;
import com.anchorwatchstudios.chaos.document.fractal.Cantor;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.math.Vec4d;
import com.anchorwatchstudios.chaos.swing.AWTColorFactory;
import com.anchorwatchstudios.chaos.swing.canvas.Java2DCanvas;

/**
 * Renders the Cantor set using Java2D.
 * 
 * @author Anthony Atella
 */
public class CantorJ2DTask extends J2DTask {

	private boolean running;
	private String message;
	private double progress;
	
	/**
	 * Constructs an empty <code>CantorJ2DTask</code> with
	 * null values.
	 */
	public CantorJ2DTask() {
		
		super();
		
	}
	
	/**
	 * Constructs a new <code>CantorJ2DTask</code> with the given values.
	 * 
	 * @param fractal The Cantor set
	 * @param shader The <code>Shader</code>
	 * @param renderMethod The <code>RenderMethod</code>
	 * @param settings The <code>Document</code> settings
	 * @param canvas The <code>Java2DCanvas</code>
	 */
	public CantorJ2DTask(Cantor fractal, Shader shader, RenderMethod renderMethod, Settings settings, Java2DCanvas canvas) {
		
		super(fractal, shader, renderMethod, settings);
		running = false;
		progress = 100.0;
		message = "";
		this.canvas = canvas;
		
	}
	
	@Override
	public void togglePause() {
		
		// Unimplemented
		
	}

	@Override
	public boolean isRunning() {

		return running;
		
	}

	@Override
	public boolean isPaused() {
		
		return false;
		
	}

	@Override
	public void stop() {

		running = false;
		
	}

	@Override
	public String getMessage() {

		return message;
		
	}

	@Override
	public double getProgress() {

		return progress;
		
	}

	@Override
	public void run() {

		render();
		
	}

	@Override
	protected void render() {
		
		progress = 0.0;
		message = "Rendering...";
		running = true;
		notifyObservers();
		Graphics2D g2d = canvas.getImage().createGraphics();
		Cantor cantor = (Cantor) getFractal();
		g2d.setColor(AWTColorFactory.convert(getShader().getFragment(new Vec4d())));
		g2d.fillRect(0, 0, canvas.getImage().getWidth(), canvas.getImage().getHeight());
		g2d.setColor(AWTColorFactory.convert(getShader().getFragment(new Vec4d(1.0, 0, 0, 0))));
		cantor(g2d, cantor.getThreshold(), 0, canvas.getImage().getWidth(), cantor.getSpace());
		progress = 100.0;
		message = "";
		running = false;
		notifyObservers();
		canvas.repaint();
		
	}

	/**
	 * Draws the Cantor set using a recursive definition.
	 * 
	 * @param g2d The graphics context
	 * @param threshold The minimum line length
	 * @param start The starting y value of lines
	 * @param length The starting length of lines
	 * @param space The number of spaces in pixels in between lines
	 */
	private void cantor(Graphics2D g2d, double threshold, double start, double length, double space) {
		
		g2d.drawLine((int) start, 0, (int) (start + length), 0);
		
		if(length > threshold) {
			
			double segment = length / 3.0;
			g2d.translate(0, space);
			Graphics2D g2d_2 = (Graphics2D) g2d.create();
			cantor(g2d, threshold, start,  segment, space);
			cantor(g2d_2, threshold, start + segment * 2, segment, space);
			
		}
		
	}
	
}
