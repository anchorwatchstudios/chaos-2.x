package com.anchorwatchstudios.chaos.swing.task;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import org.jcodec.api.awt.AWTSequenceEncoder;

import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.KeyFrame;
import com.anchorwatchstudios.chaos.swing.Chaos;
import com.anchorwatchstudios.chaos.swing.Workspace;
import com.anchorwatchstudios.chaos.swing.canvas.MultiCanvas;
import com.anchorwatchstudios.chaos.task.PriorityTask;

/**
 * Exports a video to the given file using the current
 * <code>Document</code>.
 * 
 * @author Anthony Atella
 *
 */
public class VideoExportTask extends PriorityTask {

	private static final long SLEEP = 500;
	private static final int DEFAULT_PRIORITY = 1000;
	private int fpk;
	private volatile boolean running;
	private volatile boolean paused;
	private String message;
	private double progress;
	private Chaos context;
	private File file;
	private int fps;
	
	/**
	 * Constructs a new <code>VideoExportTask</code> with the given values.
	 * 
	 * @param context The program context
	 * @param file The file to save to
	 * @param interpolationFrames The number of frames for each key-frame
	 * @param fps The frames-per-second
	 */
	public VideoExportTask(Chaos context, File file, int interpolationFrames, int fps) {
		
		running = false;
		paused = false;
		this.context = context;
		this.file = file;
		this.fpk = interpolationFrames;
		this.fps = fps;
		setPriority(DEFAULT_PRIORITY);
		
	}
	
	@Override
	public void togglePause() {

		paused = !paused;
		
	}

	@Override
	public boolean isRunning() {

		return running;

	}

	@Override
	public boolean isPaused() {

		return paused;
		
	}

	@Override
	public void stop() {

		running = false;
		
	}

	@Override
	public String getMessage() {

		return message;

	}

	@Override
	public double getProgress() {

		return progress;
		
	}

	@Override
	public void run() {

		running = true;
		FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();
		Workspace workspace = context.getWindow().getWorkspace();
		int keyFrameCount = document.getKeyFrameCount();
		AWTSequenceEncoder sequenceEncoder = null;
		context.getWindow().setEnabled(false);
		
		try {
			
			sequenceEncoder = AWTSequenceEncoder.createSequenceEncoder(file, fps);
			
		}
		catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
		for(int i = 0;i < keyFrameCount;i++) {
			
			message = "Exporting video (KeyFrame " + (i + 1) + " of " + keyFrameCount + ")";
			progress = (double) i / (double) keyFrameCount;
			document.setKeyFrameIndex(i);
			notifyObservers();
			
			if(!running) {
				
				progress = 1;
				message = "";
				break;
				
			}
					
			if(i < keyFrameCount - 1) {

				// Interpolate between keyframes
				for(int j = 0;j < fpk - 1;j++) {
					
					if(!running) {
						
						progress = 1;
						message = "";
						break;
						
					}
					
					KeyFrame keyFrame = document.getCurrentKeyFrame().interpolate(document.getKeyFrame(i + 1), (double) j / (double) fpk);
					document.addKeyFrame(keyFrame);
					MultiCanvas mc = (MultiCanvas) workspace.getSelectedComponent();
					mc.notify(context.getDocumentManager());
					
					while(mc.isRendering()) {
					
						try {
							
							Thread.sleep(SLEEP);
							
						} catch (InterruptedException e) {

							e.printStackTrace();
						
						}
												
					}
					
					addFrame(sequenceEncoder, workspace.getImage());
					document.removeCurrentKeyFrame();
					document.setKeyFrameIndex(i);
					
				}
				
			}
			else {

				// Render the last keyframe
				addFrame(sequenceEncoder, workspace.getImage());
				
			}

		}

		try {
			
			sequenceEncoder.finish();
			progress = 1;
			message = "";
			notifyObservers();

		}
		catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
		context.getWindow().setEnabled(true);
		running = false;

	}
	
	/**
	 * Adds a single frame to the video.
	 * 
	 * @param sequenceEncoder The sequence encoder
	 * @param bi The image to be added
	 */
	private void addFrame(AWTSequenceEncoder sequenceEncoder, BufferedImage bi) {
		
		try {
			
			sequenceEncoder.encodeImage(bi);

		}
		catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
	}

}
