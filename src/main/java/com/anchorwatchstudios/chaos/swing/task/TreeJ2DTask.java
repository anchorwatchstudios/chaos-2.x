package com.anchorwatchstudios.chaos.swing.task;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.anchorwatchstudios.chaos.document.Settings;
import com.anchorwatchstudios.chaos.document.fractal.Tree2D;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.math.Vec4d;
import com.anchorwatchstudios.chaos.swing.AWTColorFactory;
import com.anchorwatchstudios.chaos.swing.canvas.Java2DCanvas;

/**
 * Renders a two-dimensional fractal tree.
 * 
 * @author Anthony Atella
 */
public class TreeJ2DTask extends J2DTask {

	private volatile boolean running;
	private volatile String message;
	private volatile double progress;
	private int maxSteps;
	private int steps;
	
	/**
	 * Constructs an empty <code>TreeJ2DTask</code> with
	 * null values.
	 */
	public TreeJ2DTask() {
		
		super();
		
	}
	
	/**
	 * Creates a new <code>TreeJ2DTask</code> with the given values.
	 * 
	 * @param fractal The <code>Tree2D</code>
	 * @param shader The <code>Shader</code>
	 * @param renderMethod The <code>RenderMethod</code>
	 * @param settings The <code>Document</code> settings
	 * @param canvas The <code>Java2DCanvas</code>
	 */
	public TreeJ2DTask(Tree2D fractal, Shader shader, RenderMethod renderMethod, Settings settings, Java2DCanvas canvas) {
		
		super(fractal, shader, renderMethod, settings);
		this.canvas = canvas;
		running = false;
		progress = 100.0;
		message = "";
		steps = 0;
		maxSteps = 0;
		
	}
	
	@Override
	public void togglePause() {
		
		// Unimplemented
		
	}

	@Override
	public boolean isRunning() {

		return running;
		
	}

	@Override
	public boolean isPaused() {
		
		return false;
		
	}

	@Override
	public void stop() {

		running = false;
		
	}

	@Override
	public String getMessage() {

		return message;
		
	}

	@Override
	public double getProgress() {

		return progress;
		
	}

	@Override
	public void run() {

		render();
		
	}

	@Override
	protected void render() {
		
		progress = 0.0;
		message = "Rendering...";
		running = true;
		notifyObservers();
		Tree2D tree = (Tree2D) getFractal();
		double l = tree.getLength();
		// TODO: Wrong but quick. Not showing up anyway
		maxSteps = (int) (l / tree.getStep());
		BufferedImage image = canvas.getImage();
		Graphics2D g2d = image.createGraphics();
		int origin = image.getWidth() / 2;
		g2d.setColor(AWTColorFactory.convert(getShader().getFragment(new Vec4d())));
		g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
		g2d.translate(origin, image.getHeight());
		g2d.rotate(-tree.getAngle());
		treeBranch(g2d, tree.getLength(), tree.getAngle(), tree.getThreshold(), tree.getStep(), tree.getStroke());
		progress = 100.0;
		message = "";
		running = false;
		notifyObservers();
		canvas.repaint();
		
	}

	/**
	 * Recursive definition of a fractal tree.
	 * 
	 * @param g2d The graphics context
	 * @param length The trunk length
	 * @param angle The angle at which to branch
	 * @param threshold The minimum branch length
	 * @param step The scalar value to multiply length by at each iteration
	 * @param stroke The stroke width of the lines drawn
	 */
	private void treeBranch(Graphics2D g2d, double length, double angle, double threshold, double step, double stroke) {

		Tree2D tree = (Tree2D) getFractal();
		double totalLength = tree.getLength();
		g2d.rotate(angle);
		g2d.setColor(AWTColorFactory.convert(getShader().getFragment(new Vec4d(1 - (length * step / totalLength), 0, 0, 0))));
		g2d.setStroke(new BasicStroke((float) stroke));
		g2d.drawLine(0, 0, 0, (int) -length);
		g2d.translate(0, -length);
		steps++;
		progress = (double) steps * 100.0 / (double) maxSteps;
		notifyObservers();

		if(length > threshold) {

			Graphics2D g2d_2 = (Graphics2D) g2d.create();
			treeBranch(g2d_2, length * step, angle, threshold, step, stroke);
			treeBranch(g2d, length * step, -angle, threshold, step, stroke);

		}

	}
	
}
