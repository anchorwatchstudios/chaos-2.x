package com.anchorwatchstudios.chaos.swing.task;

import java.awt.Graphics2D;

import com.anchorwatchstudios.chaos.document.Settings;
import com.anchorwatchstudios.chaos.document.fractal.EscapeTimeFractal;
import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.math.Vec4d;
import com.anchorwatchstudios.chaos.swing.AWTColorFactory;
import com.anchorwatchstudios.chaos.swing.canvas.Java2DCanvas;

/**
 * Renders a complex plot in Java2D.
 * 
 * @author Anthony Atella
 */
public class ComplexPlotJ2DTask extends J2DTask {

	private volatile boolean running;
	private volatile double progress;
	private volatile String message;

	/**
	 * Constructs an empty <code>ComplexPlotJ2DTask</code> with
	 * null values.
	 */
	public ComplexPlotJ2DTask() {
		
		super();
		
	}
	
	/**
	 * Constructs a new <code>ComplexPlotJ2DTask</code> with the given values.
	 * 
	 * @param fractal The <code>EscapeTime</code> fractal
	 * @param shader The <code>Shader</code>
	 * @param renderMethod The <code>RenderMethod</code>
	 * @param settings The <code>Document</code> settings
	 * @param canvas The <code>J2DCanvas</code>
	 */
	public ComplexPlotJ2DTask(EscapeTimeFractal<Complex> fractal, Shader shader, RenderMethod renderMethod, Settings settings, Java2DCanvas canvas) {

		super(fractal, shader, renderMethod, settings);
		this.canvas = canvas;
		running = false;
		progress = 100.0;
		message = "";

	}

	@Override
	public void run() {

		render();

	}

	@Override
	public void render() {

		Graphics2D g2d = canvas.getImage().createGraphics();
		running = true;
		progress = 0.0;
		message = "Rendering...";
		notifyObservers();
		int width = (int) getSettings().getResolution().getX();
		int height = (int) getSettings().getResolution().getY();
		int totalWork = width * height;
		int count = 0;

		for(int j = 0;j < height;j++) {

			if(!running) {

				break;

			}

			for(int i = 0;i < width;i++) {

				if(!running) {

					break;

				}
				
				Complex point = pixelToCoords(i, j, width, height);
				@SuppressWarnings("unchecked")
				Vec4d value = ((EscapeTimeFractal<Complex>) getFractal()).isInSet(point);
				g2d.setColor(AWTColorFactory.convert(getShader().getFragment(value)));
				g2d.drawOval(i, j, 0, 0);
				count++;
				progress = (double) count / (double) totalWork;

				if(progress == 1.0) {
					
					message = "";
					running = false;
					canvas.repaint();
					
				}
				
				notifyObservers();
				
			}

		}
		
	}

	@Override
	public void togglePause() {

		// Unimplemented

	}

	@Override
	public boolean isRunning() {

		return running;

	}

	@Override
	public boolean isPaused() {

		return false;

	}

	@Override
	public void stop() {

		running = false;
		progress = 100.0;
		message = "";

	}

	@Override
	public String getMessage() {

		return message;

	}

	@Override
	public double getProgress() {

		return progress;

	}

	/**
	 * Converts a pixel on an image to a coordinate on the graph.
	 * 
	 * @param x The x coordinate of the pixel
	 * @param y The y coordinate of the pixel
	 * @param width The width of the image
	 * @param height The height of the image
	 * @return A complex number on the graph representing the pixel on the image
	 */
	private Complex pixelToCoords(int x, int y, int width, int height) {

		ComplexPlot plot = (ComplexPlot) renderMethod;
		Complex max = plot.getMax();
		Complex min = plot.getMin();
		double aspect = (double) width / (double) height;
		double a = (double) (x * (max.getA() - min.getA()) / (double) width) + min.getA();	
		double b = (double) (y * (max.getB() - min.getB()) / (double) height) + min.getB();
		return new Complex(a * aspect, b);

	}

}
