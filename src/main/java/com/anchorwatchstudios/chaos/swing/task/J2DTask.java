package com.anchorwatchstudios.chaos.swing.task;

import com.anchorwatchstudios.chaos.document.Settings;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.swing.canvas.Java2DCanvas;
import com.anchorwatchstudios.chaos.task.PriorityTask;

/**
 * Abstractly renders a fractal with Java2D.
 * 
 * @author Anthony Atella
 */
public abstract class J2DTask extends PriorityTask {

	public static final int DEFAULT_PRIORITY = 100;
	protected Java2DCanvas canvas;
	protected Fractal fractal;
	protected Shader shader;
	protected RenderMethod renderMethod;
	protected Settings settings;
	
	/**
	 * Constructs an empty <code>J2DTask</code> with
	 * null values.
	 */
	public J2DTask() {
		
		this.canvas = null;
		this.fractal = null;
		this.shader = null;
		this.renderMethod = null;
		this.settings = null;
		setPriority(DEFAULT_PRIORITY);
		
	}
	
	/**
	 * Constructs a new <code>RenderTask</code> with the given values.
	 * 
	 * @param fractal The <code>Fractal</code>
	 * @param shader The <code>Shader</code>
	 * @param renderMethod The <code>renderMethod</code>
	 * @param settings The <code>Document</code> settings
	 */
	public J2DTask(Fractal fractal, Shader shader, RenderMethod renderMethod, Settings settings) {
		
		this.fractal = fractal;
		this.shader = shader;
		this.renderMethod = renderMethod;
		this.settings = settings;
		setPriority(DEFAULT_PRIORITY);
		
	}
	
	/**
	 * Returns the <code>Fractal</code>.
	 * 
	 * @return The <code>Fractal</code>
	 */
	public Fractal getFractal() {
		
		return fractal;
		
	}
	
	/**
	 * Sets the <code>Fractal</code> to the given <code>Fractal</code>.
	 * 
	 * @param fractal The <code>Fractal</code>
	 */
	public void setFractal(Fractal fractal) {
		
		this.fractal = fractal;
		
	}
	
	/**
	 * Returns the <code>Shader</code>.
	 * 
	 * @return The <code>Shader</code>
	 */
	public Shader getShader() {
		
		return shader;
		
	}
	
	/**
	 * Sets the <code>Shader</code> to the given <code>Shader</code>.
	 * 
	 * @param shader The <code>Shader</code>
	 */
	public void setShader(Shader shader) {
		
		this.shader = shader;
		
	}
	
	/**
	 * Returns the <code>RenderMethod</code>.
	 * 
	 * @return The <code>RenderMethod</code>
	 */
	public RenderMethod getRenderSettings() {
		
		return renderMethod;
		
	}
	
	/**
	 * Sets the <code>RenderMethod</code> to the given <code>RenderMethod</code>.
	 * 
	 * @param renderMethod The <code>RenderMethod</code>
	 */
	public void setRenderMethod(RenderMethod renderMethod) {
		
		this.renderMethod = renderMethod;
		
	}

	/**
	 * Returns the <code>Settings</code>.
	 * 
	 * @return The <code>Settings</code>
	 */
	public Settings getSettings() {
		
		return settings;
		
	}

	/**
	 * Sets the <code>Settings</code> to the given <code>Settings</code>.
	 * 
	 * @param settings The <code>Settings</code>
	 */
	public void setSettings(Settings settings) {
		
		this.settings = settings;
		
	}

	/**
	 * Sets the canvas.
	 * 
	 * @param canvas The canvas
	 */
	public void setCanvas(Java2DCanvas canvas) {

		this.canvas = canvas;
		
	}
	
	/**
	 * Renders the fractal.
	 */
	abstract protected void render();
	
}
