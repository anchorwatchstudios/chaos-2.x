package com.anchorwatchstudios.chaos.swing;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class PNGFileFilter extends FileFilter {

	@Override
	public boolean accept(File f) {

		return f.getName().endsWith(".png") || f.isDirectory();
		
	}

	@Override
	public String getDescription() {

		return "Portable network graphic (.png)";
		
	}

}
