package com.anchorwatchstudios.chaos.swing;

import java.awt.Color;
import java.awt.Font;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

import org.kordamp.ikonli.fontawesome.FontAwesome;
import org.kordamp.ikonli.swing.FontIcon;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.render.RenderImplementation;
import com.anchorwatchstudios.chaos.reflect.ReflectionUtils;
import com.anchorwatchstudios.chaos.swing.event.Action;

/**
 * The menu bar.
 * 
 * @author Anthony Atella
 */
public class MenuBar extends JMenuBar implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private static final int ICON_SIZE = 16;
	private JMenuItem closeItem;
	private JCheckBoxMenuItem debugItem;
	private Chaos context;
	private JMenuItem exportVideoItem;
	private AbstractButton exportImageItem;
	private AbstractButton saveAsItem;
	private JMenuItem saveItem;
	private JRadioButtonMenuItem java2DItem;
	private JRadioButtonMenuItem openGL4Item;

	/**
	 * Constructs a new <code>MenuBar</code>.
	 * 
	 * @param context The program context
	 */
	public MenuBar(Chaos context) {

		this.context = context;
		context.getDocumentManager().registerObserver(this);
		Font font = new Font("Arial", Font.PLAIN, 24);
		JMenu fileMenu = new JMenu(Strings.MENU_FILE);
		fileMenu.setFont(font);
		FontIcon fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FILE_O);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JMenu newMenu = new JMenu(Strings.MENU_NEW);
		newMenu.setFont(font);
		newMenu.setIcon(fontIcon);
		Class<?>[] classes = ReflectionUtils.sortByName(ReflectionUtils.getSubClasses("com.anchorwatchstudios.chaos.document.fractal", Fractal.class));
		String[] classNames = ReflectionUtils.getSimpleClassNames(classes);

		for(int i = 0;i < classNames.length;i++) {

			try {

				JMenuItem menuItem = new JMenuItem(classNames[i], fontIcon);
				menuItem.addActionListener(new Action.New(context, (Fractal) ((Class<?>) classes[i]).newInstance()));
				menuItem.setFont(font);
				newMenu.add(menuItem);

			} catch (Exception e) {

			}

		}

		fileMenu.add(newMenu);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FOLDER_O);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JMenuItem openItem = new JMenuItem(Strings.MENU_OPEN, fontIcon);
		openItem.setFont(font);
		openItem.addActionListener(new Action.Open(context));
		fileMenu.add(openItem);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.CLOSE);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		closeItem = new JMenuItem(Strings.MENU_CLOSE, fontIcon);
		closeItem.setFont(font);
		closeItem.addActionListener(new Action.Close(context));
		closeItem.setEnabled(false);
		fileMenu.add(closeItem);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FLOPPY_O);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		saveItem = new JMenuItem(Strings.MENU_SAVE, fontIcon);
		saveItem.setFont(font);
		saveItem.addActionListener(new Action.Save(context));
		fileMenu.add(saveItem);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FLOPPY_O);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		saveAsItem = new JMenuItem(Strings.MENU_SAVE_AS, fontIcon);
		saveAsItem.setFont(font);
		saveAsItem.addActionListener(new Action.SaveAs(context));
		fileMenu.add(saveAsItem);
		fileMenu.addSeparator();
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FILE_IMAGE_O);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		exportImageItem = new JMenuItem(Strings.MENU_EXPORT_IMAGE, fontIcon);
		exportImageItem.setFont(font);
		exportImageItem.addActionListener(new Action.ExportImage(context));
		fileMenu.add(exportImageItem);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FILE_MOVIE_O);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		exportVideoItem = new JMenuItem(Strings.MENU_EXPORT_VIDEO, fontIcon);
		exportVideoItem.setFont(font);
		exportVideoItem.addActionListener(new Action.ExportVideo(context));
		fileMenu.add(exportVideoItem);
		fileMenu.addSeparator();
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.WINDOW_CLOSE);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JMenuItem exitItem = new JMenuItem(Strings.MENU_EXIT, fontIcon);
		exitItem.setFont(font);
		exitItem.addActionListener(new Action.Exit(context));
		fileMenu.add(exitItem);
		add(fileMenu);
		JMenu viewMenu = new JMenu(Strings.MENU_VIEW);
		viewMenu.setFont(font);
		JMenu impMenu = new JMenu(Strings.MENU_RENDER_IMP);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.ROCKET);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		impMenu.setIcon(fontIcon);
		impMenu.setFont(font);
		ButtonGroup buttonGroup = new ButtonGroup();
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.ROCKET);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		java2DItem = new JRadioButtonMenuItem(RenderImplementation.JAVA2D.toString(), fontIcon);
		java2DItem.setFont(font);
		java2DItem.addActionListener(new Action.SetRenderImplementation(context, RenderImplementation.JAVA2D));
		buttonGroup.add(java2DItem);
		impMenu.add(java2DItem);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.ROCKET);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		openGL4Item = new JRadioButtonMenuItem(RenderImplementation.OPENGL4.toString(), fontIcon);
		openGL4Item.setFont(font);
		openGL4Item.addActionListener(new Action.SetRenderImplementation(context, RenderImplementation.OPENGL4));
		buttonGroup.add(openGL4Item);
		impMenu.add(openGL4Item);
		viewMenu.add(impMenu);
		add(viewMenu);
		JMenu windowMenu = new JMenu(Strings.MENU_WINDOW);
		windowMenu.setFont(font);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.EYE);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JMenu showHideMenu = new JMenu(Strings.MENU_SHOW_HIDE);
		showHideMenu.setFont(font);
		showHideMenu.setIcon(fontIcon);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.WRENCH);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JCheckBoxMenuItem toolbarItem = new JCheckBoxMenuItem(Strings.MENU_TOOLBAR, fontIcon);
		toolbarItem.setFont(font);
		toolbarItem.setSelected(true);
		toolbarItem.addActionListener(new Action.ToggleToolbar(context));
		showHideMenu.add(toolbarItem);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.INFO);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JCheckBoxMenuItem rolloutItem = new JCheckBoxMenuItem(Strings.MENU_ROLLOUT, fontIcon);
		rolloutItem.setFont(font);
		rolloutItem.setSelected(true);
		rolloutItem.addActionListener(new Action.ToggleRollout(context));
		showHideMenu.add(rolloutItem);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.BUG);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		debugItem = new JCheckBoxMenuItem(Strings.MENU_DEBUG, fontIcon);
		debugItem.setFont(font);
		debugItem.setSelected(false);
		debugItem.addActionListener(new Action.ToggleDebug(context));
		showHideMenu.add(debugItem);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.FILM);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JCheckBoxMenuItem timelineItem = new JCheckBoxMenuItem(Strings.MENU_TIMELINE, fontIcon);
		timelineItem.setFont(font);
		timelineItem.setSelected(true);
		timelineItem.addActionListener(new Action.ToggleTimeline(context));
		showHideMenu.add(timelineItem);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.TACHOMETER);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JCheckBoxMenuItem statusBarItem = new JCheckBoxMenuItem(Strings.MENU_STATUS_BAR, fontIcon);
		statusBarItem.setFont(font);
		statusBarItem.setSelected(true);
		statusBarItem.addActionListener(new Action.ToggleStatusBar(context));
		showHideMenu.add(statusBarItem);
		windowMenu.add(showHideMenu);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.KEY);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JCheckBoxMenuItem lockToolbarItem = new JCheckBoxMenuItem(Strings.MENU_TOOLBAR_LOCK, fontIcon);
		lockToolbarItem.setFont(font);
		lockToolbarItem.addActionListener(new Action.ToggleToolbarLock(context));
		windowMenu.add(lockToolbarItem);
		add(windowMenu);
		JMenu helpMenu = new JMenu(Strings.MENU_HELP);
		helpMenu.setFont(font);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.QUESTION_CIRCLE);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JMenuItem contentsItem = new JMenuItem(Strings.MENU_CONTENTS, fontIcon);
		contentsItem.setFont(font);
		contentsItem.addActionListener(new Action.ShowHelp(context));
		helpMenu.add(contentsItem);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.INFO_CIRCLE);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JMenuItem aboutItem = new JMenuItem(Strings.MENU_ABOUT, fontIcon);
		aboutItem.setFont(font);
		aboutItem.addActionListener(new Action.ShowAbout(context));
		helpMenu.add(aboutItem);
		fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.GAMEPAD);
		fontIcon.setIconSize(ICON_SIZE);
		fontIcon.setIconColor(Color.DARK_GRAY);
		JMenuItem controlsItem = new JMenuItem(Strings.MENU_CONTROLS, fontIcon);
		controlsItem.setFont(font);
		controlsItem.addActionListener(new Action.ShowControls(context));
		helpMenu.add(controlsItem);
		add(helpMenu);
		notify(context.getDocumentManager());

	}

	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();

		if(document == null) {

			closeItem.setEnabled(false);
			exportImageItem.setEnabled(false);
			exportVideoItem.setEnabled(false);
			saveItem.setEnabled(false);
			saveAsItem.setEnabled(false);
			java2DItem.setEnabled(false);
			openGL4Item.setEnabled(false);

		}
		else {

			closeItem.setEnabled(true);
			exportImageItem.setEnabled(true);
			exportVideoItem.setEnabled(true);
			saveItem.setEnabled(true);
			saveAsItem.setEnabled(true);

			if(context.getWindow().getDebugArea().isVisible()) {

				debugItem.setSelected(true);

			}
			else {

				debugItem.setSelected(false);

			}

			if(document.getSettings().getRenderImplementation() == RenderImplementation.JAVA2D) {

				java2DItem.setSelected(true);

			}
			else {

				openGL4Item.setSelected(true);

			}

			RenderImplementation[] caps = document.getCurrentKeyFrame().getRenderMethod().getCapabilities();
			java2DItem.setEnabled(false);
			openGL4Item.setEnabled(false);
			
			for(int i = 0;i < caps.length;i++) {

				switch (caps[i]) {

				case JAVA2D:
					java2DItem.setEnabled(true);
					break;
					
				case OPENGL4:
					openGL4Item.setEnabled(true);
					break;
					
				default:
					break;
					
				}
				
			}

		}

	}

}
