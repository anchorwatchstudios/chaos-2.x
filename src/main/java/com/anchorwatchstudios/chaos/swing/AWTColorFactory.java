package com.anchorwatchstudios.chaos.swing;

import java.awt.Color;

/**
 * Converts Chaos colors to AWT colors and vice versa.
 * 
 * @author Anthony Atella
 */
public class AWTColorFactory {

	/**
	 * This class is a factory.
	 */
	private AWTColorFactory() {
		
	}
	
	/**
	 * Returns an AWT color from a Chaos color.
	 * 
	 * @param color The Chaos color to be converted
	 * @return An AWT color
	 */
	public static Color convert(com.anchorwatchstudios.chaos.document.shader.Color color) {
		
		return new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
		
	}
	
	/**
	 * Returns a Chaos color from an AWT color.
	 * 
	 * @param color The AWT color to be converted
	 * @return A Chaos color
	 */
	public static com.anchorwatchstudios.chaos.document.shader.Color convert(Color color) {
		
		return new com.anchorwatchstudios.chaos.document.shader.Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
		
	}
	
}
