package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.shader.RandomShader;
import com.anchorwatchstudios.chaos.document.shader.Shader;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate a <code>RandomShader</code>.
 * 
 * @author Anthony Atella
 */
public class RandomShaderControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private JCheckBox invertCheckBox;

	/**
	 * Constructs a new <code>RandomShaderControls</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public RandomShaderControls(DocumentManager documentManager) {

		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		Font font = new Font("Arial", Font.PLAIN, 24);
		initInvertCheckbox(documentManager, padding, font);

	}

	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();

		if(document != null) {

			Shader shader = document.getCurrentKeyFrame().getShader();
			
			if(shader instanceof RandomShader) {
				
				RandomShader rShader = (RandomShader) shader;
				invertCheckBox.setSelected(rShader.isInverted());

			}
			
		}
		
	}
	
	/**
	 * Initializes the invert checkbox.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initInvertCheckbox(DocumentManager documentManager, Border padding, Font font) {
		
		invertCheckBox = new JCheckBox();
		invertCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				FRC2Document document = (FRC2Document) documentManager.getCurrent();
				RandomShader shader = (RandomShader) document.getCurrentKeyFrame().getShader();
				shader.setInverted(!shader.isInverted());
				document.setEdited(true);
				
			}
			
		});
		invertCheckBox.setBorder(padding);
		invertCheckBox.setAlignmentX(JCheckBox.CENTER_ALIGNMENT);
		JLabel discreteLabel = new JLabel(Strings.EDITOR_INVERT);
		discreteLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		discreteLabel.setFont(font);
		add(discreteLabel);
		add(invertCheckBox);
		
	}

}