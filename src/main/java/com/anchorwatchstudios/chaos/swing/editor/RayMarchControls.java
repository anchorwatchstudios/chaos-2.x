package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.RayMarch;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.swing.DoubleSlider;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate a <code>RayMarch</code> object.
 * 
 * @author Anthony Atella
 */
public class RayMarchControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private static final int MIN_ITERATIONS = 0;
	private static final int MAX_ITERATIONS = 500;
	private static final double MIN_RAY_LENGTH = 0;
	private static final double MAX_RAY_LENGTH = 0.05;
	private static final int SEPARATOR_SIZE = 32;
	private static final int PRECISION = 5;
	private JSlider iterationsSlider;
	private DoubleSlider rayThresholdSlider;

	/**
	 * Constructs a new <code>RayMarchControls</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public RayMarchControls(DocumentManager documentManager) {

		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		Font font = new Font("Arial", Font.PLAIN, 24);
		Font font2 = new Font("Arial", Font.PLAIN, 20);
		initHeader(padding, font);
		initIterationsSlider(documentManager, padding, font2);
		initRayThresholdSlider(documentManager, padding, font2);

	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {

			RenderMethod renderMethod = document.getCurrentKeyFrame().getRenderMethod();
			
			if(renderMethod instanceof RayMarch) {
				
				RayMarch rms = (RayMarch) renderMethod;
				iterationsSlider.setValue(rms.getIterations());
				rayThresholdSlider.setDoubleValue(rms.getRayThreshold());
			
			}
		}
		
	}
	
	/**
	 * Initializes the header.
	 * 
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initHeader(Border padding, Font font) {

		JLabel title = new JLabel(RayMarch.class.getSimpleName());
		title.setFont(font);
		title.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(title);
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, SEPARATOR_SIZE));
		add(separator);
		
	}
	
	/**
	 * Initializes the iterations slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initIterationsSlider(DocumentManager documentManager, Border padding, Font font) {
		
		iterationsSlider = new JSlider(JSlider.HORIZONTAL, MIN_ITERATIONS, MAX_ITERATIONS, (MAX_ITERATIONS - MIN_ITERATIONS) / 2);
		iterationsSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!iterationsSlider.getValueIsAdjusting()) {

					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					RayMarch rms = (RayMarch) document.getCurrentKeyFrame().getRenderMethod();
					rms.setIterations(iterationsSlider.getValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		iterationsSlider.setMinorTickSpacing((int) (MAX_ITERATIONS - MIN_ITERATIONS / 100.0));
		iterationsSlider.setMajorTickSpacing((int) (MAX_ITERATIONS - MIN_ITERATIONS / 10.0));
		iterationsSlider.setLabelTable(iterationsSlider.createStandardLabels((int) (MAX_ITERATIONS / 2)));
		iterationsSlider.setPaintLabels(true);
		iterationsSlider.setPaintTicks(true);
		iterationsSlider.setBorder(padding);
		iterationsSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel iterationsLabel = new JLabel(Strings.EDITOR_ITERATIONS);
		iterationsLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		iterationsLabel.setFont(font);
		add(iterationsLabel);
		add(iterationsSlider);
		
	}
	
	/**
	 * Initializes the ray threshold slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initRayThresholdSlider(DocumentManager documentManager, Border padding, Font font) {
		
		rayThresholdSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_RAY_LENGTH, MAX_RAY_LENGTH, (MAX_RAY_LENGTH - MIN_RAY_LENGTH) / 2.0, PRECISION);
		rayThresholdSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!rayThresholdSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					RayMarch rms = (RayMarch) document.getCurrentKeyFrame().getRenderMethod();
					rms.setRayThreshold(rayThresholdSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		rayThresholdSlider.setMinorTickSpacing(MAX_RAY_LENGTH / 100);
		rayThresholdSlider.setMajorTickSpacing(MAX_RAY_LENGTH / 10);
		rayThresholdSlider.setPaintTicks(true);
		rayThresholdSlider.setLabelTable(rayThresholdSlider.createStandardLabels(MAX_RAY_LENGTH));
		rayThresholdSlider.setPaintLabels(true);
		rayThresholdSlider.setBorder(padding);
		rayThresholdSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel label = new JLabel(Strings.EDITOR_RAY_THRESHOLD);
		label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		label.setFont(font);
		add(label);
		add(rayThresholdSlider);
		
	}
	
}