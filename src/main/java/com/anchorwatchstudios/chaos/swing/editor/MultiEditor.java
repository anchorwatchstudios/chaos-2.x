package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;

import com.anchorwatchstudios.chaos.reflect.ReflectionUtils;
import com.anchorwatchstudios.chaos.swing.Chaos;

/**
 * An <code>Editor</code> that contains a combo box containing
 * sub-classes from a given package of a given class. The
 * <code>Editor</code> displays the appropriate panel associated
 * with the selected class. For switching <code>Document</code>
 * elements on the fly.
 * 
 * @author Anthony Atella
 */
public abstract class MultiEditor extends Editor {

	private static final long serialVersionUID = 1L;
	protected JComboBox<Object> comboBox;
	
	/**
	 * Constructs a new <code>MultiEditor</code> with the given package
	 * and base class.
	 * 
	 * @param context The program context
	 * @param pckg The package to look for sub-classes in
	 * @param clazz The base class to look for implementations of
	 */
	public MultiEditor(Chaos context, String pckg, Class<?> clazz) {
		
		super(context);
		comboBox = new JComboBox<Object>(ReflectionUtils.getSimpleClassNames(ReflectionUtils.getSubClasses(pckg, clazz)));
		Font font = new Font("Arial", Font.PLAIN, 24);
		comboBox.setFont(font);
		comboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {

				if(e.getStateChange() == ItemEvent.SELECTED) {
					
					onComboBoxActionPerformed(e, comboBox);
					
				}

			}
			
		});
		comboBox.setVisible(false);
		add(comboBox, BorderLayout.NORTH);
		
	}

	/**
	 * Fires when the combo box selection has been changed.
	 * 
	 * @param e The <code>ItemEvent</code>
	 * @param comboBox The combo box
	 */
	public abstract void onComboBoxActionPerformed(ItemEvent e, JComboBox<Object> comboBox);
	
	/**
	 * Sets the combo box visibility to the given visibility.
	 * 
	 * @param visible Whether or not the combo box is visible
	 */
	public void setComboBoxVisible(boolean visible) {
	
		comboBox.setVisible(visible);
		
	}
	
}
