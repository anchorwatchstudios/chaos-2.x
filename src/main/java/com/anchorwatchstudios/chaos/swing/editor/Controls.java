package com.anchorwatchstudios.chaos.swing.editor;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.Border;

import com.anchorwatchstudios.chaos.swing.DoubleSlider;

public class Controls extends JPanel {

	private static final long serialVersionUID = 1L;

	protected DoubleSlider initDoubleSlider(double min, double max, int precision, String label, Border padding) {

		DoubleSlider slider = new DoubleSlider(JSlider.HORIZONTAL, min, max, (max - min) / 2.0, precision);
		slider.setMinorTickSpacing(max / 100);
		slider.setMajorTickSpacing(max / 10);
		slider.setPaintTicks(true);
		slider.setLabelTable(slider.createStandardLabels(max));
		slider.setPaintLabels(true);
		slider.setBorder(padding);
		slider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel l = new JLabel(label);
		l.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(l);
		return slider;

	}

}
