package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.event.ItemEvent;

import javax.swing.JComboBox;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.shader.DichromaticShader;
import com.anchorwatchstudios.chaos.document.shader.MinDistanceShader;
import com.anchorwatchstudios.chaos.document.shader.MonochromaticShader;
import com.anchorwatchstudios.chaos.document.shader.RandomShader;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.swing.Chaos;
import com.anchorwatchstudios.chaos.swing.event.Action;

/**
 * An <code>Editor</code> that contains all shader
 * editor controls.
 * 
 * @author Anthony Atella
 */
public class ShaderEditor extends MultiEditor {

	private static final long serialVersionUID = 1L;
	private Chaos context;
	private boolean comboBoxEnabled;

	/**
	 * Constructs a new <code>ShaderEditor</code>.
	 * 
	 * @param context The program context
	 */
	public ShaderEditor(Chaos context) {

		super(context, "com.anchorwatchstudios.chaos.document.shader", Shader.class);
		this.context = context;
		comboBoxEnabled = true;
		DocumentManager documentManager = context.getDocumentManager();
		RandomShaderControls randomShaderEditor = new RandomShaderControls(documentManager);
		MonochromaticShaderControls monoEditor = new MonochromaticShaderControls(documentManager);
		DichromaticShaderControls dichroEditor = new DichromaticShaderControls(documentManager);
		MinDistanceShaderControls minDistEditor = new MinDistanceShaderControls(documentManager);
		documentManager.registerObserver(randomShaderEditor);
		documentManager.registerObserver(monoEditor);
		documentManager.registerObserver(dichroEditor);
		documentManager.registerObserver(minDistEditor);
		addPanel(RandomShader.class.toString(), randomShaderEditor);
		addPanel(MonochromaticShader.class.toString(), monoEditor);
		addPanel(DichromaticShader.class.toString(), dichroEditor);
		addPanel(MinDistanceShader.class.toString(), minDistEditor);
		
	}

	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();

		if(document != null) {

			setComboBoxVisible(true);
			comboBoxEnabled = false;
			comboBox.setSelectedItem(document.getCurrentKeyFrame().getShader().getClass().getSimpleName());
			comboBoxEnabled = true;
			showPanel(document.getCurrentKeyFrame().getShader().getClass().toString());

		}
		else {

			setComboBoxVisible(false);
			showPanel(Editor.EMPTY_PANEL);

		}

	}

	@Override
	public void onComboBoxActionPerformed(ItemEvent e, JComboBox<Object> comboBox) {

		if(comboBoxEnabled) {

			String item = (String) comboBox.getSelectedItem();

			if(item.equals(RandomShader.class.getSimpleName())) {

				new Action.SetShader(context, new RandomShader()).actionPerformed(null);

			}
			else if(item.equals(DichromaticShader.class.getSimpleName())) {

				new Action.SetShader(context, new DichromaticShader()).actionPerformed(null);

			}
			else if(item.equals(MonochromaticShader.class.getSimpleName())) {

				new Action.SetShader(context, new MonochromaticShader()).actionPerformed(null);

			}
			else if(item.equals(MinDistanceShader.class.getSimpleName())) {

				new Action.SetShader(context, new MinDistanceShader()).actionPerformed(null);

			}

		}

	}

}
