package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbulb;
import com.anchorwatchstudios.chaos.math.Triplex;
import com.anchorwatchstudios.chaos.swing.DoubleSlider;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate a <code>Mandelbulb</code> set.
 * 
 * @author Anthony Atella
 */
public class MandelbulbControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private static final int MIN_ITERATIONS = 0;
	private static final int MAX_ITERATIONS = 500;
	private static final double MIN_THRESHOLD = 0;
	private static final double MAX_THRESHOLD = 10.0;
	private static final double MIN_Z = -10.0;
	private static final double MAX_Z = 10.0;
	private static final double MIN_N = 0;
	private static final double MAX_N = 16;
	private static final int PRECISION = 1;
	private static final int SEPARATOR_SIZE = 32;
	private JSlider iterationsSlider;
	private DoubleSlider thresholdSlider;
	private DoubleSlider zASlider;
	private DoubleSlider zBSlider;
	private DoubleSlider zCSlider;
	private DoubleSlider nSlider;

	/**
	 * Constructs a new <code>Mandelbulb</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public MandelbulbControls(DocumentManager documentManager) {

		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		initHeader(padding);
		initIterationsSlider(documentManager, padding);
		initThresholdSlider(documentManager, padding);
		initZASlider(documentManager, padding);
		initZBSlider(documentManager, padding);
		initZCSlider(documentManager, padding);
		initNSlider(documentManager, padding);

	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {

			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Mandelbulb) {
			
				Mandelbulb mandelbulb = (Mandelbulb) fractal;
				iterationsSlider.setValue(mandelbulb.getIterations());
				thresholdSlider.setDoubleValue(mandelbulb.getThreshold());
				zASlider.setDoubleValue(mandelbulb.getZ().getA());
				zBSlider.setDoubleValue(mandelbulb.getZ().getB());
				zCSlider.setDoubleValue(mandelbulb.getZ().getC());
				nSlider.setDoubleValue(mandelbulb.getN());
			
			}
			
		}
		
	}
	
	/**
	 * Initializes the header.
	 * 
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initHeader(Border padding) {

		JLabel title = new JLabel(Mandelbulb.class.getSimpleName());
		Font font = new Font("Arial", Font.PLAIN, 24);
		title.setFont(font);
		title.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(title);
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, SEPARATOR_SIZE));
		add(separator);
		
	}
	
	/**
	 * Initializes the iterations slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initIterationsSlider(DocumentManager documentManager, Border padding) {
		
		iterationsSlider = new JSlider(JSlider.HORIZONTAL, MIN_ITERATIONS, MAX_ITERATIONS, (MAX_ITERATIONS - MIN_ITERATIONS) / 2);
		iterationsSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!iterationsSlider.getValueIsAdjusting()) {

					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbulb mandelbrot = (Mandelbulb) document.getCurrentKeyFrame().getFractal();
					mandelbrot.setIterations(iterationsSlider.getValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		iterationsSlider.setMinorTickSpacing((int) (MAX_ITERATIONS - MIN_ITERATIONS / 100.0));
		iterationsSlider.setMajorTickSpacing((int) (MAX_ITERATIONS - MIN_ITERATIONS / 10.0));
		iterationsSlider.setLabelTable(iterationsSlider.createStandardLabels((int) (MAX_ITERATIONS / 2)));
		iterationsSlider.setPaintLabels(true);
		iterationsSlider.setPaintTicks(true);
		iterationsSlider.setBorder(padding);
		iterationsSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel iterationsLabel = new JLabel(Strings.EDITOR_ITERATIONS);
		iterationsLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(iterationsLabel);
		add(iterationsSlider);
		
	}
	
	/**
	 * Initializes the threshold slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initThresholdSlider(DocumentManager documentManager, Border padding) {
		
		thresholdSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_THRESHOLD, MAX_THRESHOLD, (MAX_THRESHOLD - MIN_THRESHOLD) / 2.0, PRECISION);
		thresholdSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!thresholdSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbulb mandelbrot = (Mandelbulb) document.getCurrentKeyFrame().getFractal();
					mandelbrot.setThreshold(thresholdSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		thresholdSlider.setMinorTickSpacing(MAX_THRESHOLD / 100);
		thresholdSlider.setMajorTickSpacing(MAX_THRESHOLD / 10);
		thresholdSlider.setPaintTicks(true);
		thresholdSlider.setLabelTable(thresholdSlider.createStandardLabels(MAX_THRESHOLD));
		thresholdSlider.setPaintLabels(true);
		thresholdSlider.setBorder(padding);
		thresholdSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel thresholdLabel = new JLabel(Strings.EDITOR_THRESHOLD);
		thresholdLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(thresholdLabel);
		add(thresholdSlider);
		
	}
	
	/**
	 * Initializes the Za slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initZASlider(DocumentManager documentManager, Border padding) {
		
		zASlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_Z, MAX_Z, (MAX_Z - MIN_Z) / 2.0, PRECISION);
		zASlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!zASlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbulb mandelbrot = (Mandelbulb) document.getCurrentKeyFrame().getFractal();
					Triplex c = mandelbrot.getZ();
					c.setA(zASlider.getDoubleValue());
					mandelbrot.setZ(c);
					document.setEdited(true);
										
				}
				
			}
			
		});
		zASlider.setMinorTickSpacing(MAX_Z / 100);
		zASlider.setMajorTickSpacing(MAX_Z / 10);
		zASlider.setPaintTicks(true);
		zASlider.setLabelTable(zASlider.createStandardLabels(MAX_Z));
		zASlider.setPaintLabels(true);
		zASlider.setBorder(padding);
		zASlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel caLabel = new JLabel(Strings.EDITOR_ZA);
		caLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(caLabel);
		add(zASlider);
		
	}
	
	/**
	 * Initializes the Zb slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initZBSlider(DocumentManager documentManager, Border padding) {
		
		zBSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_Z, MAX_Z, (MAX_Z - MIN_Z) / 2.0, PRECISION);
		zBSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!zBSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbulb mandelbrot = (Mandelbulb) document.getCurrentKeyFrame().getFractal();
					Triplex c = mandelbrot.getZ();
					c.setB(zBSlider.getDoubleValue());
					mandelbrot.setZ(c);
					document.setEdited(true);
										
				}
				
			}
			
		});
		zBSlider.setMinorTickSpacing(MAX_Z / 100);
		zBSlider.setMajorTickSpacing(MAX_Z / 10);
		zBSlider.setPaintTicks(true);
		zBSlider.setLabelTable(zBSlider.createStandardLabels(MAX_Z));
		zBSlider.setPaintLabels(true);
		zBSlider.setBorder(padding);
		zBSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel cbLabel = new JLabel(Strings.EDITOR_ZB);
		cbLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(cbLabel);
		add(zBSlider);
		
	}
	
	/**
	 * Initializes the Zc slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initZCSlider(DocumentManager documentManager, Border padding) {
		
		zCSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_Z, MAX_Z, (MAX_Z - MIN_Z) / 2.0, PRECISION);
		zCSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!zCSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbulb mandelbrot = (Mandelbulb) document.getCurrentKeyFrame().getFractal();
					Triplex c = mandelbrot.getZ();
					c.setC(zCSlider.getDoubleValue());
					mandelbrot.setZ(c);
					document.setEdited(true);
										
				}
				
			}
			
		});
		zCSlider.setMinorTickSpacing(MAX_Z / 100);
		zCSlider.setMajorTickSpacing(MAX_Z / 10);
		zCSlider.setPaintTicks(true);
		zCSlider.setLabelTable(zCSlider.createStandardLabels(MAX_Z));
		zCSlider.setPaintLabels(true);
		zCSlider.setBorder(padding);
		zCSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel ccLabel = new JLabel(Strings.EDITOR_ZC);
		ccLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(ccLabel);
		add(zCSlider);
		
	}
	
	/**
	 * Initializes the n slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initNSlider(DocumentManager documentManager, Border padding) {
		
		nSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_N, MAX_N, (MAX_N - MIN_N) / 2.0, PRECISION);
		nSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!nSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbulb mandelbrot = (Mandelbulb) document.getCurrentKeyFrame().getFractal();
					mandelbrot.setN(nSlider.getDoubleValue());
					document.setEdited(true);
										
				}
				
			}
			
		});
		nSlider.setMinorTickSpacing(0.25);
		nSlider.setMajorTickSpacing(1.0);
		nSlider.setPaintTicks(true);
		nSlider.setLabelTable(nSlider.createStandardLabels(MAX_N));
		nSlider.setPaintLabels(true);
		nSlider.setBorder(padding);
		nSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel zDLabel = new JLabel(Strings.EDITOR_N);
		zDLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(zDLabel);
		add(nSlider);
		
	}
	
}