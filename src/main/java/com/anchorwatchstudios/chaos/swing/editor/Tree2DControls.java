package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Tree2D;
import com.anchorwatchstudios.chaos.swing.DoubleSlider;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate a <code>Tree2D</code>.
 * 
 * @author Anthony Atella
 */
public class Tree2DControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private static final double MIN_THRESHOLD = 0;
	private static final double MAX_THRESHOLD = 10.0;
	private static final double MIN_LENGTH = 0.0;
	private static final double MAX_LENGTH = 500.0;
	private static final double MIN_ANGLE = 0.0;
	private static final double MAX_ANGLE = Math.PI;
	private static final double MIN_STEP = 0.25;
	private static final double MAX_STEP = 0.75;
	private static final double MIN_STROKE = 0;
	private static final double MAX_STROKE = 100;
	private static final int PRECISION = 2;
	private static final int SEPARATOR_SIZE = 32;
	private DoubleSlider lengthSlider;
	private DoubleSlider thresholdSlider;
	private DoubleSlider angleSlider;
	private DoubleSlider stepSlider;
	private DoubleSlider strokeSlider;
	
	/**
	 * Constructs a new <code>Tree2D</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public Tree2DControls(DocumentManager documentManager) {

		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		Font font = new Font("Arial", Font.PLAIN, 24);
		Font font2 = new Font("Arial", Font.PLAIN, 20);
		initHeader(padding, font);
		initLengthSlider(documentManager, padding, font2);
		initThresholdSlider(documentManager, padding, font2);
		initAngleSlider(documentManager, padding, font2);
		initStepSlider(documentManager, padding, font2);
		initStrokeSlider(documentManager, padding, font2);

	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {

			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Tree2D) {
				
				Tree2D tree = (Tree2D) fractal;
				thresholdSlider.setDoubleValue(tree.getThreshold());
				lengthSlider.setDoubleValue(tree.getLength());
				angleSlider.setDoubleValue(tree.getAngle());
				stepSlider.setDoubleValue(tree.getStep());
				strokeSlider.setDoubleValue(tree.getStroke());
				
			}
			
		}

	}
	
	/**
	 * Initializes the header.
	 * 
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initHeader(Border padding, Font font) {

		JLabel title = new JLabel(Tree2D.class.getSimpleName());
		title.setFont(font);
		title.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(title);
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, SEPARATOR_SIZE));
		add(separator);
		
	}
	
	/**
	 * Initializes the length slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initLengthSlider(DocumentManager documentManager, Border padding, Font font) {
		
		lengthSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_LENGTH, MAX_LENGTH, (MAX_LENGTH - MIN_LENGTH) / 2.0, PRECISION);
		lengthSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!lengthSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Tree2D tree = (Tree2D) document.getCurrentKeyFrame().getFractal();
					tree.setLength(lengthSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		lengthSlider.setMinorTickSpacing(MAX_LENGTH / 100);
		lengthSlider.setMajorTickSpacing(MAX_LENGTH / 10);
		lengthSlider.setPaintTicks(true);
		lengthSlider.setLabelTable(lengthSlider.createStandardLabels(MAX_LENGTH));
		lengthSlider.setPaintLabels(true);
		lengthSlider.setBorder(padding);
		lengthSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel lengthLabel = new JLabel(Strings.EDITOR_LENGTH);
		lengthLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		lengthLabel.setFont(font);
		add(lengthLabel);
		add(lengthSlider);
		
	}
	
	/**
	 * Initializes the threshold slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initThresholdSlider(DocumentManager documentManager, Border padding, Font font) {
		
		thresholdSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_THRESHOLD, MAX_THRESHOLD, (MAX_THRESHOLD - MIN_THRESHOLD) / 2.0, PRECISION);
		thresholdSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!thresholdSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Tree2D tree = (Tree2D) document.getCurrentKeyFrame().getFractal();
					tree.setThreshold(thresholdSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		thresholdSlider.setMinorTickSpacing(MAX_THRESHOLD / 100);
		thresholdSlider.setMajorTickSpacing(MAX_THRESHOLD / 10);
		thresholdSlider.setPaintTicks(true);
		thresholdSlider.setLabelTable(thresholdSlider.createStandardLabels(MAX_THRESHOLD));
		thresholdSlider.setPaintLabels(true);
		thresholdSlider.setBorder(padding);
		thresholdSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel thresholdLabel = new JLabel(Strings.EDITOR_THRESHOLD);
		thresholdLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		thresholdLabel.setFont(font);
		add(thresholdLabel);
		add(thresholdSlider);
		
	}
	
	/**
	 * Initializes the angle slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initAngleSlider(DocumentManager documentManager, Border padding, Font font) {
		
		angleSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_ANGLE, MAX_ANGLE, (MAX_ANGLE - MIN_ANGLE) / 2.0, PRECISION);
		angleSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!angleSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Tree2D tree = (Tree2D) document.getCurrentKeyFrame().getFractal();
					tree.setAngle(angleSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		angleSlider.setMinorTickSpacing(MAX_ANGLE / 100);
		angleSlider.setMajorTickSpacing(MAX_ANGLE / 10);
		angleSlider.setPaintTicks(true);
		angleSlider.setLabelTable(angleSlider.createStandardLabels(MAX_ANGLE));
		angleSlider.setPaintLabels(true);
		angleSlider.setBorder(padding);
		angleSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel angleLabel = new JLabel(Strings.EDITOR_ANGLE);
		angleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		angleLabel.setFont(font);
		add(angleLabel);
		add(angleSlider);
		
	}
	
	/**
	 * Initializes the step slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initStepSlider(DocumentManager documentManager, Border padding, Font font) {
		
		stepSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_STEP, MAX_STEP, (MAX_STEP - MIN_STEP) / 2.0, PRECISION);
		stepSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!stepSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Tree2D tree = (Tree2D) document.getCurrentKeyFrame().getFractal();
					tree.setStep(stepSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		stepSlider.setMinorTickSpacing(MAX_STEP / 100);
		stepSlider.setMajorTickSpacing(MAX_STEP / 10);
		stepSlider.setPaintTicks(true);
		stepSlider.setLabelTable(stepSlider.createStandardLabels(MAX_STEP));
		stepSlider.setPaintLabels(true);
		stepSlider.setBorder(padding);
		stepSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel stepLabel = new JLabel(Strings.EDITOR_STEP);
		stepLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		stepLabel.setFont(font);
		add(stepLabel);
		add(stepSlider);
		
	}
	
	/**
	 * Initializes the stroke slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initStrokeSlider(DocumentManager documentManager, Border padding, Font font) {
		
		strokeSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_STROKE, MAX_STROKE, (MAX_STROKE - MIN_STROKE) / 2.0, PRECISION);
		strokeSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!strokeSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Tree2D tree = (Tree2D) document.getCurrentKeyFrame().getFractal();
					tree.setStroke(strokeSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		strokeSlider.setMinorTickSpacing(MAX_STROKE / 100);
		strokeSlider.setMajorTickSpacing(MAX_STROKE / 10);
		strokeSlider.setPaintTicks(true);
		strokeSlider.setLabelTable(strokeSlider.createStandardLabels(MAX_STROKE));
		strokeSlider.setPaintLabels(true);
		strokeSlider.setBorder(padding);
		strokeSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel strokeLabel = new JLabel(Strings.EDITOR_STROKE);
		strokeLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		strokeLabel.setFont(font);
		add(strokeLabel);
		add(strokeSlider);
		
	}
	
}