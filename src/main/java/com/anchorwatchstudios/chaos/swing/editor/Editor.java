package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JPanel;

import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.swing.Chaos;

/**
 * A base class for editors that go in the <code>Rollout</code>. <code>Editor</code>s
 * have <code>CardLayout</code>s that contain many panels.
 * 
 * @author Anthony Atella
 */
public abstract class Editor extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private JPanel cardPanel;
	protected static final String EMPTY_PANEL = "None";
	
	/**
	 * Constructs a new <code>Editor</code>.
	 * 
	 * @param context The program context
	 */
	public Editor(Chaos context) {
		
		setLayout(new BorderLayout());
		cardPanel = new JPanel(new CardLayout());
		cardPanel.add(EMPTY_PANEL, new JPanel());
		add(cardPanel, BorderLayout.CENTER);
		
	}
	
	/**
	 * Adds a component to this <code>Editor</code> with the given key.
	 * 
	 * @param panelKey The panel key
	 * @param component The component to be added
	 */
	protected void addPanel(String panelKey, Component component) {
	
		cardPanel.add(panelKey, component);
		
	}
	
	/**
	 * Shows the panel with the given key.
	 * 
	 * @param panelKey The panel key
	 */
	protected void showPanel(String panelKey) {
	
		((CardLayout) cardPanel.getLayout()).show(cardPanel, panelKey);
		
	}
	
}