package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbox;
import com.anchorwatchstudios.chaos.swing.DoubleSlider;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate a <code>Mandelbox</code> set.
 * 
 * @author Anthony Atella
 */
public class MandelboxControls extends Controls implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private static final int MIN_ITERATIONS = 0;
	private static final int MAX_ITERATIONS = 500;
	private static final double MIN_THRESHOLD = 0;
	private static final double MAX_THRESHOLD = 10.0;
	private static final double MAX_SCALE = 10.0;
	private static final double MIN_SCALE = -10.0;
	private static final int PRECISION = 1;
	private static final int SEPARATOR_SIZE = 32;
	private static final double MAX_FOLD_LIMIT = 10.0;
	private static final double MIN_FOLD_LIMIT = 0;
	private static final double MIN_RADIUS = 0;
	private static final double MAX_RADIUS = 10.0;
	private JSlider iterationsSlider;
	private DoubleSlider thresholdSlider;
	private DoubleSlider foldLimitSlider;
	private DoubleSlider minRadiusSlider;
	private DoubleSlider fixedRadiusSlider;
	private DoubleSlider scaleSlider;

	/**
	 * Constructs a new <code>Mandelbox</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public MandelboxControls(DocumentManager documentManager) {

		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		initHeader(padding);
		initIterationsSlider(documentManager, padding);
		initThresholdSlider(documentManager, padding);
		initFoldLimitSlider(documentManager, padding);
		initMinRadiusSlider(documentManager, padding);
		initFixedRadiusSlider(documentManager, padding);
		initScaleSlider(documentManager, padding);

	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {

			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Mandelbox) {
			
				Mandelbox mandelbox = (Mandelbox) fractal;
				iterationsSlider.setValue(mandelbox.getIterations());
				thresholdSlider.setDoubleValue(mandelbox.getThreshold());
				foldLimitSlider.setDoubleValue(mandelbox.getFoldLimit());
				minRadiusSlider.setDoubleValue(mandelbox.getMinRadius());
				fixedRadiusSlider.setDoubleValue(mandelbox.getFixedRadius());
				scaleSlider.setDoubleValue(mandelbox.getScale());
			
			}
			
		}
		
	}
	
	/**
	 * Initializes the header.
	 * 
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initHeader(Border padding) {

		JLabel title = new JLabel(Mandelbox.class.getSimpleName());
		Font font = new Font("Arial", Font.PLAIN, 24);
		title.setFont(font);
		title.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(title);
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, SEPARATOR_SIZE));
		add(separator);
		
	}
	
	/**
	 * Initializes the iterations slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initIterationsSlider(DocumentManager documentManager, Border padding) {
		
		iterationsSlider = new JSlider(JSlider.HORIZONTAL, MIN_ITERATIONS, MAX_ITERATIONS, (MAX_ITERATIONS - MIN_ITERATIONS) / 2);
		iterationsSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!iterationsSlider.getValueIsAdjusting()) {

					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbox mandelbox = (Mandelbox) document.getCurrentKeyFrame().getFractal();
					mandelbox.setIterations(iterationsSlider.getValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		iterationsSlider.setMinorTickSpacing((int) (MAX_ITERATIONS - MIN_ITERATIONS / 100.0));
		iterationsSlider.setMajorTickSpacing((int) (MAX_ITERATIONS - MIN_ITERATIONS / 10.0));
		iterationsSlider.setLabelTable(iterationsSlider.createStandardLabels((int) (MAX_ITERATIONS / 2)));
		iterationsSlider.setPaintLabels(true);
		iterationsSlider.setPaintTicks(true);
		iterationsSlider.setBorder(padding);
		iterationsSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel iterationsLabel = new JLabel(Strings.EDITOR_ITERATIONS);
		iterationsLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(iterationsLabel);
		add(iterationsSlider);
		
	}
	
	/**
	 * Initializes the threshold slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initThresholdSlider(DocumentManager documentManager, Border padding) {
		
		thresholdSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_THRESHOLD, MAX_THRESHOLD, (MAX_THRESHOLD - MIN_THRESHOLD) / 2.0, PRECISION);
		thresholdSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!thresholdSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbox mandelbox = (Mandelbox) document.getCurrentKeyFrame().getFractal();
					mandelbox.setThreshold(thresholdSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		thresholdSlider.setMinorTickSpacing(MAX_THRESHOLD / 100);
		thresholdSlider.setMajorTickSpacing(MAX_THRESHOLD / 10);
		thresholdSlider.setPaintTicks(true);
		thresholdSlider.setLabelTable(thresholdSlider.createStandardLabels(MAX_THRESHOLD));
		thresholdSlider.setPaintLabels(true);
		thresholdSlider.setBorder(padding);
		thresholdSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel thresholdLabel = new JLabel(Strings.EDITOR_THRESHOLD);
		thresholdLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(thresholdLabel);
		add(thresholdSlider);
		
	}
	
	/**
	 * Initializes the fold limit slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 */
	private void initFoldLimitSlider(DocumentManager documentManager, Border padding) {
		
		foldLimitSlider = initDoubleSlider(MIN_FOLD_LIMIT, MAX_FOLD_LIMIT, PRECISION, Strings.EDITOR_FOLD_LIMIT, padding);
		foldLimitSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!foldLimitSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbox mandelbox = (Mandelbox) document.getCurrentKeyFrame().getFractal();
					mandelbox.setFoldLimit(foldLimitSlider.getDoubleValue());
					document.setEdited(true);
										
				}
				
			}
			
		});
		add(foldLimitSlider);
		
	}
	
	/**
	 * Initializes the minimum radius slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 */
	private void initMinRadiusSlider(DocumentManager documentManager, Border padding) {
		
		minRadiusSlider = initDoubleSlider(MIN_RADIUS, MAX_RADIUS, PRECISION, Strings.EDITOR_MIN_RADIUS, padding);
		minRadiusSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!minRadiusSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbox mandelbox = (Mandelbox) document.getCurrentKeyFrame().getFractal();
					mandelbox.setMinRadius(minRadiusSlider.getDoubleValue());
					document.setEdited(true);
										
				}
				
			}
			
		});
		add(minRadiusSlider);
		
	}
	
	/**
	 * Initializes the fixed radius slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 */
	private void initFixedRadiusSlider(DocumentManager documentManager, Border padding) {
		
		fixedRadiusSlider = initDoubleSlider(MIN_RADIUS, MAX_RADIUS, PRECISION, Strings.EDITOR_FIXED_RADIUS, padding);
		fixedRadiusSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!fixedRadiusSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbox mandelbox = (Mandelbox) document.getCurrentKeyFrame().getFractal();
					mandelbox.setFixedRadius(fixedRadiusSlider.getDoubleValue());
					document.setEdited(true);
										
				}
				
			}
			
		});
		add(fixedRadiusSlider);
		
	}
	
	/**
	 * Initializes the scale slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 */
	private void initScaleSlider(DocumentManager documentManager, Border padding) {
		
		scaleSlider = initDoubleSlider(MIN_SCALE, MAX_SCALE, PRECISION, Strings.EDITOR_SCALE, padding);
		scaleSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!scaleSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Mandelbox mandelbox = (Mandelbox) document.getCurrentKeyFrame().getFractal();
					mandelbox.setScale(scaleSlider.getDoubleValue());
					document.setEdited(true);
										
				}
				
			}
			
		});
		add(scaleSlider);
		
	}

}