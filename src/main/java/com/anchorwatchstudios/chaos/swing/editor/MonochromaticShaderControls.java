package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.shader.MonochromaticShader;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.swing.AWTColorFactory;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate a <code>MonochromaticShader</code>.
 * 
 * @author Anthony Atella
 */
public class MonochromaticShaderControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private JColorChooser chooser;
	private JCheckBox invertCheckBox;
	private boolean lock;

	/**
	 * Constructs a new <code>MonochromaticShaderControls</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public MonochromaticShaderControls(DocumentManager documentManager) {

		lock = false;
		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		Font font = new Font("Arial", Font.PLAIN, 24);
		initChoosers(padding, documentManager, font);
		initInvertCheckbox(documentManager, padding, font);

	}

	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();

		if(document != null) {

			Shader shader = document.getCurrentKeyFrame().getShader();
			
			if(shader instanceof MonochromaticShader && lock == false) {
			
				lock = true;
				MonochromaticShader mShader = (MonochromaticShader) shader;
				chooser.setColor(AWTColorFactory.convert(mShader.getColor1()));
				invertCheckBox.setSelected(mShader.isInverted());
				lock = false;
			
			}

		}

	}

	/**
	 * Initializes the color choosers.
	 * 
	 * @param padding The default padding
	 * @param documentManager The <code>DocumentManager</code>
	 * @param font The default font
	 */
	private void initChoosers(Border padding, DocumentManager documentManager, Font font) {

		chooser = new JColorChooser();
		chooser.setPreviewPanel(new JPanel());
		chooser.getSelectionModel().addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {

				FRC2Document document = (FRC2Document) documentManager.getCurrent();

				if(document != null) {

					Shader shader = document.getCurrentKeyFrame().getShader();

					if(shader instanceof MonochromaticShader && lock == false) {

						lock = true;
						MonochromaticShader mono = (MonochromaticShader) shader;
						mono.setColor1(AWTColorFactory.convert(chooser.getColor()));
						document.setEdited(true);
						lock = false;

					}

				}

			}

		});

		chooser.setAlignmentX(JColorChooser.CENTER_ALIGNMENT);
		chooser.setBorder(padding);
		JLabel label0 = new JLabel(Strings.COLOR + " 1");
		label0.setFont(font);
		label0.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(label0, padding);
		add(chooser);

	}
	
	/**
	 * Initializes the invert checkbox.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initInvertCheckbox(DocumentManager documentManager, Border padding, Font font) {
		
		invertCheckBox = new JCheckBox();
		invertCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				FRC2Document document = (FRC2Document) documentManager.getCurrent();
				MonochromaticShader shader = (MonochromaticShader) document.getCurrentKeyFrame().getShader();
				shader.setInverted(!shader.isInverted());
				document.setEdited(true);
				
			}
			
		});
		invertCheckBox.setBorder(padding);
		invertCheckBox.setAlignmentX(JCheckBox.CENTER_ALIGNMENT);
		JLabel discreteLabel = new JLabel(Strings.EDITOR_INVERT);
		discreteLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		discreteLabel.setFont(font);
		add(discreteLabel);
		add(invertCheckBox);
		
	}

}