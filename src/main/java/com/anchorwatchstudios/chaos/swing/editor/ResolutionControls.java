package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.math.Vec2i;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate the <code>Document</code> resolution.
 * 
 * @author Anthony Atella
 */
public class ResolutionControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private static final int MIN_VALUE = 0;
	private static final int MAX_VALUE = 4000;
	private static final int SEPARATOR_SIZE = 32;
	private JSlider widthSlider;
	private JSlider heightSlider;

	/**
	 * Constructs a new <code>ResolutionControls</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public ResolutionControls(DocumentManager documentManager) {

		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		Font font = new Font("Arial", Font.PLAIN, 24);
		Font font2 = new Font("Arial", Font.PLAIN, 20);
		initHeader(padding, font);
		initWidthSlider(documentManager, padding, font2);
		initHeightSlider(documentManager, padding, font2);
		initFooter();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {

			Vec2i resolution = document.getSettings().getResolution();
			widthSlider.setValue((int) resolution.getX());
			heightSlider.setValue((int) resolution.getY());
			
		}
		
	}
	
	/**
	 * Initializes the header.
	 * 
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initHeader(Border padding, Font font) {

		JLabel title = new JLabel(Strings.EDITOR_RESOLUTION);
		title.setFont(font);
		title.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(title);
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, SEPARATOR_SIZE));
		add(separator);
		
	}
	
	/**
	 * Initializes the footer.
	 */
	private void initFooter() {
		
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, SEPARATOR_SIZE));
		add(separator);
		
	}
	
	/**
	 * Initializes the width slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initWidthSlider(DocumentManager documentManager, Border padding, Font font) {
		
		widthSlider = new JSlider(JSlider.HORIZONTAL, MIN_VALUE, MAX_VALUE, (MAX_VALUE - MIN_VALUE) / 2);
		widthSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!widthSlider.getValueIsAdjusting()) {

					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Vec2i r = document.getSettings().getResolution();
					int value = widthSlider.getValue();
					
					if(value %2 != 0) {
						
						value++;
						
					}
					
					r.setX(value);
					r.setY(r.getY());
					document.setEdited(true);
					
				}
				
			}
			
		});
		widthSlider.setMinorTickSpacing((int) (MAX_VALUE - MIN_VALUE / 100.0));
		widthSlider.setMajorTickSpacing((int) (MAX_VALUE - MIN_VALUE / 10.0));
		widthSlider.setLabelTable(widthSlider.createStandardLabels((int) (MAX_VALUE / 2)));
		widthSlider.setPaintLabels(true);
		widthSlider.setPaintTicks(true);
		widthSlider.setBorder(padding);
		widthSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel iterationsLabel = new JLabel(Strings.EDITOR_WIDTH);
		iterationsLabel.setFont(font);
		iterationsLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(iterationsLabel);
		add(widthSlider);
		
	}
	
	/**
	 * Initializes the height slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initHeightSlider(DocumentManager documentManager, Border padding, Font font) {

		heightSlider = new JSlider(JSlider.HORIZONTAL, MIN_VALUE, MAX_VALUE, (MAX_VALUE - MIN_VALUE) / 2);
		heightSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!heightSlider.getValueIsAdjusting()) {

					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Vec2i r = document.getSettings().getResolution();
					int value = heightSlider.getValue();
					
					if(value %2 != 0) {
						
						value++;
						
					}
					
					r.setX(r.getX());
					r.setY(value);
					document.setEdited(true);
					
				}
				
			}
			
		});
		heightSlider.setMinorTickSpacing((int) (MAX_VALUE - MIN_VALUE / 100.0));
		heightSlider.setMajorTickSpacing((int) (MAX_VALUE - MIN_VALUE / 10.0));
		heightSlider.setLabelTable(heightSlider.createStandardLabels((int) (MAX_VALUE / 2)));
		heightSlider.setPaintLabels(true);
		heightSlider.setPaintTicks(true);
		heightSlider.setBorder(padding);
		heightSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel label = new JLabel(Strings.EDITOR_HEIGHT);
		label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		label.setFont(font);
		add(label);
		add(heightSlider);
		
	}
		
}