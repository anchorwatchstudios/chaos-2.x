package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.shader.DichromaticShader;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.swing.AWTColorFactory;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate a <code>DichromaticShader</code>.
 * 
 * @author Anthony Atella
 */
public class DichromaticShaderControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private JColorChooser[] chooser;
	private JCheckBox invertCheckBox;
	private boolean choosersActive;

	/**
	 * Constructs a new <code>DichromaticShaderControls</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public DichromaticShaderControls(DocumentManager documentManager) {

		choosersActive = true;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		Font font = new Font("Arial", Font.PLAIN, 24);
		initChoosers(padding, documentManager, font);
		initInvertCheckbox(documentManager, padding, font);

	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Shader shader = document.getCurrentKeyFrame().getShader();
			
			if(shader instanceof DichromaticShader) {
				
				DichromaticShader dShader = (DichromaticShader) shader;
				choosersActive = false;
				chooser[0].setColor(AWTColorFactory.convert(dShader.getColor1()));
				chooser[1].setColor(AWTColorFactory.convert(dShader.getColor2()));
				invertCheckBox.setSelected(dShader.isInverted());
				choosersActive = true;
			
			}
			
		}
		
	}
	
	/**
	 * Initializes the color choosers.
	 * 
	 * @param padding The default padding
	 * @param documentManager The <code>DocumentManager</code>
	 * @param font The default font
	 */
	private void initChoosers(Border padding, DocumentManager documentManager, Font font) {
		
		chooser = new JColorChooser[] { new JColorChooser(), new JColorChooser() };
		
		for(int i = 0;i < chooser.length; i++) {
			
			chooser[i].setPreviewPanel(new JPanel());
			chooser[i].getSelectionModel().addChangeListener(new ChangeListener() {

				@Override
				public void stateChanged(ChangeEvent e) {

					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					
					if(document != null) {
					
						Shader shader = document.getCurrentKeyFrame().getShader();
						
						if(shader instanceof DichromaticShader && choosersActive) {
							
							DichromaticShader dichromatic = (DichromaticShader) shader;
							dichromatic.setColor1(AWTColorFactory.convert(chooser[0].getColor()));
							dichromatic.setColor2(AWTColorFactory.convert(chooser[1].getColor()));
							document.setEdited(true);
							
						}
						
					}
					
				}
				
			});
			
		}
		
		chooser[0].setAlignmentX(JColorChooser.CENTER_ALIGNMENT);
		chooser[0].setBorder(padding);
		chooser[1].setAlignmentX(JColorChooser.CENTER_ALIGNMENT);
		chooser[1].setBorder(padding);
		JLabel label0 = new JLabel(Strings.COLOR + " 1");
		label0.setFont(font);
		label0.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(label0, padding);
		add(chooser[0]);
		JLabel label1 = new JLabel(Strings.COLOR + " 2");
		label1.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		label1.setFont(font);
		add(label1, padding);
		add(chooser[1]);
		
	}
	
	/**
	 * Initializes the invert checkbox.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initInvertCheckbox(DocumentManager documentManager, Border padding, Font font) {
		
		invertCheckBox = new JCheckBox();
		invertCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				FRC2Document document = (FRC2Document) documentManager.getCurrent();
				DichromaticShader shader = (DichromaticShader) document.getCurrentKeyFrame().getShader();
				shader.setInverted(!shader.isInverted());
				document.setEdited(true);
				
			}
			
		});
		invertCheckBox.setBorder(padding);
		invertCheckBox.setAlignmentX(JCheckBox.CENTER_ALIGNMENT);
		JLabel discreteLabel = new JLabel(Strings.EDITOR_INVERT);
		discreteLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		discreteLabel.setFont(font);
		add(discreteLabel);
		add(invertCheckBox);
		
	}
	
}