package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Cantor;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.swing.DoubleSlider;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate a <code>Cantor</code> set.
 * 
 * @author Anthony Atella
 */
public class CantorControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private static final double MIN_THRESHOLD = 0.0;
	private static final double MAX_THRESHOLD = 10.0;
	private static final double MIN_SPACE = 0.0;
	private static final double MAX_SPACE = 100.0;
	private static final int PRECISION = 1;
	private static final int SEPARATOR_SIZE = 32;
	private DoubleSlider spaceSlider;
	private DoubleSlider thresholdSlider;
	
	/**
	 * Constructs a new <code>CantorControls</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public CantorControls(DocumentManager documentManager) {

		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		Font font = new Font("Arial", Font.PLAIN, 24);
		Font font2 = new Font("Arial", Font.PLAIN, 20);
		initHeader(padding, font);
		initSpaceSlider(documentManager, padding, font2);
		initThresholdSlider(documentManager, padding, font2);

	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
		
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Cantor) {

				Cantor cantor = (Cantor) fractal;
				thresholdSlider.setDoubleValue(cantor.getThreshold());
				spaceSlider.setDoubleValue(cantor.getSpace());
				
			}
			
		}

	}
	
	/**
	 * Initializes the header.
	 * 
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initHeader(Border padding, Font font) {

		JLabel title = new JLabel(Cantor.class.getSimpleName());
		title.setFont(font);
		title.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(title);
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, SEPARATOR_SIZE));
		add(separator);
		
	}
	
	/**
	 * Initializes the space slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initSpaceSlider(DocumentManager documentManager, Border padding, Font font) {
		
		spaceSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_SPACE, MAX_SPACE, (MAX_SPACE - MIN_SPACE) / 2.0, PRECISION);
		spaceSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!spaceSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Cantor cantor = (Cantor) document.getCurrentKeyFrame().getFractal();
					cantor.setSpace(spaceSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		spaceSlider.setMinorTickSpacing(MAX_SPACE / 100);
		spaceSlider.setMajorTickSpacing(MAX_SPACE / 10);
		spaceSlider.setPaintTicks(true);
		spaceSlider.setLabelTable(spaceSlider.createStandardLabels(MAX_SPACE));
		spaceSlider.setPaintLabels(true);
		spaceSlider.setBorder(padding);
		spaceSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel spaceLabel = new JLabel(Strings.EDITOR_SPACE);
		spaceLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		spaceLabel.setFont(font);
		add(spaceLabel);
		add(spaceSlider);
		
	}
	
	/**
	 * Initializes the threshold slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initThresholdSlider(DocumentManager documentManager, Border padding, Font font) {
		
		thresholdSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_THRESHOLD, MAX_THRESHOLD, (MAX_THRESHOLD - MIN_THRESHOLD) / 2.0, PRECISION);
		thresholdSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!thresholdSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Cantor cantor = (Cantor) document.getCurrentKeyFrame().getFractal();
					cantor.setThreshold(thresholdSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		thresholdSlider.setMinorTickSpacing(MAX_THRESHOLD / 100);
		thresholdSlider.setMajorTickSpacing(MAX_THRESHOLD / 10);
		thresholdSlider.setPaintTicks(true);
		thresholdSlider.setLabelTable(thresholdSlider.createStandardLabels(MAX_THRESHOLD));
		thresholdSlider.setPaintLabels(true);
		thresholdSlider.setBorder(padding);
		thresholdSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel thresholdLabel = new JLabel(Strings.EDITOR_THRESHOLD);
		thresholdLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		thresholdLabel.setFont(font);
		add(thresholdLabel);
		add(thresholdSlider);
		
	}
	
}