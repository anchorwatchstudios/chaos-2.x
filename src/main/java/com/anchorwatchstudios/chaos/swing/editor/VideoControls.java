package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.Settings;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate the <code>Document</code> video settings.
 * 
 * @author Anthony Atella
 */
public class VideoControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private static final int MIN_VALUE = 0;
	private static final int MAX_VALUE = 120;
	private static final int SEPARATOR_SIZE = 32;
	private JSlider fpsSlider;
	private JSlider fpkSlider;

	/**
	 * Constructs a new <code>VideoControls</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public VideoControls(DocumentManager documentManager) {

		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		Font font = new Font("Arial", Font.PLAIN, 24);
		Font font2 = new Font("Arial", Font.PLAIN, 20);
		initHeader(padding, font);
		initFPSSlider(documentManager, padding, font2);
		initFPKSlider(documentManager, padding, font2);
		initFooter();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {

			Settings settings = document.getSettings();
			fpsSlider.setValue(settings.getFPS());
			fpkSlider.setValue(settings.getFPK());
			
		}
		
	}
	
	/**
	 * Initializes the header.
	 * 
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initHeader(Border padding, Font font) {

		JLabel title = new JLabel(Strings.EDITOR_VIDEO);
		title.setFont(font);
		title.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(title);
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, SEPARATOR_SIZE));
		add(separator);
		
	}
	
	/**
	 * Initializes the footer.
	 */
	private void initFooter() {
		
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, SEPARATOR_SIZE));
		add(separator);
		
	}
	
	/**
	 * Initializes the FPS slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initFPSSlider(DocumentManager documentManager, Border padding, Font font) {
		
		fpsSlider = new JSlider(JSlider.HORIZONTAL, MIN_VALUE, MAX_VALUE, (MAX_VALUE - MIN_VALUE) / 2);
		fpsSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!fpsSlider.getValueIsAdjusting()) {

					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					document.getSettings().setFPS(fpsSlider.getValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		fpsSlider.setMinorTickSpacing((int) (MAX_VALUE - MIN_VALUE / 100.0));
		fpsSlider.setMajorTickSpacing((int) (MAX_VALUE - MIN_VALUE / 10.0));
		fpsSlider.setLabelTable(fpsSlider.createStandardLabels((int) (MAX_VALUE / 4)));
		fpsSlider.setPaintLabels(true);
		fpsSlider.setPaintTicks(true);
		fpsSlider.setBorder(padding);
		fpsSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel iterationsLabel = new JLabel(Strings.EDITOR_FPS);
		iterationsLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		iterationsLabel.setFont(font);
		add(iterationsLabel);
		add(fpsSlider);
		
	}
	
	/**
	 * Initializes the key frames per frame slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initFPKSlider(DocumentManager documentManager, Border padding, Font font) {

		fpkSlider = new JSlider(JSlider.HORIZONTAL, MIN_VALUE, MAX_VALUE, (MAX_VALUE - MIN_VALUE) / 2);
		fpkSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!fpkSlider.getValueIsAdjusting()) {

					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					document.getSettings().setFPK(fpkSlider.getValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		fpkSlider.setMinorTickSpacing(1);
		fpkSlider.setMajorTickSpacing(10);
		fpkSlider.setLabelTable(fpkSlider.createStandardLabels((int) (MAX_VALUE / 4)));
		fpkSlider.setPaintLabels(true);
		fpkSlider.setPaintTicks(true);
		fpkSlider.setBorder(padding);
		fpkSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel label = new JLabel(Strings.EDITOR_FPK);
		label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		label.setFont(font);
		add(label);
		add(fpkSlider);
		
	}
		
}