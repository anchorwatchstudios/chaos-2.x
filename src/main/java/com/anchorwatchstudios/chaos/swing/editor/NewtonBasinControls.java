package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Dimension;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
//import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.NewtonBasin;
import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.swing.DoubleSlider;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate a <code>NewtonBasin</code>.
 * 
 * @author Anthony Atella
 */
public class NewtonBasinControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private static final int MIN_ITERATIONS = 0;
	private static final int MAX_ITERATIONS = 500;
	private static final double MIN_THRESHOLD = 0;
	private static final double MAX_THRESHOLD = 10.0;
	private static final double MIN_ALPHA = -10.0;
	private static final double MAX_ALPHA = 10.0;
	private static final double MIN_N = 0;
	private static final double MAX_N = 16;
	private static final int PRECISION = 1;
	private static final int SEPARATOR_SIZE = 32;
	private JSlider iterationsSlider;
	private DoubleSlider thresholdSlider;
	private DoubleSlider nASlider;
	private DoubleSlider nBSlider;
	private DoubleSlider alphaASlider;
	private DoubleSlider alphaBSlider;

	/**
	 * Constructs a new <code>NewtonBasin</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public NewtonBasinControls(DocumentManager documentManager) {

		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		initHeader(padding);
		initIterationsSlider(documentManager, padding);
		initThresholdSlider(documentManager, padding);
		initNASlider(documentManager, padding);
		initNBSlider(documentManager, padding);
		initAlphaASlider(documentManager, padding);
		initAlphaBSlider(documentManager, padding);

	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {

			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof NewtonBasin) {
				
				NewtonBasin nBasin = (NewtonBasin) fractal;
				iterationsSlider.setValue(nBasin.getIterations());
				thresholdSlider.setDoubleValue(nBasin.getThreshold());
				nASlider.setDoubleValue(nBasin.getN().getA());
				nBSlider.setDoubleValue(nBasin.getN().getB());
				alphaASlider.setDoubleValue(nBasin.getAlpha().getA());
				alphaBSlider.setDoubleValue(nBasin.getAlpha().getB());
			
			}
			
		}
		
	}
	
	/**
	 * Initializes the header.
	 * 
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initHeader(Border padding) {

		JLabel title = new JLabel(NewtonBasin.class.getSimpleName());
		Font font = new Font("Arial", Font.PLAIN, 24);
		title.setFont(font);
		title.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(title);
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, SEPARATOR_SIZE));
		add(separator);
		
	}
	
	/**
	 * Initializes the iterations slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initIterationsSlider(DocumentManager documentManager, Border padding) {
		
		iterationsSlider = new JSlider(JSlider.HORIZONTAL, MIN_ITERATIONS, MAX_ITERATIONS, (MAX_ITERATIONS - MIN_ITERATIONS) / 2);
		iterationsSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!iterationsSlider.getValueIsAdjusting()) {

					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					NewtonBasin newton = (NewtonBasin) document.getCurrentKeyFrame().getFractal();
					newton.setIterations(iterationsSlider.getValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		iterationsSlider.setMinorTickSpacing((int) (MAX_ITERATIONS - MIN_ITERATIONS / 100.0));
		iterationsSlider.setMajorTickSpacing((int) (MAX_ITERATIONS - MIN_ITERATIONS / 10.0));
		iterationsSlider.setLabelTable(iterationsSlider.createStandardLabels((int) (MAX_ITERATIONS / 2)));
		iterationsSlider.setPaintLabels(true);
		iterationsSlider.setPaintTicks(true);
		iterationsSlider.setBorder(padding);
		iterationsSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel iterationsLabel = new JLabel(Strings.EDITOR_ITERATIONS);
		iterationsLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(iterationsLabel);
		add(iterationsSlider);
		
	}
	
	/**
	 * Initializes the threshold slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initThresholdSlider(DocumentManager documentManager, Border padding) {
		
		thresholdSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_THRESHOLD, MAX_THRESHOLD, (MAX_THRESHOLD - MIN_THRESHOLD) / 2.0, PRECISION);
		thresholdSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!thresholdSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					NewtonBasin newtonBasin = (NewtonBasin) document.getCurrentKeyFrame().getFractal();
					newtonBasin.setThreshold(thresholdSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		thresholdSlider.setMinorTickSpacing(MAX_THRESHOLD / 100);
		thresholdSlider.setMajorTickSpacing(MAX_THRESHOLD / 10);
		thresholdSlider.setPaintTicks(true);
		thresholdSlider.setLabelTable(thresholdSlider.createStandardLabels(MAX_THRESHOLD));
		thresholdSlider.setPaintLabels(true);
		thresholdSlider.setBorder(padding);
		thresholdSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel thresholdLabel = new JLabel(Strings.EDITOR_THRESHOLD);
		thresholdLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(thresholdLabel);
		add(thresholdSlider);
		
	}
	
	/**
	 * Initializes the Na slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initNASlider(DocumentManager documentManager, Border padding) {
		
		nASlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_N, MAX_N, (MAX_N - MIN_N) / 2.0, PRECISION);
		nASlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!nASlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					NewtonBasin newtonBasin = (NewtonBasin) document.getCurrentKeyFrame().getFractal();
					Complex n = newtonBasin.getN();
					n.setA(nASlider.getDoubleValue());
					newtonBasin.setN(n);
					document.setEdited(true);
					
				}
				
			}
			
		});
		nASlider.setMinorTickSpacing(MAX_N / 100);
		nASlider.setMajorTickSpacing(MAX_N / 10);
		nASlider.setPaintTicks(true);
		nASlider.setLabelTable(nASlider.createStandardLabels(MAX_N));
		nASlider.setPaintLabels(true);
		nASlider.setBorder(padding);
		nASlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel caLabel = new JLabel(Strings.EDITOR_NA);
		caLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(caLabel);
		add(nASlider);
		
	}
	
	/**
	 * Initializes the Nb slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initNBSlider(DocumentManager documentManager, Border padding) {
		
		nBSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_N, MAX_N, (MAX_N - MIN_N) / 2.0, PRECISION);
		nBSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!nBSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					NewtonBasin newtonBasin = (NewtonBasin) document.getCurrentKeyFrame().getFractal();
					Complex n = newtonBasin.getN();
					n.setB(nBSlider.getDoubleValue());
					newtonBasin.setN(n);
					document.setEdited(true);
					
				}
				
			}
			
		});
		nBSlider.setMinorTickSpacing(MAX_N / 100);
		nBSlider.setMajorTickSpacing(MAX_N / 10);
		nBSlider.setPaintTicks(true);
		nBSlider.setLabelTable(nBSlider.createStandardLabels(MAX_N));
		nBSlider.setPaintLabels(true);
		nBSlider.setBorder(padding);
		nBSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel cbLabel = new JLabel(Strings.EDITOR_NB);
		cbLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(cbLabel);
		add(nBSlider);
		
	}
	
	/**
	 * Initializes the Aa slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initAlphaASlider(DocumentManager documentManager, Border padding) {
		
		alphaASlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_ALPHA, MAX_ALPHA, (MAX_ALPHA - MIN_ALPHA) / 2.0, PRECISION);
		alphaASlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!alphaASlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					NewtonBasin newtonBasin = (NewtonBasin) document.getCurrentKeyFrame().getFractal();
					Complex alpha = newtonBasin.getAlpha();
					alpha.setA(alphaASlider.getDoubleValue());
					newtonBasin.setAlpha(alpha);
					document.setEdited(true);
					
				}
				
			}
			
		});
		alphaASlider.setMinorTickSpacing(MAX_ALPHA / 100);
		alphaASlider.setMajorTickSpacing(MAX_ALPHA / 10);
		alphaASlider.setPaintTicks(true);
		alphaASlider.setLabelTable(nASlider.createStandardLabels(MAX_ALPHA));
		alphaASlider.setPaintLabels(true);
		alphaASlider.setBorder(padding);
		alphaASlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel alphaALabel = new JLabel(Strings.EDITOR_ALPHAA);
		alphaALabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(alphaALabel);
		add(alphaASlider);
		
	}
	
	/**
	 * Initializes the Ab slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initAlphaBSlider(DocumentManager documentManager, Border padding) {
		
		alphaBSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_ALPHA, MAX_ALPHA, (MAX_ALPHA - MIN_ALPHA) / 2.0, PRECISION);
		alphaBSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!alphaBSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					NewtonBasin newtonBasin = (NewtonBasin) document.getCurrentKeyFrame().getFractal();
					Complex alpha = newtonBasin.getAlpha();
					alpha.setB(alphaBSlider.getDoubleValue());
					newtonBasin.setAlpha(alpha);
					document.setEdited(true);
					
				}
				
			}
			
		});
		alphaBSlider.setMinorTickSpacing(MAX_ALPHA / 100);
		alphaBSlider.setMajorTickSpacing(MAX_ALPHA / 10);
		alphaBSlider.setPaintTicks(true);
		alphaBSlider.setLabelTable(nASlider.createStandardLabels(MAX_ALPHA));
		alphaBSlider.setPaintLabels(true);
		alphaBSlider.setBorder(padding);
		alphaBSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel alphaBLabel = new JLabel(Strings.EDITOR_ALPHAB);
		alphaBLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(alphaBLabel);
		add(alphaBSlider);
		
	}

}