package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Julia;
import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.swing.DoubleSlider;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate a <code>Julia</code> set.
 * 
 * @author Anthony Atella
 */
public class JuliaControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private static final int MIN_ITERATIONS = 0;
	private static final int MAX_ITERATIONS = 500;
	private static final double MIN_THRESHOLD = 0;
	private static final double MAX_THRESHOLD = 10.0;
	private static final double MIN_C = -10.0;
	private static final double MAX_C = 10.0;
	private static final int PRECISION = 1;
	private static final int SEPARATOR_SIZE = 32;
	private JSlider iterationsSlider;
	private DoubleSlider thresholdSlider;
	private DoubleSlider cASlider;
	private DoubleSlider cBSlider;
	private JCheckBox discreteCheckBox;

	/**
	 * Constructs a new <code>JuliaControls</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public JuliaControls(DocumentManager documentManager) {

		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		Font font = new Font("Arial", Font.PLAIN, 24);
		Font font2 = new Font("Arial", Font.PLAIN, 20);
		initHeader(padding, font);
		initIterationsSlider(documentManager, padding, font2);
		initThresholdSlider(documentManager, padding, font2);
		initCASlider(documentManager, padding, font2);
		initCBSlider(documentManager, padding, font2);
		initDiscreteCheckBox(documentManager, padding, font2);

	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {

			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Julia) {
			
				Julia julia = (Julia) document.getCurrentKeyFrame().getFractal();
				iterationsSlider.setValue(julia.getIterations());
				thresholdSlider.setDoubleValue(julia.getThreshold());
				cASlider.setDoubleValue(julia.getC().getA());
				cBSlider.setDoubleValue(julia.getC().getB());
				discreteCheckBox.setSelected(julia.getDiscrete());
				
			}
			
		}
		
	}
	
	/**
	 * Initializes the header.
	 * 
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initHeader(Border padding, Font font) {

		JLabel title = new JLabel(Julia.class.getSimpleName());
		title.setFont(font);
		title.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(title);
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, SEPARATOR_SIZE));
		add(separator);
		
	}
	
	/**
	 * Initializes the iterations slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initIterationsSlider(DocumentManager documentManager, Border padding, Font font) {
		
		iterationsSlider = new JSlider(JSlider.HORIZONTAL, MIN_ITERATIONS, MAX_ITERATIONS, (MAX_ITERATIONS - MIN_ITERATIONS) / 2);
		iterationsSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!iterationsSlider.getValueIsAdjusting()) {

					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Julia julia = (Julia) document.getCurrentKeyFrame().getFractal();
					julia.setIterations(iterationsSlider.getValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		iterationsSlider.setMinorTickSpacing((int) (MAX_ITERATIONS - MIN_ITERATIONS / 100.0));
		iterationsSlider.setMajorTickSpacing((int) (MAX_ITERATIONS - MIN_ITERATIONS / 10.0));
		iterationsSlider.setLabelTable(iterationsSlider.createStandardLabels((int) (MAX_ITERATIONS / 2)));
		iterationsSlider.setPaintLabels(true);
		iterationsSlider.setPaintTicks(true);
		iterationsSlider.setBorder(padding);
		iterationsSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel iterationsLabel = new JLabel(Strings.EDITOR_ITERATIONS);
		iterationsLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		iterationsLabel.setFont(font);
		add(iterationsLabel);
		add(iterationsSlider);
		
	}
	
	/**
	 * Initializes the threshold slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initThresholdSlider(DocumentManager documentManager, Border padding, Font font) {
		
		thresholdSlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_THRESHOLD, MAX_THRESHOLD, (MAX_THRESHOLD - MIN_THRESHOLD) / 2.0, PRECISION);
		thresholdSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!thresholdSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Julia julia = (Julia) document.getCurrentKeyFrame().getFractal();
					julia.setThreshold(thresholdSlider.getDoubleValue());
					document.setEdited(true);
					
				}
				
			}
			
		});
		thresholdSlider.setMinorTickSpacing(MAX_THRESHOLD / 100);
		thresholdSlider.setMajorTickSpacing(MAX_THRESHOLD / 10);
		thresholdSlider.setPaintTicks(true);
		thresholdSlider.setLabelTable(thresholdSlider.createStandardLabels(MAX_THRESHOLD));
		thresholdSlider.setPaintLabels(true);
		thresholdSlider.setBorder(padding);
		thresholdSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel thresholdLabel = new JLabel(Strings.EDITOR_THRESHOLD);
		thresholdLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		thresholdLabel.setFont(font);
		add(thresholdLabel);
		add(thresholdSlider);
		
	}
	
	/**
	 * Initializes the Ca slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initCASlider(DocumentManager documentManager, Border padding, Font font) {
		
		cASlider = new DoubleSlider(JSlider.HORIZONTAL, MIN_C, MAX_C, (MAX_C - MIN_C) / 2.0, PRECISION);
		cASlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!cASlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Julia julia = (Julia) document.getCurrentKeyFrame().getFractal();
					Complex c = julia.getC();
					c.setA(cASlider.getDoubleValue());
					julia.setC(c);
					document.setEdited(true);
					
				}
				
			}
			
		});
		cASlider.setMinorTickSpacing(MAX_C / 100);
		cASlider.setMajorTickSpacing(MAX_C / 10);
		cASlider.setPaintTicks(true);
		cASlider.setLabelTable(cASlider.createStandardLabels(MAX_C));
		cASlider.setPaintLabels(true);
		cASlider.setBorder(padding);
		cASlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel caLabel = new JLabel(Strings.EDITOR_CA);
		caLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		caLabel.setFont(font);
		add(caLabel);
		add(cASlider);
		
	}
	
	/**
	 * Initializes the Cb slider.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initCBSlider(DocumentManager documentManager, Border padding, Font font) {
		
		cBSlider = new DoubleSlider(JSlider.HORIZONTAL, (int) MIN_C, MAX_C, (MAX_C - MIN_C) / 2.0, PRECISION);
		cBSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {

				if(!cBSlider.getValueIsAdjusting()) {
				
					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					Julia julia = (Julia) document.getCurrentKeyFrame().getFractal();
					Complex c = julia.getC();
					c.setB(cBSlider.getDoubleValue());
					julia.setC(c);
					document.setEdited(true);
					
				}
				
			}
			
		});
		cBSlider.setMinorTickSpacing(MAX_C / 100);
		cBSlider.setMajorTickSpacing(MAX_C / 10);
		cBSlider.setPaintTicks(true);
		cBSlider.setLabelTable(cBSlider.createStandardLabels(MAX_C));
		cBSlider.setPaintLabels(true);
		cBSlider.setBorder(padding);
		cBSlider.setAlignmentX(JSlider.CENTER_ALIGNMENT);
		JLabel cbLabel = new JLabel(Strings.EDITOR_CB);
		cbLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		cbLabel.setFont(font);
		add(cbLabel);
		add(cBSlider);
		
	}
	
	/**
	 * Initializes the discrete checkbox.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initDiscreteCheckBox(DocumentManager documentManager, Border padding, Font font) {
		
		discreteCheckBox = new JCheckBox();
		discreteCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				FRC2Document document = (FRC2Document) documentManager.getCurrent();
				Julia julia = (Julia) document.getCurrentKeyFrame().getFractal();
				julia.setDiscrete(!julia.getDiscrete());
				document.setEdited(true);
				
			}
			
		});
		discreteCheckBox.setBorder(padding);
		discreteCheckBox.setAlignmentX(JCheckBox.CENTER_ALIGNMENT);
		JLabel discreteLabel = new JLabel(Strings.EDITOR_DISCRETE);
		discreteLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		discreteLabel.setFont(font);
		add(discreteLabel);
		add(discreteCheckBox);
		
	}
	
}