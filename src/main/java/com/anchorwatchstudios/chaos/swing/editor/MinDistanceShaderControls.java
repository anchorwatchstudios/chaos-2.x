package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.shader.MinDistanceShader;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.swing.AWTColorFactory;

/**
 * A <code>JPanel</code> containing all the controls necessary to
 * manipulate a <code>MinDistanceShader</code>.
 * 
 * @author Anthony Atella
 */
public class MinDistanceShaderControls extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private JColorChooser[] chooser;
	private JCheckBox invertCheckBox;
	private boolean lock;

	/**
	 * Constructs a new <code>MagnitudeShaderControls</code>.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 */
	public MinDistanceShaderControls(DocumentManager documentManager) {

		lock = false;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		Border padding = BorderFactory.createEmptyBorder(5, 10, 5, 10);
		Font font = new Font("Arial", Font.PLAIN, 24);
		initChoosers(padding, documentManager, font);
		initInvertCheckbox(documentManager, padding, font);

	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Shader shader = document.getCurrentKeyFrame().getShader();
			
			if(shader instanceof MinDistanceShader && lock == false) {
				
				lock = true;
				MinDistanceShader artistic = (MinDistanceShader) shader;
				chooser[0].setColor(AWTColorFactory.convert(artistic.getColor1()));
				chooser[1].setColor(AWTColorFactory.convert(artistic.getColor2()));
				chooser[2].setColor(AWTColorFactory.convert(artistic.getColor3()));
				invertCheckBox.setSelected(artistic.isInverted());
				lock = false;
			
			}
			
		}
		
	}
	
	/**
	 * Initializes the color choosers.
	 * 
	 * @param padding The default padding
	 * @param documentManager The <code>DocumentManager</code>
	 * @param font The default font
	 */
	private void initChoosers(Border padding, DocumentManager documentManager, Font font) {
		
		chooser = new JColorChooser[] { new JColorChooser(), new JColorChooser(), new JColorChooser() };
		
		for(int i = 0;i < chooser.length; i++) {
			
			chooser[i].setPreviewPanel(new JPanel());
			chooser[i].getSelectionModel().addChangeListener(new ChangeListener() {

				@Override
				public void stateChanged(ChangeEvent e) {

					FRC2Document document = (FRC2Document) documentManager.getCurrent();
					
					if(document != null) {
					
						Shader shader = document.getCurrentKeyFrame().getShader();
						
						if(shader instanceof MinDistanceShader && lock == false) {
							
							lock = true;
							MinDistanceShader artistic = (MinDistanceShader) shader;
							artistic.setColor1(AWTColorFactory.convert(chooser[0].getColor()));
							artistic.setColor2(AWTColorFactory.convert(chooser[1].getColor()));
							artistic.setColor3(AWTColorFactory.convert(chooser[2].getColor()));
							document.setEdited(true);
							lock = false;
							
						}
						
					}
					
				}
				
			});
			chooser[i].setAlignmentX(JColorChooser.CENTER_ALIGNMENT);
			chooser[i].setBorder(padding);
			JLabel label = new JLabel(Strings.COLOR + " " + (i + 1));
			label.setFont(font);
			label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
			add(label, padding);
			add(chooser[i]);
			
		}
		
	}
	
	/**
	 * Initializes the invert checkbox.
	 * 
	 * @param documentManager The <code>DocumentManager</code>
	 * @param padding The default padding
	 * @param font The default font
	 */
	private void initInvertCheckbox(DocumentManager documentManager, Border padding, Font font) {
		
		invertCheckBox = new JCheckBox();
		invertCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				FRC2Document document = (FRC2Document) documentManager.getCurrent();
				MinDistanceShader shader = (MinDistanceShader) document.getCurrentKeyFrame().getShader();
				shader.setInverted(!shader.isInverted());
				document.setEdited(true);
				
			}
			
		});
		invertCheckBox.setBorder(padding);
		invertCheckBox.setAlignmentX(JCheckBox.CENTER_ALIGNMENT);
		JLabel discreteLabel = new JLabel(Strings.EDITOR_INVERT);
		discreteLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		discreteLabel.setFont(font);
		add(discreteLabel);
		add(invertCheckBox);
		
	}
	
}