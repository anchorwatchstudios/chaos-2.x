package com.anchorwatchstudios.chaos.swing.editor;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.document.render.RayMarch;
import com.anchorwatchstudios.chaos.swing.Chaos;

/**
 * An <code>Editor</code> that contains all render
 * editor controls.
 * 
 * @author Anthony Atella
 */
public class RenderEditor extends Editor {

	private ResolutionControls resControls;
	private VideoControls videoControls;
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructs a new <code>RenderEditor</code>.
	 * 
	 * @param context The program context
	 */
	public RenderEditor(Chaos context) {
		
		super(context);
		resControls = new ResolutionControls(context.getDocumentManager());
		context.getDocumentManager().registerObserver(resControls);
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(resControls, BorderLayout.NORTH);
		videoControls = new VideoControls(context.getDocumentManager());
		context.getDocumentManager().registerObserver(videoControls);
		panel.add(videoControls, BorderLayout.SOUTH);
		add(panel, BorderLayout.NORTH);
		JPanel complexPlotPanel = new JPanel();
		addPanel(ComplexPlot.class.toString(), complexPlotPanel);
		RayMarchControls rmse = new RayMarchControls(context.getDocumentManager());
		context.getDocumentManager().registerObserver(rmse);
		addPanel(RayMarch.class.toString(), rmse);
		videoControls.setVisible(false);
		resControls.setVisible(false);

	}

	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();

		if(document == null) {
			
			showPanel(Editor.EMPTY_PANEL);
			resControls.setVisible(false);
			videoControls.setVisible(false);

		}
		else {
			
			resControls.setVisible(true);
			videoControls.setVisible(true);
			showPanel(document.getCurrentKeyFrame().getRenderMethod().getClass().toString());
			
		}
		
	}
	
}