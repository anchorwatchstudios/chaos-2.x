package com.anchorwatchstudios.chaos.swing.editor;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Cantor;
import com.anchorwatchstudios.chaos.document.fractal.Julia;
import com.anchorwatchstudios.chaos.document.fractal.Juliabulb;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbox;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbrot;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbulb;
import com.anchorwatchstudios.chaos.document.fractal.NewtonBasin;
import com.anchorwatchstudios.chaos.document.fractal.NewtonKnot;
import com.anchorwatchstudios.chaos.document.fractal.Tree2D;
import com.anchorwatchstudios.chaos.swing.Chaos;

/**
 * An <code>Editor</code> that contains all fractal
 * editor controls.
 * 
 * @author Anthony Atella
 */
public class FractalEditor extends Editor {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructs a new <code>FractalEditor</code>.
	 * 
	 * @param context The program context
	 */
	public FractalEditor(Chaos context) {
		
		super(context);
		DocumentManager documentManager = context.getDocumentManager();
		JuliaControls juliaEditor = new JuliaControls(documentManager);
		MandelbrotControls mandelbrotEditor = new MandelbrotControls(documentManager);
		NewtonBasinControls newtonBasinEditor = new NewtonBasinControls(documentManager);
		Tree2DControls tree2DEditor = new Tree2DControls(documentManager);
		CantorControls cantorEditor = new CantorControls(documentManager);
		JuliabulbControls juliabulbEditor = new JuliabulbControls(documentManager);
		MandelbulbControls mandelbulbEditor = new MandelbulbControls(documentManager);
		MandelboxControls mandelboxEditor = new MandelboxControls(documentManager);
		NewtonKnotControls newtonKnotEditor = new NewtonKnotControls(documentManager);
		documentManager.registerObserver(juliaEditor);
		documentManager.registerObserver(mandelbrotEditor);
		documentManager.registerObserver(newtonBasinEditor);
		documentManager.registerObserver(tree2DEditor);
		documentManager.registerObserver(cantorEditor);
		documentManager.registerObserver(juliabulbEditor);
		documentManager.registerObserver(mandelbulbEditor);
		documentManager.registerObserver(mandelboxEditor);
		documentManager.registerObserver(newtonKnotEditor);
		addPanel(Julia.class.toString(), juliaEditor);
		addPanel(Mandelbrot.class.toString(), mandelbrotEditor);
		addPanel(NewtonBasin.class.toString(), newtonBasinEditor);
		addPanel(Tree2D.class.toString(), tree2DEditor);
		addPanel(Cantor.class.toString(), cantorEditor);
		addPanel(Juliabulb.class.toString(), juliabulbEditor);
		addPanel(Mandelbulb.class.toString(), mandelbulbEditor);
		addPanel(Mandelbox.class.toString(), mandelboxEditor);
		addPanel(NewtonKnot.class.toString(), newtonKnotEditor);
		
	}

	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {

			showPanel(document.getCurrentKeyFrame().getFractal().getClass().toString());
			
		}
		else {
			
			showPanel("None");
			
		}
		
	}

}