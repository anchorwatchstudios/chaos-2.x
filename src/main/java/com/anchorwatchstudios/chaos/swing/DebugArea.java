package com.anchorwatchstudios.chaos.swing;

import java.awt.Font;

import javax.swing.JTextArea;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;

/**
 * Displays debug information about the current <code>Document</code>.
 * 
 * @author Anthony Atella
 */
public class DebugArea extends JTextArea implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new <code>DebugArea</code>.
	 * 
	 * @param context The program context
	 */
	public DebugArea(Chaos context) {

		context.getDocumentManager().registerObserver(this);
		Font font = new Font("Arial", Font.PLAIN, 24);
		setFont(font);
		setVisible(false);

	}

	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();

		if(document != null) {

			setText(document.getDescription());

		}

	}

}