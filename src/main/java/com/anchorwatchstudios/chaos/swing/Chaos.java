package com.anchorwatchstudios.chaos.swing;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.ArrayListDocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.fractal.Cantor;
import com.anchorwatchstudios.chaos.document.fractal.Julia;
import com.anchorwatchstudios.chaos.document.fractal.Juliabulb;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbox;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbrot;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbulb;
import com.anchorwatchstudios.chaos.document.fractal.NewtonBasin;
import com.anchorwatchstudios.chaos.document.fractal.NewtonKnot;
import com.anchorwatchstudios.chaos.document.fractal.Tree2D;
import com.anchorwatchstudios.chaos.document.render.GL4RIM;
import com.anchorwatchstudios.chaos.document.render.J2DRIM;
import com.anchorwatchstudios.chaos.jogl.gl4.program.JuliaComplexPlotGL4Program;
import com.anchorwatchstudios.chaos.jogl.gl4.program.JuliabulbRayMarchGL4Program;
import com.anchorwatchstudios.chaos.jogl.gl4.program.MandelboxRayMarchGL4Program;
import com.anchorwatchstudios.chaos.jogl.gl4.program.MandelbrotComplexPlotGL4Program;
import com.anchorwatchstudios.chaos.jogl.gl4.program.MandelbulbRayMarchGL4Program;
import com.anchorwatchstudios.chaos.jogl.gl4.program.NewtonBasinComplexPlotGL4Program;
import com.anchorwatchstudios.chaos.jogl.gl4.program.NewtonKnotRayMarchGL4Program;
import com.anchorwatchstudios.chaos.swing.task.CantorJ2DTask;
import com.anchorwatchstudios.chaos.swing.task.ComplexPlotJ2DTask;
import com.anchorwatchstudios.chaos.swing.task.TreeJ2DTask;
import com.anchorwatchstudios.chaos.task.MultiTaskManager;
import com.jogamp.opengl.GLProfile;

/**
 * A fractal geometry generator built with Java Swing and OpenGL.
 * Renders geometry on the complex plane and in hypercomplex space,
 * as well as one-off fractals like the Cantor set and trees. Capable
 * of exporting images and video.
 * 
 * @author Anthony Atella
 */
public class Chaos {
	
	public static final int VERSION_MAJOR = 2;
	public static final int VERSION_MINOR = 4;
	public static final int VERSION_PATCH = 0;
	public static final String URL = "https://anchorwatchstudios.com/chaos";
	private static Window window;
	private DocumentManager documentManager;
	private MultiTaskManager taskManager;
	private JFileChooser jFileChooser;
	private J2DRIM j2dRIM;
	private GL4RIM gl4RIM;

	/**
	 * Creates a new <code>Chaos</code>.
	 */
	public Chaos() {
		
		if(GLProfile.isAvailable(GLProfile.GL4bc)) {
		
			documentManager = new ArrayListDocumentManager();
			taskManager = new MultiTaskManager();
			jFileChooser = new JFileChooser();
			j2dRIM = new J2DRIM(this);
			j2dRIM.registerImpl(Julia.class, ComplexPlotJ2DTask.class);
			j2dRIM.registerImpl(Mandelbrot.class, ComplexPlotJ2DTask.class);
			j2dRIM.registerImpl(NewtonBasin.class, ComplexPlotJ2DTask.class);
			j2dRIM.registerImpl(Cantor.class, CantorJ2DTask.class);
			j2dRIM.registerImpl(Tree2D.class, TreeJ2DTask.class);
			gl4RIM = new GL4RIM();
			gl4RIM.registerImpl(Julia.class, JuliaComplexPlotGL4Program.class);
			gl4RIM.registerImpl(Mandelbrot.class, MandelbrotComplexPlotGL4Program.class);
			gl4RIM.registerImpl(NewtonBasin.class, NewtonBasinComplexPlotGL4Program.class);
			gl4RIM.registerImpl(Juliabulb.class, JuliabulbRayMarchGL4Program.class);
			gl4RIM.registerImpl(Mandelbulb.class, MandelbulbRayMarchGL4Program.class);
			gl4RIM.registerImpl(Mandelbox.class, MandelboxRayMarchGL4Program.class);
			gl4RIM.registerImpl(NewtonKnot.class, NewtonKnotRayMarchGL4Program.class);
			window = new Window(this, Strings.WINDOW_TITLE);
			
		}
		else {
			
			JOptionPane.showMessageDialog(null, Strings.GL_ERROR_VERSION, "No OpenGL4 Profile Found", JOptionPane.ERROR_MESSAGE);
			
		}

	}
	
	/**
	 * Returns the <code>JFrame</code>.
	 * 
	 * @return The <code>JFrame</code>
	 */
	public Window getWindow() {
		
		return window;
		
	}
	
	/**
	 * Returns the <code>DocumentManager</code>.
	 * 
	 * @return The <code>DocumentManager</code>
	 */
	public DocumentManager getDocumentManager() {

		return documentManager;

	}
	
	/**
	 * Returns the <code>MultiTaskManager</code>.
	 * 
	 * @return The <code>MultiTaskManager</code>
	 */
	public MultiTaskManager getMultiTaskManager() {

		return taskManager;

	}
	
	/**
	 * Returns the <code>JFileChooser</code>.
	 * 
	 * @return The <code>JFileChooser</code>
	 */
	public JFileChooser getFileChooser() {
		
		return jFileChooser;
		
	}
	
	/**
	 * Returns the <code>J2DRIM</code>.
	 * 
	 * @return The <code>J2DRIM</code>
	 */
	public J2DRIM getJ2DRIM() {
		
		return j2dRIM;
		
	}

	/**
	 * Returns the <code>GL4RIM</code>.
	 * 
	 * @return The <code>GL4RIM</code>
	 */
	public GL4RIM getGL4RIM() {

		return gl4RIM;
		
	}
	
	/**
	 * The main entry point for the program.
	 * No arguments are used.
	 * 
	 * @param args The program arguments
	 */
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				new Chaos();

			}

		});

	}

}