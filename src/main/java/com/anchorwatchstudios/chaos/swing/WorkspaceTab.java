package com.anchorwatchstudios.chaos.swing;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.kordamp.ikonli.fontawesome.FontAwesome;
import org.kordamp.ikonli.swing.FontIcon;

import com.anchorwatchstudios.chaos.document.Document;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.swing.event.Action;
import com.anchorwatchstudios.chaos.swing.event.Action.Close;

/**
 * A tab with a close button for a <code>JTabbedPane</code>
 * 
 * @author Anthony Atella
 */
public class WorkspaceTab extends JPanel implements DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private JLabel title;
	private Component parent;
	private Workspace workspace;

	/**
	 * Constructs a new <code>WorkspaceTab</code> with the given
	 * name.
	 * 
	 * @param context The program context
	 * @param parent The panel associated with this tab
	 * @param title The title of this tab
	 */
	public WorkspaceTab(Chaos context, Component parent, String title) {
		
		this.parent = parent;
		this.workspace = context.getWindow().getWorkspace();
		context.getDocumentManager().registerObserver(this);
		int size = 24;
		Font font = new Font("Arial", Font.PLAIN, size);
		setLayout(new GridBagLayout());
		setOpaque(false);
		this.title = new JLabel(title + " ");
		this.title.setFont(font);
		FontIcon fontIcon = new FontIcon();
		fontIcon.setIkon(FontAwesome.CLOSE);
		fontIcon.setIconSize(size);
		JButton button = new JButton(fontIcon);
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Workspace workspace = context.getWindow().getWorkspace();
				workspace.setSelectedComponent(parent);
				Close close = new Action.Close(context);
				close.actionPerformed(e);
				
				if(!close.isCancelled()) {
					
					context.getDocumentManager().unregisterObserver(WorkspaceTab.this);
					
				}
				
				if(workspace.getSelectedIndex() == -1 && workspace.getTabCount() > 0) {
					
					workspace.setSelectedIndex(0);
					
				}
				
			}
			
		});
		add(this.title);
		add(button);
		
	}
	
	/**
	 * Sets the title of this tab.
	 * 
	 * @param title The title
	 */
	public void setTitle(String title) {
		
		this.title.setText(title);
		
	}

	@Override
	public void notify(DocumentManager documentManager) {

		Document document = documentManager.getCurrent();
		Component selectedComponent = workspace.getSelectedComponent();

		if(document != null && selectedComponent != null && selectedComponent.equals(parent)) {
			
			String current = title.getText();
			
			if(document.isEdited()) {
			
				if(!current.endsWith("* ")) {
					
					title.setText(current + "* ");
					
				}
				
			}
			else {
				
				if(current.endsWith("* ")) {
					
					title.setText(current.substring(0, current.length() - 2));
					
				}
				
			}
			
		}
		
	}
	
}
