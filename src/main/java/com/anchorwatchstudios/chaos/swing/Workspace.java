package com.anchorwatchstudios.chaos.swing;

import java.awt.Component;
import java.awt.image.BufferedImage;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.swing.canvas.Canvas;
import com.anchorwatchstudios.chaos.swing.canvas.MultiCanvas;

/**
 * A <code>JTabbedPane</code> that holds visual representations of each open <code>Document</code>.
 * 
 * @author Anthony Atella
 */
public class Workspace extends JTabbedPane implements ChangeListener, DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private Chaos context;
	
	/**
	 * Constructs a new <code>Workspace</code>.
	 * 
	 * @param context The program context
	 */
	public Workspace(Chaos context) {

		DocumentManager documentManager = context.getDocumentManager();
		this.context = context;
		addChangeListener(this);
		documentManager.registerObserver(this);
		
	}

	/**
	 * Returns an image of the currently displayed <code>Canvas</code>.
	 * 
	 * @return An image of the currently displayed <code>Canvas</code>
	 */
	public BufferedImage getImage() {

		return ((Canvas) getComponentAt(context.getDocumentManager().getIndex())).getImage();
		
	}
	
	/**
	 * Returns the current canvas shown.
	 * 
	 * @return The current canvas shown
	 */
	public MultiCanvas getCurrentCanvas() {
	
		return (MultiCanvas) getComponentAt(getSelectedIndex());
		
	}
	
	@Override
	public Component add(String name, Component c) {

		super.add(name, c);
		setTabComponentAt(indexOfComponent(c), new WorkspaceTab(context, c, name));
		return c;
		
	}

	@Override
	public void stateChanged(ChangeEvent e) {

		context.getDocumentManager().setIndex(getSelectedIndex());
		context.getWindow().getTimeline().notify(context.getDocumentManager());

	}

	@Override
	public void notify(DocumentManager documentManager) {

		if(documentManager.getIndex() < getTabCount()) {
		
			int index = documentManager.getIndex();
			setSelectedIndex(index);
			
			if(index > -1) {
			
				DocumentManagerObserver panel = ((DocumentManagerObserver) getSelectedComponent());
				DocumentManagerObserver tab = ((DocumentManagerObserver) getTabComponentAt(index));
				
				if(panel != null) {
					
					panel.notify(documentManager);
				
				}
				
				if(tab != null) {	

					tab.notify(documentManager);
					
				}					
				
			}
			
		}
			
	}
	
}
