package com.anchorwatchstudios.chaos.swing;

import java.text.NumberFormat;
import java.util.Dictionary;
import java.util.Hashtable;

import javax.swing.JLabel;
import javax.swing.JSlider;

/**
 * Displays and stores doubles in a <code>JSlider</code>.
 * 
 * @author Anthony Atella
 */
public class DoubleSlider extends JSlider {

	private static final long serialVersionUID = 1L;
	private int precision;
	
	/**
	 * Constructs a new <code>DoubleSlider</code> with a precision of 1.
	 */
	public DoubleSlider() {

		super();
		precision = 1;
		
	}
	
	/**
	 * Constructs a new <code>DoubleSlider</code> with the given precision.
	 * 
	 * @param precision The number of decimals to be precise to
	 */
	public DoubleSlider(int precision) {
		
		super();
		this.precision = precision;
		
	}
	
	/**
	 * Constructs a new <code>DoubleSlider</code> with the given values.
	 * 
	 * @param orientation Vertical or horizontal
	 * @param precision The number of decimals to be precise to
	 * @param min The minimum of this slider
	 * @param max The maximum of this slider
	 * @param value The initial value
	 * @param precision The number of decimals to be precise to
	 */
	public DoubleSlider(int orientation, double min, double max, double value, int precision) {

		super(orientation, (int) (min * Math.pow(10, precision)), (int) (max * Math.pow(10, precision)), (int) (value * Math.pow(10, precision)));
		this.precision = precision;

	}

	/**
	 * Sets this sliders precision in decimal places.
	 * 
	 * @param precision The number of decimal places to be precise to
	 */
	public void setPrecision(int precision) {
		
		this.precision = precision;
		
	}
	
	/**
	 * Returns this sliders precision.
	 * 
	 * @return The number of decimal places to be precise to
	 */
	public int getPrecision() {
		
		return precision;
		
	}
	
	/**
	 * Sets the major tick spacing in double precision.
	 * 
	 * @param n The major tick spacing
	 */
	public void setMajorTickSpacing(double n) {
		
		super.setMajorTickSpacing((int) (n * Math.pow(10, precision)));
		
	}
	
	/**
	 * Sets the minor tick spacing in double precision.
	 * 
	 * @param n The minor tick spacing
	 */
	public void setMinorTickSpacing(double n) {
		
		super.setMinorTickSpacing((int) (n * Math.pow(10, precision)));
		
	}
	
	/**
	 * Creates standard labels from a double value.
	 * 
	 * @param increment The label increment
	 * @return A new <code>Dictionary</code> of labels
	 */
	public Dictionary<Integer, JLabel> createStandardLabels(double increment) {
        
	      Dictionary<Integer, JLabel> labels = new Hashtable<>();
	      
	      for(int i = getMinimum();i <= getMaximum();i += increment * Math.pow(10, precision)) {
	    	  
	         String text = String.format("%4.3f", i / (float) Math.pow(10.0, precision));
	         NumberFormat nf = NumberFormat.getInstance();
	         nf.setMaximumFractionDigits(precision);
	         nf.setMinimumFractionDigits(precision);
	         text = nf.format(i / (float) Math.pow(10.0, precision));
	         labels.put(i, new JLabel(text));
	         
	      }

	      return labels;
		
	}
	
	/**
	 * Returns the value in double precision.
	 * 
	 * @return The value in double precision
	 */
	public double getDoubleValue() {
		
		return super.getValue() / Math.pow(10, precision);
		
	}
	
	/**
	 * Sets the value in double precision.
	 * 
	 * @param value The value in double precision
	 */
	public void setDoubleValue(double value) {

		String text = Double.toString(Math.abs(value));
		int integerPlaces = text.indexOf('.');
		int decimalPlaces = text.length() - integerPlaces - 1;
		
		if(decimalPlaces > precision) {
			
			text = text.substring(0, integerPlaces) + "." + text.substring(integerPlaces + 1, integerPlaces + 1 + precision);
			
			if(value > 0) {
				
				value = Double.parseDouble(text);
			
			}
			else {
				
				value = -Double.parseDouble(text);
				
			}
			
		}

		super.setValue((int) (value * Math.pow(10, precision)));
					
	}

}
