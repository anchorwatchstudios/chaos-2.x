package com.anchorwatchstudios.chaos.swing;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.swing.editor.FractalEditor;
import com.anchorwatchstudios.chaos.swing.editor.RenderEditor;
import com.anchorwatchstudios.chaos.swing.editor.ShaderEditor;

/**
 * An area where all settings are displayed for the
 * current <code>Document</code> and <code>KeyFrame</code>.
 * 
 * @author Anthony Atella
 */
public class Rollout extends JPanel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructs a new <code>Rollout</code>.
	 * 
	 * @param context The program context
	 */
	public Rollout(Chaos context) {

		DocumentManager documentManager = context.getDocumentManager();
		setLayout(new BorderLayout());
		JTabbedPane tabs = new JTabbedPane();
		FractalEditor fractalEditor = new FractalEditor(context);
		documentManager.registerObserver(fractalEditor);
		RenderEditor settingsEditor = new RenderEditor(context);
		documentManager.registerObserver(settingsEditor);
		ShaderEditor shaderEditor = new ShaderEditor(context);
		documentManager.registerObserver(shaderEditor);
		tabs.add(fractalEditor, "Fractal");
		tabs.add(shaderEditor, "Shader");
		tabs.add(settingsEditor, "Render");
		tabs.setFont(new Font("Arial", Font.PLAIN, 24));
		add(tabs, BorderLayout.CENTER);
		
	}
	
}
