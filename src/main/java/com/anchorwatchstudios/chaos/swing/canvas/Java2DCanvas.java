package com.anchorwatchstudios.chaos.swing.canvas;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.Settings;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.math.Vec2i;
import com.anchorwatchstudios.chaos.swing.Chaos;
import com.anchorwatchstudios.chaos.swing.event.Action;
import com.anchorwatchstudios.chaos.task.MultiTaskManager;
import com.anchorwatchstudios.chaos.task.Task;

/**
 * A canvas that displays a <code>BufferedImage</code>.
 * 
 * @author Anthony Atella
 */
public class Java2DCanvas extends JPanel implements Canvas, DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private BufferedImage image;
	private FRC2Document document;
	private Chaos context;
	private int viewportX, viewportY, viewportW, viewportH;
	private volatile boolean rendering;

	/**
	 * Constructs a new <code>Java2DCanvas</code>.
	 * 
	 * @param context The program context
	 */
	public Java2DCanvas(Chaos context) {

		super();
		this.context = context;
		DocumentManager documentManager = context.getDocumentManager();
		Vec2i resolution = ((FRC2Document) documentManager.getCurrent()).getSettings().getResolution();
		image = new BufferedImage((int) resolution.getX(), (int) resolution.getY(), BufferedImage.TYPE_INT_ARGB);
		document = (FRC2Document) documentManager.getCurrent();

	}

	/**
	 * Resizes the canvas. Equivalent to the CSS 'background-size: contain;'.
	 */
	public void resize() {

		double imageAspectRatio = (double) image.getHeight() / (double) image.getWidth();
		double panelAspectRatio = (double) getHeight() / (double) getWidth();
		double comparison = Double.compare(panelAspectRatio, imageAspectRatio);

		if(comparison == 0) {

			// Equal
			viewportX = 0;
			viewportW = getWidth();
			viewportY = 0;
			viewportH = getHeight();

		}
		else if(comparison < 1) {

			// Panel is wider
			double scaledWidth = ((double) getHeight() / (double) image.getHeight()) * (double) image.getWidth();
			int offset = (int) (((double) getWidth() - (double) scaledWidth) / (double) 2);
			viewportX = offset;
			viewportW = (int) scaledWidth;
			viewportY = 0;
			viewportH = getHeight();

		}
		else {

			// Image is wider
			double scaledHeight = ((double) getWidth() / (double) image.getWidth()) * (double) image.getHeight();
			int offset = (int) (((double) getHeight() - (double) scaledHeight) / (double) 2);
			viewportX = 0;
			viewportW = getWidth();
			viewportY = offset;
			viewportH = (int) scaledHeight;

		}
		
	}

	@Override
	public boolean isRendering() {

		return rendering;
		
	}
	
	@Override
	public BufferedImage getImage() {

		return image;

	}

	@Override
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
		resize();
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setBackground(Color.GRAY);
		g2d.clearRect(0, 0, getWidth(), getHeight());
		g2d.drawImage(image, viewportX, viewportY, viewportW, viewportH, null);
		rendering = false;
		
	}

	@Override
	public void notify(DocumentManager documentManager) {

		document = (FRC2Document) documentManager.getCurrent();

		if(document != null) {

			rendering = true;
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			Settings settings = document.getSettings();
			Vec2i resolution = settings.getResolution();
			updateImage(resolution);
			Task task = context.getJ2DRIM().getImpl(fractal.getClass());
			
			if(task != null) {
				
				new Action.AddTask(context, task, MultiTaskManager.SINGLE).actionPerformed(null);
				
			}

		}

	}

	@Override
	public String toString() {
		
		return "Java2DCanvas";
		
	}
	
	/**
	 * Resizes the image if the given resolution differs from its
	 * own.
	 * 
	 * @param resolution The new resolution
	 */
	private void updateImage(Vec2i resolution) {

		if(image.getWidth() != resolution.getX() || image.getHeight() != resolution.getY()) {

			image = new BufferedImage((int) resolution.getX(), (int) resolution.getY(), BufferedImage.TYPE_INT_ARGB);

		}

	}

}