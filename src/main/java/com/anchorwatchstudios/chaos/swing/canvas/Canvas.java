package com.anchorwatchstudios.chaos.swing.canvas;

import java.awt.image.BufferedImage;

/**
 * A common interface for drawing canvases.
 * 
 * @author Anthony Atella
 */
public interface Canvas {
	
	/**
	 * Returns true if the <code>Canvas</code> is
	 * rendering.
	 * 
	 * @return true if the <code>Canvas</code> is rendering.
	 */
	boolean isRendering();
	
	/**
	 * Returns an image of this <code>Canvas</code>.
	 * 
	 * @return An image of this <code>Canvas</code>
	 */
	BufferedImage getImage();
	
}