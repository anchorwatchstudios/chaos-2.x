package com.anchorwatchstudios.chaos.swing.canvas;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.jogl.gl4.program.GL4Program;
import com.anchorwatchstudios.chaos.swing.Chaos;
import com.anchorwatchstudios.chaos.swing.StatusBar;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLJPanel;

/**
 * Takes an array of programs and initializes, displays, reshapes, 
 * and disposes of them in the order given. Notifies the programs
 * of document changes.
 * 
 * @author Anthony Atella
 */
public abstract class OpenGL4Canvas extends JPanel implements Canvas, DocumentManagerObserver, GLEventListener {

	private static final long serialVersionUID = 1L;
	protected GLJPanel canvas;
	protected GL4Program[] programs;
	private StatusBar statusBar;
	private volatile boolean rendering;

	/**
	 * Constructs a new <code>OpenGL4Canvas</code> with the given programs.
	 * 
	 * @param context The program context
	 * @param programs The GL shader programs
	 */
	public OpenGL4Canvas(Chaos context, GL4Program[] programs) {

		super();
		statusBar = context.getWindow().getStatusBar();
		this.programs = programs;
		GLProfile glProfile = GLProfile.get(GLProfile.GL4bc);
		GLCapabilities glCapabilities = new GLCapabilities(glProfile);
		canvas = new GLJPanel(glCapabilities);
		canvas.addGLEventListener(this);
		setLayout(new BorderLayout());
		add(canvas, BorderLayout.CENTER);

	}

	@Override
	public void notify(DocumentManager documentManager) {

		for(int i = 0;i < programs.length;i++) {
			
			programs[i].notify(documentManager);
			
		}
		
		canvas.display();

	}

	@Override
	public void init(GLAutoDrawable drawable) {

		for(int i = 0;i < programs.length;i++) {
			
			programs[i].init(drawable);
			
		}
		
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {

		for(int i = 0;i < programs.length;i++) {
			
			programs[i].dispose(drawable);
			
		}
		
	}

	@Override
	public void display(GLAutoDrawable drawable) {

		rendering = true;
		GL4 gl = drawable.getGL().getGL4();
		
		for(int i = 0;i < programs.length;i++) {
			
			programs[i].display(drawable);
			
		}
		
		checkError(gl);
		rendering = false;
		
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

		for(int i = 0;i < programs.length;i++) {
			
			programs[i].reshape(drawable, x, y, width, height);
			
		}

	}
	
	@Override
	public String toString() {
		
		return "OpenGL4Canvas";
		
	}
	
	@Override
	public boolean isRendering() {
		
		return rendering;
		
	}

	/**
	 * Checks for a GL error and outputs a message
	 * if an error was found.
	 * 
	 * @param gl The GL context
	 */
	private void checkError(GL gl) {
		
		int error = gl.glGetError();
		
		if(error != GL4.GL_NO_ERROR) {
			
			statusBar.setStatus(Strings.GL_ERROR + error);
			System.out.println(Strings.GL_ERROR + error);
			
		}
		
	}
	
}
