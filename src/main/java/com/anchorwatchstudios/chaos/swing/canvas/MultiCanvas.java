package com.anchorwatchstudios.chaos.swing.canvas;

import java.awt.CardLayout;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.Settings;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.document.render.RenderImplementation;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.swing.Chaos;
import com.anchorwatchstudios.chaos.swing.event.ComplexPlotKeyListener;
import com.anchorwatchstudios.chaos.swing.event.ComplexPlotMouseListener;
import com.anchorwatchstudios.chaos.swing.event.RayMarchKeyListener;
import com.anchorwatchstudios.chaos.swing.event.RayMarchMouseListener;

/**
 * A canvas capable of all implementations. Can switch between
 * various rendering implementations of <code>Canvas</code> on
 * the fly.
 * 
 * @author Anthony Atella
 */
public class MultiCanvas extends JPanel implements Canvas, DocumentManagerObserver {

	private static final long serialVersionUID = 1L;
	private Java2DCanvas j2DCanvas;
	private OpenGL4Canvas glCanvas;
	private Chaos context;
	
	/**
	 * Constructs a new <code>MultiCanvas</code>.
	 * 
	 * @param context The program context
	 */
	public MultiCanvas(Chaos context) {
		
		this.context = context;
		setLayout(new CardLayout());
		j2DCanvas = new Java2DCanvas(context);
		add(j2DCanvas.toString(), j2DCanvas);
		FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();
		Fractal fractal = document.getCurrentKeyFrame().getFractal();
		RenderMethod renderMethod = document.getCurrentKeyFrame().getRenderMethod();
		glCanvas = new ImageOpenGL4Canvas(context, context.getGL4RIM().getImpl(fractal.getClass()));
		
		if(renderMethod instanceof ComplexPlot) {
			
			addKeyListener(new ComplexPlotKeyListener(context));
			ComplexPlotMouseListener mouseListener = new ComplexPlotMouseListener(context);
			addMouseListener(mouseListener);
			addMouseMotionListener(mouseListener);
			addMouseWheelListener(mouseListener);
			
		}
		else {
			
			addKeyListener(new RayMarchKeyListener(context));
			RayMarchMouseListener mouseListener = new RayMarchMouseListener(context);
			addMouseListener(mouseListener);
			addMouseMotionListener(mouseListener);
			addMouseWheelListener(mouseListener);
			
		}
		
		add(glCanvas.toString(), glCanvas);
		
	}

	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
		
			Settings settings = document.getSettings();
			RenderImplementation imp = settings.getRenderImplementation();

			switch(imp) {
				
			case JAVA2D:

				((CardLayout) getLayout()).show(this, j2DCanvas.toString());
				j2DCanvas.notify(documentManager);
				break;
				
			case OPENGL4:

				((CardLayout) getLayout()).show(this, glCanvas.toString());
				glCanvas.notify(documentManager);
				break;
				
			default:

				break;
				
			}
					
		}
	
	}
	
	@Override
	public BufferedImage getImage() {
		
		Settings settings = ((FRC2Document) context.getDocumentManager().getCurrent()).getSettings();
		RenderImplementation imp = settings.getRenderImplementation();
		BufferedImage result = null;
		
		switch(imp) {
			
		case JAVA2D:

			((CardLayout) getLayout()).show(this, j2DCanvas.toString());
			result = j2DCanvas.getImage();
			break;
			
		case OPENGL4:

			((CardLayout) getLayout()).show(this, glCanvas.toString());
			result = glCanvas.getImage();
			break;
			
		default:

			break;
			
		}
		
		return result;
		
	}

	@Override
	public boolean isRendering() {

		Settings settings = ((FRC2Document) context.getDocumentManager().getCurrent()).getSettings();
		RenderImplementation imp = settings.getRenderImplementation();
		boolean result = false;
		
		switch(imp) {
			
		case JAVA2D:

			result = j2DCanvas.isRendering();
			break;
			
		case OPENGL4:

			result = glCanvas.isRendering();
			break;
			
		default:

			break;
			
		}

		return result;
		
	}

	/**
	 * Returns the Java2D canvas.
	 * 
	 * @return The Java2D canvas
	 */
	public Java2DCanvas getJ2DCanvas() {

		return j2DCanvas;

	}
	
	/**
	 * Returns the OpenGL4 canvas.
	 * 
	 * @return The OpenGL4 canvas
	 */
	public OpenGL4Canvas getGL4Canvas() {
		
		return glCanvas;
		
	}

}
