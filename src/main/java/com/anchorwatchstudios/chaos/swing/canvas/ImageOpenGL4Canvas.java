package com.anchorwatchstudios.chaos.swing.canvas;

import java.awt.image.BufferedImage;

import com.anchorwatchstudios.chaos.jogl.gl4.program.GL4Program;
import com.anchorwatchstudios.chaos.jogl.gl4.program.ImageGL4Program;
import com.anchorwatchstudios.chaos.swing.Chaos;

/**
 * A canvas that uses OpenGL4 to draw an image with a given
 * OpenGL4 shader program. First draws to a frame buffer object
 * with the given program and then draws to the main buffer. This
 * is done so that the image copied is the correct resolution.
 * 
 * @author Anthony Atella
 */
public class ImageOpenGL4Canvas extends OpenGL4Canvas {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new <code>ImageOpenGL4Canvas</code> with the given shader program.
	 * @param context The program context
	 * @param program The GL4 program
	 */
	public ImageOpenGL4Canvas(Chaos context, GL4Program program) {

		super(context, new GL4Program[] { new ImageGL4Program(program) });
		
	}
	
	@Override
	public BufferedImage getImage() {

		((ImageGL4Program) programs[0]).setExport(true);
		canvas.display();
		return ((ImageGL4Program) programs[0]).getImage();
		
	}
	
}
