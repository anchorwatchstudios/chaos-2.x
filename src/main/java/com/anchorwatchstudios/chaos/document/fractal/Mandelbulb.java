package com.anchorwatchstudios.chaos.document.fractal;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Discrete;
import com.anchorwatchstudios.chaos.math.Interpolation;
import com.anchorwatchstudios.chaos.math.Triplex;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * Represents the Mandelbrot set in triplex space. Contains 
 * all the information used to calculate it.
 * 
 * @author Anthony Atella
 */
public class Mandelbulb implements EscapeTimeFractal<Triplex>, Discrete {

	private static final Triplex DEFAULT_Z = new Triplex(0, 0, 0.1);
	private static final double DEFAULT_N = 8.0;
	private Triplex z;
	private double n;
	private int iterations;
	private double threshold;
	private boolean discrete;
	
	/**
	 * Constructs a new <code>Mandelbulb</code> with default values.
	 */
	public Mandelbulb() {
		
		z = DEFAULT_Z;
		n = DEFAULT_N;
		iterations = DEFAULT_ITERATIONS;
		threshold = DEFAULT_THRESHOLD;
		discrete = Discrete.DEFAULT;
		
	}
	
	/**
	 * Constructs a new <code>Mandelbulb</code> with the given values.
	 * 
	 * @param z The triplex number 'z0', from zn = zn-1 + C
	 * @param n The power to raise this <code>Mandelbulb</code> to
	 * @param iterations The number of iterations
	 * @param threshold The threshold
	 * @param discrete Whether or not this fractal is calculated discretely
	 */
	public Mandelbulb(Triplex z, double n, int iterations, double threshold, boolean discrete) {
		
		this.z = z;
		this.n = n;
		this.iterations = iterations;
		this.threshold = threshold;
		this.discrete = discrete;
		
	}
	
	/**
	 * Returns the triplex number 'z0', from zn = zn-1 + C.
	 * 
	 * @return The triplex number 'z0', from zn = zn-1 + C
	 */
	public Triplex getZ() {
		
		return z;
	
	}

	/**
	 * Sets the triplex number 'z0', from zn = zn-1 + C.
	 * 
	 * @param z The triplex number 'z0', from zn = zn-1 + C
	 */
	public void setZ(Triplex z) {
	
		this.z = z;
	
	}
	
	
	/**
	 * Returns the power this <code>Mandelbulb</code> is raised
	 * to.
	 * 
	 * @return The power this <code>Mandelbulb</code> is raised to
	 */
	public double getN() {
		
		return n;
		
	}

	/**
	 * Sets the power this <code>Mandelbulb</code> is raised
	 * to.
	 * 
	 * @param n The power this <code>Mandelbulb</code> is raised to
	 */
	public void setN(double n) {
		
		this.n = n;
		
	}

	@Override
	public Vec4d isInSet(Triplex point) {
		
		// TODO: Unimplemented
		return new Vec4d();
		
	}

	@Override
	public int getIterations() {

		return iterations;

	}

	@Override
	public void setIterations(int iterations) {

		this.iterations = iterations;
		
	}

	@Override
	public double getThreshold() {

		return threshold;

	}

	@Override
	public void setThreshold(double threshold) {

		this.threshold = threshold;
		
	}

	@Override
	public String getDescription() {

		return "Mandelbulb: \n" +
				"  z: " + z.toString() + "\n" +
				"  n: " + n + "\n" +
				"  Iterations: " + iterations + "\n" +
				"  Threshold: " + threshold + "\n" +
				"  Discrete: " + discrete + "\n\n";

	}
	
	@Override
	public Mandelbulb clone() {
		
		return new Mandelbulb(new Triplex(z.getA(), z.getB(), z.getC()), n, iterations, threshold, discrete);
		
	}

	@Override
	public Class<Triplex> getType() {

		return Triplex.class;

	}

	@Override
	public boolean getDiscrete() {

		return discrete;

	}

	@Override
	public void setDiscrete(boolean discrete) {

		this.discrete = discrete;
		
	}
	
	@Override
	public String toString() {
		
		return Strings.FRACTAL_MANDELBULB;
		
	}

	@Override
	public Fractal interpolate(Fractal fractal, double percent) {

		if(fractal instanceof Mandelbulb) {
			
			Mandelbulb mandelbulb = (Mandelbulb) fractal;
			Mandelbulb result = new Mandelbulb();
			result.setZ(z.interpolate(mandelbulb.getZ(), percent));
			result.setN(Interpolation.interpolate(n, mandelbulb.getN(), percent));
			result.setIterations(Interpolation.interpolate(iterations, mandelbulb.getIterations(), percent));
			result.setThreshold(Interpolation.interpolate(threshold, mandelbulb.getThreshold(), percent));
			
			if(percent < 0.5) {
				
				result.setDiscrete(discrete);
				
			}
			else {
				
				result.setDiscrete(mandelbulb.getDiscrete());
				
			}
			
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return fractal.clone();
			
		}
		
	}
	
}