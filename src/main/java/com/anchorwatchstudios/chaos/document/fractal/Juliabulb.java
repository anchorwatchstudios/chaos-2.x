package com.anchorwatchstudios.chaos.document.fractal;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Discrete;
import com.anchorwatchstudios.chaos.math.Interpolation;
import com.anchorwatchstudios.chaos.math.Triplex;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * Represents the Julia set in triplex space. Contains
 * all the information needed to calculate it.
 * 
 * @author Anthony Atella
 */
public class Juliabulb implements EscapeTimeFractal<Triplex>, Discrete {

	private static final Triplex DEFAULT_C = new Triplex(-0.223, 0.745, 0.0);
	private static final double DEFAULT_N = 8.0;
	private Triplex c;
	private double n;
	private int iterations;
	private double threshold;
	private boolean discrete;
	
	/**
	 * Constructs a new <code>JuliaBulb</code> with default values.
	 */
	public Juliabulb() {
	
		c = DEFAULT_C;
		n = DEFAULT_N;
		iterations = DEFAULT_ITERATIONS;
		threshold = DEFAULT_THRESHOLD;
		discrete = Discrete.DEFAULT;
		
	}
	
	/**
	 * Constructs a new <code>JuliaBulb</code> with the given values.
	 * 
	 * @param c The triplex number 'C', from zn = zn-1 + C
	 * @param n The power this <code>Juliabulb</code> is raised to
	 * @param iterations The number of iterations
	 * @param threshold The threshold
	 * @param discrete Whether or not this fractal is calculated discretely
	 */
	public Juliabulb(Triplex c, double n, int iterations, double threshold, boolean discrete) {
		
		this.c = c;
		this.n = n;
		this.iterations = iterations;
		this.threshold = threshold;
		this.discrete = discrete;
		
	}
	
	/**
	 * Returns the triplex number 'C', from zn = zn-1 + C.
	 * 
	 * @return The triplex number 'C', from zn = zn-1 + C
	 */
	public Triplex getC() {
	
		return c;
	
	}

	/**
	 * Sets the triplex number 'C', from zn = zn-1 + C.
	 * 
	 * @param c The triplex number 'C', from zn = zn-1 + C
	 */
	public void setC(Triplex c) {
	
		this.c = c;
	
	}
	
	/**
	 * Returns the power this <code>Juliabulb</code> is raised
	 * to.
	 * 
	 * @return The power this <code>Juliabulb</code> is raised to
	 */
	public double getN() {
		
		return n;
		
	}

	/**
	 * Sets the power this <code>Juliabulb</code> is raised
	 * to.
	 * 
	 * @param n The power this <code>Juliabulb</code> is raised to
	 */
	public void setN(double n) {
		
		this.n = n;
		
	}

	@Override
	public Vec4d isInSet(Triplex point) {

		// TODO: Unimplemented
		return new Vec4d();
		
	}

	@Override
	public int getIterations() {

		return iterations;
		
	}

	@Override
	public void setIterations(int iterations) {

		this.iterations = iterations;
		
	}

	@Override
	public double getThreshold() {

		return threshold;
		
	}

	@Override
	public void setThreshold(double threshold) {

		this.threshold = threshold;
		
	}

	@Override
	public String getDescription() {

		return "Juliabulb: \n" +
				"  c: " + c.toString() + "\n" +
				"  n: " + n + "\n" +
				"  Iterations: " + iterations + "\n" +
				"  Threshold: " + threshold + "\n" +
				"  Discrete: " + discrete + "\n\n";

	}

	@Override
	public Juliabulb clone() {
		
		return new Juliabulb(new Triplex(c.getA(), c.getB(), c.getC()), n, iterations, threshold, discrete);
		
	}
	
	@Override
	public Class<Triplex> getType() {

		return Triplex.class;

	}

	@Override
	public boolean getDiscrete() {

		return discrete;
		
	}

	@Override
	public void setDiscrete(boolean discrete) {

		this.discrete = discrete;
		
	}
	
	@Override
	public String toString() {
		
		return Strings.FRACTAL_JULIABULB;
		
	}

	@Override
	public Fractal interpolate(Fractal fractal, double percent) {

		if(fractal instanceof Juliabulb) {
			
			Juliabulb juliaBulb = (Juliabulb) fractal;
			Juliabulb result = new Juliabulb();
			result.setC(c.interpolate(juliaBulb.getC(), percent));
			result.setN(Interpolation.interpolate(n, juliaBulb.getN(), percent));
			result.setIterations(Interpolation.interpolate(iterations, juliaBulb.getIterations(), percent));
			result.setThreshold(Interpolation.interpolate(threshold, juliaBulb.getThreshold(), percent));
			
			if(percent < 0.5) {
				
				result.setDiscrete(discrete);
				
			}
			else {
				
				result.setDiscrete(juliaBulb.getDiscrete());
				
			}
			
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return fractal.clone();
			
		}
		
	}

}