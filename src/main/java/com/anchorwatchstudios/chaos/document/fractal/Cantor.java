package com.anchorwatchstudios.chaos.document.fractal;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Interpolation;

/**
 * Represents the Cantor set. Holds all the 
 * information used to calculate it.
 * 
 * @author Anthony Atella
 */
public class Cantor implements Fractal {
	
	public static final double DEFAULT_THRESHOLD = 1;
	public static final double DEFAULT_SPACE = 25;
	
	private double space;
	private double threshold;
	
	/**
	 * Constructs a new Cantor set with
	 * default values.
	 */
	public Cantor() {
	
		threshold = DEFAULT_THRESHOLD;
		space = DEFAULT_SPACE;
		
	}
	
	/**
	 * Constructs a new Cantor with the given values.
	 * 
	 * @param space The space between the lines
	 * @param threshold The width to stop recursing at
	 */
	public Cantor(double space, double threshold) {
		
		this();
		this.threshold = threshold;
		this.space = space;
		
	}

	/**
	 * Returns the space between lines.
	 * 
	 * @return The space between lines
	 */
	public double getSpace() {
		
		return space;
		
	}

	/**
	 * Sets the space between lines.
	 * 
	 * @param space The space between lines
	 */
	public void setSpace(double space) {
		
		this.space = space;
		
	}

	/**
	 * Returns the width to stop recursing
	 * at.
	 * 
	 * @return The width to stop recursing
	 * at
	 */
	public double getThreshold() {
		
		return threshold;
		
	}
	
	/**
	 * Sets the width to stop recursing
	 * at.
	 * 
	 * @param threshold The width to stop recursing
	 * at
	 */
	public void setThreshold(double threshold) {
		
		this.threshold = threshold;
		
	}
	
	@Override
	public String toString() {
		
		return Strings.FRACTAL_CANTOR;
		
	}

	@Override
	public String getDescription() {

		String result = "Cantor: \n";
		result += "  Space: " + space + "\n";
		result += "  Threshold: " + threshold + "\n\n";
		return result;

	}
	
	@Override
	public Cantor clone() {
		
		return new Cantor(space, threshold);
		
	}

	@Override
	public Fractal interpolate(Fractal fractal, double percent) {

		if(fractal instanceof Cantor) {
			
			Cantor cantor = (Cantor) fractal;
			Cantor result = new Cantor();
			result.setSpace(Interpolation.interpolate(space, cantor.getSpace(), percent));
			result.setThreshold(Interpolation.interpolate(threshold, cantor.getThreshold(), percent));
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return fractal.clone();
			
		}

	}
	
}
