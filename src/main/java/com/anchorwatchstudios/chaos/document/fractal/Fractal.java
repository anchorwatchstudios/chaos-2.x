package com.anchorwatchstudios.chaos.document.fractal;

import com.anchorwatchstudios.chaos.math.Interpolation;

/**
 * A common interface for all fractals.
 * 
 * @author Anthony Atella
 */
public interface Fractal extends Interpolation<Fractal>, Cloneable {

	/**
	 * Returns a description of this Fractal.
	 * 
	 * @return A description of this Fractal.
	 */
	String getDescription();
	
	/**
	 * Returns a clone of this Fractal.
	 * 
	 * @return A clone of this Fractal.
	 */
	Fractal clone();
	
}