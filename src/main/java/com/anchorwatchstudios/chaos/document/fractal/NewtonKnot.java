package com.anchorwatchstudios.chaos.document.fractal;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Interpolation;
import com.anchorwatchstudios.chaos.math.Triplex;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * Represents a Newtonian knot. Contains all the information
 * needed to calculate it.
 * 
 * @author Anthony Atella
 */
public class NewtonKnot implements EscapeTimeFractal<Triplex> {

	public static final double DEFAULT_THRESHOLD = 0.5;
	public static final Triplex DEFAULT_ALPHA = new Triplex(2, 0, 0);
	public static final Triplex DEFAULT_N = new Triplex(3, 0, 0);
	private Triplex alpha;
	private Triplex n;
	private int iterations;
	private double threshold;
	
	/**
	 * Constructs a new <code>NewtonKnot</code> with default values.
	 */
	public NewtonKnot() {
		
		iterations = EscapeTimeFractal.DEFAULT_ITERATIONS;
		threshold = DEFAULT_THRESHOLD;
		n = DEFAULT_N;
		alpha = DEFAULT_ALPHA;
		
	}
	
	/**
	 * Constructs a new <code>NewtonBasin</code> with the given values.
	 * 
	 * @param alpha The triplex number 'a', Zn+1 = Zn - (Zn^x - a) / (Zn^y - a)
	 * @param n The triplex number 'n', Zn+1 = Zn - (Zn^x - a) / (Zn^y - a)
	 * @param iterations The number of iterations
	 * @param threshold The threshold
	 */
	public NewtonKnot(Triplex alpha, Triplex n, int iterations, double threshold) {
		
		this.alpha = alpha;
		this.n = n;
		this.iterations = iterations;
		this.threshold = threshold;
		
	}
	
	/**
	 * Returns the complex number 'a', Zn+1 = Zn - (Zn^x - a) / (Zn^y - a).
	 * 
	 * @return The complex number 'a', Zn+1 = Zn - (Zn^x - a) / (Zn^y - a)
	 */
	public Triplex getAlpha() {
		
		return alpha;
		
	}

	/**
	 * Sets the complex number 'a', Zn+1 = Zn - (Zn^x - a) / (Zn^y - a).
	 * 
	 * @param alpha The complex number 'a', Zn+1 = Zn - (Zn^x - a) / (Zn^y - a)
	 */
	public void setAlpha(Triplex alpha) {
		
		this.alpha = alpha;
		
	}

	/**
	 * Returns the complex number 'x', Zn+1 = Zn - (Zn^x - a) / (Zn^y - a).
	 * 
	 * @return The complex number 'x', Zn+1 = Zn - (Zn^x - a) / (Zn^y - a)
	 */
	public Triplex getN() {
		
		return n;
		
	}

	/**
	 * Sets the complex number 'n', from Zn+1 = Zn - (Zn^x - a) / (Zn^y - a).
	 * 
	 * @param n The complex number 'n', from Zn+1 = Zn - (Zn^x - a) / (Zn^y - a)
	 */
	public void setN(Triplex n) {
		
		this.n = n;
		
	}

	@Override
	public int getIterations() {
		
		return iterations;
		
	}

	@Override
	public void setIterations(int iterations) {
		
		this.iterations = iterations;
		
	}

	@Override
	public double getThreshold() {
		
		return threshold;
		
	}

	@Override
	public void setThreshold(double threshold) {
		
		this.threshold = threshold;
		
	}

	@Override
	public Vec4d isInSet(Triplex z) {

		double result = 0;
		Triplex one = new Triplex(1, 0, 0);
		double minDistance = 1000.0;

		for(int i = 0;i < iterations;i++) {

			Triplex numerator = z.pow((int) n.getA());
			numerator = numerator.add(one);
			numerator = numerator.multiply(alpha);
			Triplex denominator = z.pow((int) n.subtract(one).getA());
			denominator = denominator.multiply(n);
			Triplex oldZ = z;
			z = numerator.divide(denominator);
			double l = z.subtract(oldZ).abs();
			minDistance = Math.min(minDistance, l);
			
			if(l <= Math.pow(threshold, 2.0)) {

				result++;

			}
			else {

				break;
				
			}

		}

		double length = z.abs();
		return new Vec4d(result / (double) iterations, length, minDistance, 0.33 * Math.log(length * length) + 1.0);
		
	}

	@Override
	public String toString() {
		
		return Strings.FRACTAL_NEWTON_KNOT;
		
	}

	@Override
	public String getDescription() {

		String result = "Newton Knot: \n";
		result += "  Alpha: " + alpha.toString() + "\n";
		result += "  N: " + n.toString() + "\n";
		result += "  Iterations: " + iterations + "\n";
		result += "  Threshold: " + threshold + "\n\n";
		return result;
	}
	
	@Override
	public NewtonKnot clone() {
		
		return new NewtonKnot(new Triplex(alpha.getA(), alpha.getB(), alpha.getC()), new Triplex(n.getA(), n.getB(), n.getC()), iterations, threshold);
		
	}
	
	@Override
	public Class<Triplex> getType() {

		return Triplex.class;

	}

	@Override
	public Fractal interpolate(Fractal fractal, double percent) {

		if(fractal instanceof NewtonKnot) {
			
			NewtonKnot newtonBasin = (NewtonKnot) fractal;
			NewtonKnot result = new NewtonKnot();
			result.setAlpha(alpha.interpolate(newtonBasin.getAlpha(), percent));
			result.setN(n.interpolate(newtonBasin.getN(), percent));
			result.setIterations(Interpolation.interpolate(iterations, newtonBasin.getIterations(), percent));
			result.setThreshold(Interpolation.interpolate(threshold, newtonBasin.getThreshold(), percent));
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return fractal.clone();
			
		}
		
	}
	
}
