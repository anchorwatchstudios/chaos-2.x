package com.anchorwatchstudios.chaos.document.fractal;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.math.Discrete;
import com.anchorwatchstudios.chaos.math.Interpolation;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * Represents the Julia set. Contains all the information
 * needed to calculate it.
 * 
 * @author Anthony Atella
 */
public class Julia implements EscapeTimeFractal<Complex>, Discrete {

	public static final Complex DEFAULT_C = new Complex(-0.223, 0.745);
	private Complex c;
	private int iterations;
	private double threshold;
	private boolean discrete;

	/**
	 * Constructs a new <code>Julia</code> with default values.
	 */
	public Julia() {
		
		iterations = EscapeTimeFractal.DEFAULT_ITERATIONS;
		threshold = EscapeTimeFractal.DEFAULT_THRESHOLD;
		c = DEFAULT_C;
		discrete = Discrete.DEFAULT;
		
	}
	
	/**
	 * Constructs a new <code>Julia</code> with the given values.
	 * 
	 * @param c The complex number 'C', from zn = zn-1 + C
	 * @param iterations The number of iterations
	 * @param threshold The threshold
	 * @param discrete Whether or not this fractal is calculated discretely
	 */
	public Julia(Complex c, int iterations, double threshold, boolean discrete) {
		
		this.c = c;
		this.iterations = iterations;
		this.threshold = threshold;
		this.discrete = discrete;
		
	}
	
	/**
	 * Returns the complex number 'C', from zn = zn-1 + C.
	 * 
	 * @return The complex number 'C', from zn = zn-1 + C
	 */
	public Complex getC() {
		
		return c;
		
	}

	/**
	 * Sets the complex number 'C', from zn = zn-1 + C.
	 * 
	 * @param c The complex number 'C', from zn = zn-1 + C
	 */
	public void setC(Complex c) {
		
		this.c = c;
		
	}
		
	@Override
	public Vec4d isInSet(Complex point) {

		Complex z = new Complex(point.getA(), point.getB());
		double result = 0;
		double minDistance = 1000.0;
		
		for(int i = 0;i < iterations;i++) {
			
			z = z.pow(2).add(c);
			double l = z.abs();
			minDistance = Math.min(minDistance, l);
			
			if(l >= Math.pow(threshold, 2.0)) {
				
				break;
				
			}
			
			result++;
			
		}
		
		if(!discrete) {
			
			result = EscapeTimeFractal.getPreciseIterations(z, result, iterations);
			
		}
		
		double length = z.abs();
		return new Vec4d(result / (double) iterations, length, minDistance, 0.33 * Math.log(length * length) + 1.0);
		
	}
	
	@Override
	public int getIterations() {
		
		return iterations;
		
	}

	@Override
	public void setIterations(int iterations) {
		
		this.iterations = iterations;
		
	}
	
	@Override
	public double getThreshold() {
		
		return threshold;
		
	}

	@Override
	public void setThreshold(double threshold) {
		
		this.threshold = threshold;
		
	}
	
	@Override
	public boolean getDiscrete() {
		
		return discrete;
		
	}

	@Override
	public void setDiscrete(boolean discrete) {
		
		this.discrete = discrete;
		
	}
	
	@Override
	public String toString() {
		
		return Strings.FRACTAL_JULIA;
		
	}

	@Override
	public String getDescription() {

		String result = "Julia: \n";
		result += "  C: " + c + "\n";
		result += "  Iterations: " + iterations + "\n";
		result += "  Threshold: " + threshold + "\n";
		result += "  Discrete: " + discrete + "\n\n";
		return result;
	
	}
	
	@Override
	public Julia clone() {
		
		return new Julia(new Complex(c.getA(), c.getB()), iterations, threshold, discrete);
		
	}

	@Override
	public Class<Complex> getType() {

		return Complex.class;

	}

	@Override
	public Fractal interpolate(Fractal fractal, double percent) {

		if(fractal instanceof Julia) {
			
			Julia julia = (Julia) fractal;
			Julia result = new Julia();
			result.setC(c.interpolate(julia.getC(), percent));
			result.setIterations(Interpolation.interpolate(iterations, julia.getIterations(), percent));
			result.setThreshold(Interpolation.interpolate(threshold, julia.getThreshold(), percent));
			
			if(percent < 0.5) {
				
				result.setDiscrete(discrete);
				
			}
			else {
				
				result.setDiscrete(julia.getDiscrete());
				
			}
			
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return fractal.clone();
			
		}
		
	}
	
}