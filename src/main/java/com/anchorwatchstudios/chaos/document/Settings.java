package com.anchorwatchstudios.chaos.document;

import com.anchorwatchstudios.chaos.document.render.RenderImplementation;
import com.anchorwatchstudios.chaos.math.Vec2i;

/**
 * Holds all of the settings that are broad and
 * relate to the <code>Document</code>, not
 * its components.
 * 
 * @author Anthony Atella
 *
 */
public class Settings implements Cloneable {

	private static final Vec2i DEFAULT_RESOLUTION = new Vec2i(400, 400);
	private static final RenderImplementation DEFAULT_RENDER_IMPL = RenderImplementation.OPENGL4;
	private static final int DEFAULT_FPS = 30;
	private Vec2i resolution;
	private RenderImplementation renderImplementation;
	private int fps;
	private int fpk;
	
	/**
	 * Constructs a new <code>Settings</code> with default values.
	 */
	public Settings() {
		
		renderImplementation = DEFAULT_RENDER_IMPL;
		resolution = DEFAULT_RESOLUTION;
		fps = DEFAULT_FPS;
		fpk = DEFAULT_FPS;
		
	}
	
	/**
	 * Constructs a new <code>Settings</code> with the given values.
	 * 
	 * @param resolution The resolution
	 * @param renderImplementation The render implementation
	 * @param fps The frames per second
	 * @param fpk The frames per key-frame
	 */
	public Settings(Vec2i resolution, RenderImplementation renderImplementation, int fps, int fpk) {
		
		this.resolution = resolution;
		this.renderImplementation = renderImplementation;
		this.fps = fps;
		this.fpk = fpk;
		
	}

	/**
	 * Returns the <code>Document</code> resolution.
	 * 
	 * @return The <code>Document</code> resolution
	 */
	public Vec2i getResolution() {
		
		return resolution;
		
	}

	/**
	 * Sets the <code>Document</code> resolution to the
	 * given resolution.
	 * 
	 * @param resolution The new <code>Document</code> resolution
	 */
	public void setResolution(Vec2i resolution) {
		
		this.resolution = resolution;
		
	}

	/**
	 * Returns the <code>RenderImplementation</code> that this
	 * <code>Document</code> uses.
	 * 
	 * @return The <code>RenderImplementation</code> that this <code>Document</code> uses
	 */
	public RenderImplementation getRenderImplementation() {
		
		return renderImplementation;
		
	}

	/**
	 * Sets this <code>Document</code>s <code>RenderImplementation</code>
	 * to the given <code>RenderImplementation</code>.
	 * 
	 * @param renderImplementation The new <code>RenderImplementation</code>
	 */
	public void setRenderImplementation(RenderImplementation renderImplementation) {
		
		this.renderImplementation = renderImplementation;
		
	}

	/**
	 * Returns the video frames per second.
	 * 
	 * @return The video frames per second.
	 */
	public int getFPS() {
		
		return fps;
		
	}
	
	/**
	 * Sets the video frames per second.
	 * 
	 * @param fps The video frames per second.
	 */
	public void setFPS(int fps) {
		
		this.fps = fps;
		
	}
	
	/**
	 * Returns the frames per key-frame.
	 * 
	 * @return The frames per key-frame
	 */
	public int getFPK() {
		
		return fpk;
		
	}
	
	/**
	 * Sets the frames per key-frame.
	 * 
	 * @param fpk The frames per key-frame.
	 */
	public void setFPK(int fpk) {
		
		this.fpk = fpk;
		
	}
	
	/**
	 * Returns a text description of this <code>Document</code>s settings and
	 * its components.
	 * 
	 * @return A text description of this <code>Document</code>s settings and its components.
	 */
	public String getDescription() {

		return "Settings: \n" +
				"  Resolution: " + resolution.getX() + "x" + resolution.getY() + "\n" +
				"  FPS: " + fps + "\n" +
				"  FPK: " + fpk + "\n" +
				"  RenderImplementation: " + renderImplementation.toString() + "\n\n";
	}
	
	@Override
	public Settings clone() {
		
		return new Settings(new Vec2i((int) resolution.getX(), (int) resolution.getY()), DEFAULT_RENDER_IMPL, fps, fpk);
		
	}

}
