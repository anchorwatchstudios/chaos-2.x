package com.anchorwatchstudios.chaos.document;

/**
 * Observes a <code>DocumentManager</code> and is notified by the
 * <code>DocumentManager</code> when the current <code>Document</code> 
 * is edited. Any class that needs to access the current document when it has been edited 
 * must implement this interface. This interface utilizes the observer pattern in 
 * a second level of observation because a <code>DocumentManagerObserver</code> is observing
 * a <code>DocumentManager</code>, which is an extension of <code>DocumentObserver</code>. 
 * In other words, this observer observes an observer. This centralizes the <code>Document</code> 
 * observation flow and allows multiple documents to be observed through one registration.
 * 
 * @author Anthony Atella
 */
public interface DocumentManagerObserver {

	/**
	 * Notifies this <code>DocumentManagerObserver</code> that
	 * the current document has changed.
	 * 
	 * @param documentManager The <code>DocumentManager</code> that
	 * sent the notification.
	 */
	void notify(DocumentManager documentManager);
	
}