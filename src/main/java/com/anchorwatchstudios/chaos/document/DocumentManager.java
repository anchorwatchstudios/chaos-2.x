package com.anchorwatchstudios.chaos.document;

/**
 * A <code>DocumentObserver</code> that manages multiple <code>Document</code>s.
 * Implementations must be able to register and unregister 
 * <code>DocumentManagerObserver</code>s, and alert those observers
 * when the current <code>Document</code> has changed.
 * 
 * @author Anthony Atella
 */
public interface DocumentManager extends DocumentObserver {

	/**
	 * Adds the given <code>Document</code> to this
	 * <code>DocumentManager</code>.
	 * 
	 * @param document The <code>Document</code> to be
	 * added to this <code>DocumentManager</code>
	 */
	void add(Document document);
	
	/**
	 * Removes the <code>Document</code> at the given index.
	 * 
	 * @param index the index of the <code>Document</code> to be
	 * removed
	 */
	void remove(int index);
	
	/**
	 * Returns the current document index.
	 * 
	 * @return The current index
	 */
	int getIndex();
	
	/**
	 * Sets the current document index.
	 * 
	 * @param index The index
	 */
	void setIndex(int index);
	
	/**
	 * Returns the number of <code>Document</code>s.
	 * 
	 * @return The number of <code>Document</code>s
	 */
	int getDocumentCount();
	
	/**
	 * Returns the current <code>Document</code>.
	 * 
	 * @return The current <code>Document</code>
	 */
	Document getCurrent();
	
	/**
	 * Returns true if this <code>DocumentManager</code>
	 * contains the given <code>Document</code>.
	 * 
	 * @param document The document to be queried
	 * @return true if this <code>DocumentManager</code>
	 * contains the given <code>Document</code>
	 */
	boolean hasDocument(Document document);
	
	/**
	 * Registers an observer to be alerted when the current document 
	 * has been edited.
	 * 
	 * @param observer The <code>DocumentManagerObserver</code> to be
	 * registered
	 */
	void registerObserver(DocumentManagerObserver observer);
	
	/**
	 * Unregisters a <code>DocumentManagerObserver</code> with this 
	 * <code>DocumentManager</code>.
	 * 
	 * @param observer The <code>DocumentManagerObserver</code> to be
	 * unregistered
	 */
	void unregisterObserver(DocumentManagerObserver observer);
	
	/**
	 * Notifies all registered <code>DocumentManagerObserver</code>s that
	 * the current <code>Document</code> has been edited.
	 */
	void notifyObservers();
	
}