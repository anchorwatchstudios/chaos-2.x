package com.anchorwatchstudios.chaos.document;

import java.io.File;
import java.util.ArrayList;

import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.document.shader.Shader;

/**
 * An implementation of the <code>Document</code> interface that uses
 * an <code>ArrayList</code> to store its <code>DocumentObserver</code>s
 * and <code>KeyFrame</code>s.
 * 
 * @author Anthony Atella
 */
public class ArrayListFRC2Document implements FRC2Document {
	
	private static int COUNT = 0;
	private transient boolean edited;
	private transient ArrayList<DocumentObserver> documentObserver;
	private transient File file;
	private ArrayList<KeyFrame> keyFrames;
	private int currentKeyFrame;
	private Settings settings;

	/**
	 * Creates a new <code>ArrayListFRC2Document</code> with
	 * an empty list of observers and a null <code>File</code>.
	 * Edited is set to false by default.
	 */
	public ArrayListFRC2Document() {

		COUNT++;
		file = new File("untitled" + COUNT);
		settings = new Settings();
		edited = false;
		documentObserver = new ArrayList<DocumentObserver>();
		keyFrames = new ArrayList<KeyFrame>();
		currentKeyFrame = -1;

	}

	/**
	 * Creates a new <code>ArrayListFRC2Document</code> with
	 * an empty list of observers and the given <code>File</code>.
	 * Edited is set to false by default.
	 * 
	 * @param file The document's <code>file</code>
	 */
	public ArrayListFRC2Document(File file) {
		
		this();
		this.file = file;
		
	}
	
	/**
	 * Creates a new <code>ArrayListFRC2Document</code> with
	 * the given information.
	 * 
	 * @param fractal A fractal
	 * @param renderMethod A render method
	 * @param settings A settings object
	 * @param shader A shader
	 */
	public ArrayListFRC2Document(Fractal fractal, RenderMethod renderMethod, Settings settings, Shader shader) {

		this();
		this.settings = settings;
		addKeyFrame(new KeyFrame(fractal, shader, renderMethod));
		
	}

	@Override
	public void registerObserver(DocumentObserver documentObserver) {

		this.documentObserver.add(documentObserver);

	}

	@Override
	public void unregisterObserver(DocumentObserver documentObserver) {
		
		this.documentObserver.remove(documentObserver);
		
	}
	
	@Override
	public boolean isEdited() {

		return edited;

	}
	
	@Override
	public void setEdited(boolean edited) {
		
		this.edited = edited;
		
		if(edited) {
			
			notifyObservers();
			
		}
		
	}
	
	@Override
	public File getFile() {

		return file;
		
	}

	@Override
	public void setFile(File file) {

		this.file = file;
		
	}

	@Override
	public void notifyObservers() {

		for(int i = 0;i < documentObserver.size();i++) {

			documentObserver.get(i).notify(this);

		}
		
	}

	@Override
	public String getDescription() {
	
		String result = "";
		result += getCurrentKeyFrame().getDescription();
		result += settings.getDescription();
		return result;
		
	}

	@Override
	public KeyFrame getKeyFrame(int index) {

		return keyFrames.get(index);

	}

	@Override
	public KeyFrame getCurrentKeyFrame() {

		return keyFrames.get(currentKeyFrame);

	}

	@Override
	public void setKeyFrameIndex(int index) {

		this.currentKeyFrame = index;
		
	}

	@Override
	public int getKeyFrameIndex() {

		return currentKeyFrame;

	}

	@Override
	public void addKeyFrame(KeyFrame keyFrame) {

		currentKeyFrame += 1;
		keyFrames.add(currentKeyFrame, keyFrame);
		
	}

	@Override
	public void removeKeyFrame(int index) {

		if(currentKeyFrame >= index) {
			
			currentKeyFrame--;
			
		}
		
		keyFrames.remove(index);
		
		
	}

	@Override
	public void removeCurrentKeyFrame() {

		removeKeyFrame(currentKeyFrame);
		
	}

	@Override
	public int getKeyFrameCount() {

		return keyFrames.size();
		
	}

	@Override
	public Settings getSettings() {

		return settings;
		
	}

	@Override
	public void setSettings(Settings settings) {

		this.settings = settings;
		
	}
	
}
