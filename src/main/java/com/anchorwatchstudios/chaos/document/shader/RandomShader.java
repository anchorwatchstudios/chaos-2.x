package com.anchorwatchstudios.chaos.document.shader;

import java.util.Random;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * A shader that outputs random colors.
 * 
 * @author Anthony Atella
 */
public class RandomShader implements Shader {

	private Random random;
	private boolean inverted;
	
	/**
	 * Constructs a new <code>RandomShader</code> with default values.
	 */
	public RandomShader() {
	
		random = new Random();
		inverted = false;
		
	}
	
	@Override
	public String getDescription() {
		return "Random Shader: \n" +
				"  Inverted: " + inverted + "\n\n";
	}

	@Override
	public Color getFragment(Vec4d value) {

		return new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255), 255);

	}

	@Override
	public boolean isInverted() {

		return inverted;
		
	}
	
	@Override
	public void setInverted(boolean inverted) {
		
		this.inverted = inverted;
		
	}
	
	@Override
	public RandomShader clone() {
		
		RandomShader result = new RandomShader();
		result.setInverted(inverted);
		return result;
	
	}

	@Override
	public Shader interpolate(Shader item, double percent) {

		if(item instanceof RandomShader) {
			
			RandomShader randomShader = (RandomShader) item;
			RandomShader result = new RandomShader();
			
			if(percent < 0.5) {
				
				result.setInverted(inverted);
				
			}
			else {
				
				result.setInverted(randomShader.isInverted());
				
			}
			
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return item.clone();
			
		}
		
	}

	@Override
	public String toString() {
	
		return Strings.RANDOM;
		
	}
	
}
