package com.anchorwatchstudios.chaos.document.shader;

/**
 * Represents anything with three colors.
 * 
 * @author Anthony Atella
 *
 */
public interface Trichromatic extends Dichromatic {

	/**
	 * Returns the third color
	 * 
	 * @return The third color
	 */
	Color getColor3();
	
	/**
	 * Sets the third color to the given
	 * color.
	 * 
	 * @param color The new color
	 */
	void setColor3(Color color);
	
}