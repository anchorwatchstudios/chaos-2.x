package com.anchorwatchstudios.chaos.document.shader;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * A simple dichromatic shader that returns the first color
 * if the value is 0 and the second color multiplied by
 * the given value otherwise.
 * 
 * @author Anthony Atella
 */
public class DichromaticShader implements Shader, Dichromatic {

	private static final Color DEFAULT_COLOR1 = new Color(0, 255, 0, 255);
	private static final Color DEFAULT_COLOR2 = new Color();
	private Color color1;
	private Color color2;
	private boolean inverted;
	
	/**
	 * Constructs a new <code>DichromaticShader</code> with default values.
	 */
	public DichromaticShader() {
		
		color1 = DEFAULT_COLOR1;
		color2 = DEFAULT_COLOR2;
		inverted = false;
		
	}
	
	@Override
	public boolean isInverted() {

		return inverted;

	}
	
	@Override
	public void setInverted(boolean inverted) {
		
		this.inverted = inverted;
		
	}
	
	@Override
	public Color getColor1() {

		return color1;
	
	}

	@Override
	public void setColor1(Color color1) {

		this.color1 = color1;
		
	}

	@Override
	public Color getColor2() {

		return color2;

	}

	@Override
	public void setColor2(Color color2) {

		this.color2 = color2;
		
	}

	@Override
	public String getDescription() {

		String result = "Dichromatic Shader: \n";
		result += "  Color1: " + String.format("#%02x%02x%02x", color1.getRed(), color1.getBlue(), color1.getGreen()) + "\n";
		result += "  Color2: " + String.format("#%02x%02x%02x", color2.getRed(), color2.getBlue(), color2.getGreen()) + "\n";
		result += "  Inverted: " + inverted + "\n\n";
		return result;

	}

	@Override
	public Color getFragment(Vec4d value) {
		
		double percent = value.getX();
		
		if(inverted) {
			
			percent = 1.0 - percent;
			
		}
		if(percent < 1.0) {
			
			return new Color((int) (color1.getRed() * percent), (int) (color1.getGreen() * percent), (int) (color1.getBlue() * percent), color1.getAlpha());
			
		}

		return color2;

	}
	
	@Override
	public DichromaticShader clone() {
		
		DichromaticShader result = new DichromaticShader();
		result.setColor1(new Color(color1.getRed(), color1.getGreen(), color1.getBlue(), color1.getAlpha()));
		result.setColor2(new Color(color2.getRed(), color2.getGreen(), color2.getBlue(), color2.getAlpha()));
		result.setInverted(inverted);
		return result;
		
	}

	@Override
	public Shader interpolate(Shader item, double percent) {

		if(item instanceof DichromaticShader) {
			
			DichromaticShader dichromaticShader = (DichromaticShader) item;
			DichromaticShader result = new DichromaticShader();
			result.setColor1(color1.interpolate(dichromaticShader.getColor1(), percent));
			result.setColor2(color2.interpolate(dichromaticShader.getColor2(), percent));
			
			if(percent < 0.5) {
				
				result.setInverted(inverted);
				
			}
			else {
				
				result.setInverted(dichromaticShader.isInverted());
				
			}
			
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return item.clone();
			
		}
		
	}

	@Override
	public String toString() {
	
		return Strings.DICHROMATIC;
		
	}
	
}
