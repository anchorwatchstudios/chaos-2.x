package com.anchorwatchstudios.chaos.document.shader;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * A plain shader that has one color. The value returned by <code>getFragment(double)</code>
 * is the color multiplied by the value.
 * 
 * @author Anthony Atella
 *
 */
public class MonochromaticShader implements Shader, Monochromatic {

	public static final Color DEFAULT_COLOR = new Color(255, 255, 255, 255);
	private Color color;
	private boolean inverted;
	
	/**
	 * Constructs a new <code>MonochromaticShader</code> with a default color.
	 */
	public MonochromaticShader() {
		
		color = DEFAULT_COLOR;
		
	}
	
	@Override
	public boolean isInverted() {

		return inverted;
	}
	
	@Override
	public void setInverted(boolean inverted) {
		
		this.inverted = inverted;
		
	}
	
	@Override
	public Color getColor1() {

		return color;
	
	}

	@Override
	public void setColor1(Color color) {

		this.color = color;
		
	}

	@Override
	public String getDescription() {

		String result = "Monochromatic: \n";
		result += "  Color: " + String.format("#%02x%02x%02x", color.getRed(), color.getBlue(), color.getGreen()) + "\n";
		result += "  Inverted: " + inverted + "\n\n";
		return result;

	}

	@Override
	public Color getFragment(Vec4d value) {

		int red = (int) (color.getRed() * value.getX());
		int green = (int) (color.getGreen() * value.getX());
		int blue = (int) (color.getBlue() * value.getX());
		return new Color(red, green, blue, color.getAlpha());
		
	}

	@Override
	public MonochromaticShader clone() {
		
		MonochromaticShader result = new MonochromaticShader();
		result.setColor1(new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()));
		result.setInverted(inverted);
		return result;
		
	}

	@Override
	public Shader interpolate(Shader item, double percent) {

		if(item instanceof MonochromaticShader) {
			
			MonochromaticShader monochromaticShader = (MonochromaticShader) item;
			MonochromaticShader result = new MonochromaticShader();
			result.setColor1(color.interpolate(monochromaticShader.getColor1(), percent));
			
			if(percent < 0.5) {
				
				result.setInverted(inverted);
				
			}
			else {
				
				result.setInverted(monochromaticShader.isInverted());
				
			}
			
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return item.clone();
			
		}
		
	}
	
	@Override
	public String toString() {
	
		return Strings.MONOCHROMATIC;
		
	}
	
}
