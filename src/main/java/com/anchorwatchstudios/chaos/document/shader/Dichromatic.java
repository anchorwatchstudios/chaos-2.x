package com.anchorwatchstudios.chaos.document.shader;

/**
 * Represents anything with two colors.
 * 
 * @author Anthony Atella
 *
 */
public interface Dichromatic extends Monochromatic {
	
	/**
	 * Returns the second color
	 * 
	 * @return The second color
	 */
	Color getColor2();
	
	/**
	 * Sets the second color to the given
	 * color.
	 * 
	 * @param color The new color
	 */
	void setColor2(Color color);
	
}