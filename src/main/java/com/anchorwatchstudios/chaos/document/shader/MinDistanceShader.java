package com.anchorwatchstudios.chaos.document.shader;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * A shader that accents a base color with another based
 * on the minimum distance.
 * 
 * @author Anthony Atella
 */
public class MinDistanceShader implements Shader, Trichromatic {

	private Color color1;
	private Color color2;
	private Color color3;
	private boolean inverted;

	/**
	 * Constructs a new MinDistanceShader with default values.
	 */
	public MinDistanceShader() {

		color1 = new Color();
		color2 = new Color(0, 0, 255, 255);
		color3 = new Color(255, 255, 0, 255);
		inverted = false;

	}

	/**
	 * Constructs a new MinDistanceShader with the given values.
	 * 
	 * @param color1 The first color
	 * @param color2 The second color
	 * @param color3 The third color
	 * @param inverted Whether or not to invert values
	 */
	public MinDistanceShader(Color color1, Color color2, Color color3, boolean inverted) {

		this.color1 = color1;
		this.color2 = color2;
		this.color3 = color3;
		this.inverted = inverted;

	}

	@Override
	public Color getColor1() {

		return color1;

	}

	@Override
	public void setColor1(Color color1) {

		this.color1 = color1;

	}

	@Override
	public Color getColor2() {

		return color2;

	}

	@Override
	public void setColor2(Color color2) {

		this.color2 = color2;

	}

	@Override
	public Color getColor3() {

		return color3;

	}

	@Override
	public void setColor3(Color color3) {

		this.color3 = color3;

	}

	@Override
	public Shader interpolate(Shader item, double percent) {

		if(item instanceof MinDistanceShader) {

			MinDistanceShader s = (MinDistanceShader) item;
			MinDistanceShader result = new MinDistanceShader();
			result.setColor1(color1.interpolate(s.getColor1(), percent));
			result.setColor2(color2.interpolate(s.getColor2(), percent));
			result.setColor3(color3.interpolate(s.getColor3(), percent));

			if(percent < 0.5) {

				result.setInverted(inverted);

			}
			else {

				result.setInverted(s.isInverted());

			}

			return result;

		}
		else if(percent < 0.5) {

			return this.clone();

		}
		else {

			return item.clone();

		}

	}

	@Override
	public String getDescription() {

		String result = "Min. Distance Shader: \n";
		result += "  Color1: " + String.format("#%02x%02x%02x", color1.getRed(), color1.getBlue(), color1.getGreen()) + "\n";
		result += "  Color2: " + String.format("#%02x%02x%02x", color2.getRed(), color2.getBlue(), color2.getGreen()) + "\n";
		result += "  Color3: " + String.format("#%02x%02x%02x", color3.getRed(), color3.getBlue(), color3.getGreen()) + "\n";
		result += "  Inverted: " + inverted + "\n\n";
		return result;

	}

	@Override
	public Color getFragment(Vec4d value) {

	    if(inverted) {
	    	
	    	value.setX(1.0 - value.getX());
	    	
	    }
	    
	    Color result = color1;
	    
	    if(value.getX() > 0) {
	    	
	    	result = color1.interpolate(color2.interpolate(color3, value.getX() * value.getZ()), value.getX());
	    	
	    }
	    
	    return result;

	}

	@Override
	public boolean isInverted() {

		return inverted;

	}

	@Override
	public void setInverted(boolean inverted) {

		this.inverted = inverted;

	}

	@Override
	public Shader clone() {

		MinDistanceShader result = new MinDistanceShader();
		result.setColor1(new Color(color1.getRed(), color1.getGreen(), color1.getBlue(), color1.getAlpha()));
		result.setColor2(new Color(color2.getRed(), color2.getGreen(), color2.getBlue(), color2.getAlpha()));
		result.setColor3(new Color(color3.getRed(), color3.getGreen(), color3.getBlue(), color3.getAlpha()));
		result.setInverted(inverted);
		return result;

	}

	@Override
	public String toString() {

		return Strings.MIN_DISTANCE;

	}

}