package com.anchorwatchstudios.chaos.document.shader;

import com.anchorwatchstudios.chaos.math.Interpolation;

/**
 * A class representing RGBA color, implemented
 * with integers.
 * 
 * @author Anthony Atella
 */
public class Color implements Interpolation<Color> {

	private int red;
	private int green;
	private int blue;
	private int alpha;
	
	/**
	 * Constructs a new black color.
	 */
	public Color() {
		
		red = 0;
		green = 0;
		blue = 0;
		alpha = 255;
		
	}
	
	/**
	 * Constructs a new color with the given values.
	 * 
	 * @param red The red component
	 * @param green The green component
	 * @param blue The blue component
	 * @param alpha The alpha component
	 */
	public Color(int red, int green, int blue, int alpha) {
		
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = 255;
		
	}

	/**
	 * Returns the red value.
	 * 
	 * @return The red value
	 */
	public int getRed() {
		
		return red;
		
	}

	/**
	 * Sets the red value.
	 * 
	 * @param red The red value
	 */
	public void setRed(int red) {
		
		this.red = red;
		
	}

	/**
	 * Returns the green value.
	 * 
	 * @return The green value
	 */
	public int getGreen() {
		
		return green;
		
	}

	/**
	 * Sets the green value.
	 * 
	 * @param green The green value
	 */
	public void setGreen(int green) {
		
		this.green = green;
		
	}

	/**
	 * Returns the blue value.
	 * 
	 * @return The blue value
	 */
	public int getBlue() {
		
		return blue;
		
	}

	/**
	 * Sets the blue value.
	 * 
	 * @param blue The blue value
	 */
	public void setBlue(int blue) {
		
		this.blue = blue;
		
	}

	/**
	 * Returns the alpha value.
	 * 
	 * @return The alpha value
	 */
	public int getAlpha() {
		
		return alpha;
		
	}

	/**
	 * Sets the alpha value.
	 * 
	 * @param alpha The alpha value
	 */
	public void setAlpha(int alpha) {
		
		this.alpha = alpha;
		
	}

	@Override
	public Color interpolate(Color item, double percent) {

		int redDiff = item.getRed() - this.getRed();
		int greenDiff = item.getGreen() - this.getGreen();
		int blueDiff = item.getBlue() - this.getBlue();
		int alphaDiff = item.getAlpha() - this.getAlpha();
		double redOffset = redDiff * percent;
		double greenOffset = greenDiff * percent;
		double blueOffset = blueDiff * percent;
		double alphaOffset = alphaDiff * percent;
		int red = (int) (this.getRed() + redOffset);
		int green = (int) (this.getGreen() + greenOffset);
		int blue = (int) (this.getBlue() + blueOffset);
		int alpha = (int) (this.getAlpha() + alphaOffset);
		return new Color(red, green, blue, alpha);
		
	}
	
}
