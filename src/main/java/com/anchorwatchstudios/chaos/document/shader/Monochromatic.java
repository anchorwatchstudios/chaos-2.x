package com.anchorwatchstudios.chaos.document.shader;

/**
 * Represents anything with one color.
 * 
 * @author Anthony Atella
 */
public interface Monochromatic {

	/**
	 * Returns the color.
	 * 
	 * @return The color
	 */
	Color getColor1();
	
	/**
	 * Sets the color to the given color.
	 * 
	 * @param color The new color
	 */
	void setColor1(Color color);
	
}