package com.anchorwatchstudios.chaos.document.render;

import java.util.HashMap;

import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.jogl.gl4.program.GL4Program;

public class GL4RIM implements RenderImplMgr<GL4Program> {

	private HashMap<Class<?>, Class<?>> map;
	
	public GL4RIM() {
		
		map = new HashMap<Class<?>, Class<?>>();
		
	}
	
	@Override
	public void registerImpl(Class<? extends Fractal> fractalClass, Class<? extends GL4Program> object) {

		map.put(fractalClass, object);
		
	}

	@Override
	public GL4Program getImpl(Class<? extends Fractal> fractalClass) {

			try {
				
				Class<?> c = map.get(fractalClass);
				
				if(c != null) {
					
					return (GL4Program) map.get(fractalClass).newInstance();
					
				}
			
			} catch (InstantiationException | IllegalAccessException e) {

				e.printStackTrace();
			
			}
			
			return null;

	}

}
