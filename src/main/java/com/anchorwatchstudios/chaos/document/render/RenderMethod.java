package com.anchorwatchstudios.chaos.document.render;

import com.anchorwatchstudios.chaos.math.Interpolation;

/**
 * Represents a way to render something. This should not be confused
 * with the <code>RenderImplementation</code> enum, which represents
 * how something is rendered. You can think of <code>RenderMethod</code>
 * as an interface and <code>RenderImplementation</code> as an implementation
 * to that interface. For example, the complex plane is a way to draw fractals,
 * but it can be done in Java2D or OpenGL4.
 * 
 * @author Anthony Atella
 */
public interface RenderMethod extends Interpolation<RenderMethod>, Cloneable {

	/**
	 * Returns a list of <code>RenderImplementation</code>s compatible with this
	 * <code>RenderMethod</code>. The first one in the list should be the default.
	 * 
	 * @return A list of <code>RenderImplementation</code>s compatible with this <code>RenderMethod</code>
	 */
	public RenderImplementation[] getCapabilities();
	
	/**
	 * Returns a text description of this <code>RenderMethod</code>
	 * and its components.
	 * 
	 * @return A text description of this <code>RenderMethod</code> and its components
	 */
	public String getDescription();
	
	/**
	 * Creates a copy of this <code>RenderMethod</code>.
	 * 
	 * @return A copy of this <code>RenderMethod</code>
	 */
	public RenderMethod clone();
	
}
