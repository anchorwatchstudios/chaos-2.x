package com.anchorwatchstudios.chaos.document.render;

/**
 * An enum describing what implementation to use when rendering.
 * Not to be confused with <code>RenderMethod</code>, which describes
 * which way something is rendered but not how, like an interface.
 * 
 * @author Anthony Atella
 */
public enum RenderImplementation {

	JAVA2D,
	OPENGL4;

	@Override
	public String toString() {

		switch(this) {

		case JAVA2D:

			return "Java 2D";

		case OPENGL4:

			return "OpenGL4";

		default:

			return "";

		}

	}

}
