package com.anchorwatchstudios.chaos.document.render;

import java.util.HashMap;

import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.KeyFrame;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.swing.Chaos;
import com.anchorwatchstudios.chaos.swing.Workspace;
import com.anchorwatchstudios.chaos.swing.task.J2DTask;

public class J2DRIM implements RenderImplMgr<J2DTask> {

	private Chaos context;
	private HashMap<Class<?>, Class<?>> map;
	
	public J2DRIM(Chaos context) {
		
		map = new HashMap<Class<?>, Class<?>>();
		this.context = context;
		
	}
	
	@Override
	public void registerImpl(Class<? extends Fractal> fractalClass, Class<? extends J2DTask> object) {

		map.put(fractalClass, object);
		
	}

	@Override
	public J2DTask getImpl(Class<? extends Fractal> fractalClass) {

		try {
			
			J2DTask task = (J2DTask) map.get(fractalClass).newInstance();
			FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();
			
			if(task != null && document != null) {
			
				KeyFrame keyframe = document.getCurrentKeyFrame();
				task.setRenderMethod(keyframe.getRenderMethod());
				task.setFractal(keyframe.getFractal());
				task.setShader(keyframe.getShader());
				task.setSettings(document.getSettings());
				Workspace workspace = context.getWindow().getWorkspace();
				task.setCanvas(workspace.getCurrentCanvas().getJ2DCanvas());
				return task;
				
			}
		
		} catch (InstantiationException | IllegalAccessException e) {

			e.printStackTrace();
			
		}
		
		return null;

	}

}
