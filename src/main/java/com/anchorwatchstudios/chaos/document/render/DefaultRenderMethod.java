package com.anchorwatchstudios.chaos.document.render;

/**
 * An empty render method for when there are no options
 * to choose. Mainly a convenience class to avoid null
 * checks and make GSON happy when parsing.
 * 
 * @author Anthony Atella
 *
 */
public class DefaultRenderMethod implements RenderMethod {
	
	@Override
	public RenderImplementation[] getCapabilities() {

		return new RenderImplementation[] { RenderImplementation.JAVA2D };
		
	}
	
	@Override
	public String getDescription() {

		return "Default Render Method: \n\n";

	}

	@Override
	public RenderMethod clone() {

		return new DefaultRenderMethod();
	
	}

	@Override
	public RenderMethod interpolate(RenderMethod item, double percent) {
	
		if(item instanceof DefaultRenderMethod || percent < 0.5) {
		
			return this.clone();
		
		}
		else {
		
			return item.clone();
			
		}
		
	}

}
