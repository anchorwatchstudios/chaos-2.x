package com.anchorwatchstudios.chaos.document;

import java.io.File;

/**
 * The base interface for all documents. This interface
 * utilizes the observer pattern to alert observers of
 * changes in the <code>Document</code>.
 * 
 * @author Anthony Atella
 */
public interface Document {

	/**
	 * Returns the <code>File</code> that contains
	 * this <code>Document</code>'s location.
	 * 
	 * @return The <code>File</code> that contains
	 * this <code>Document</code>'s location
	 */
	File getFile();
	
	/**
	 * Sets the <code>File</code> that contains
	 * this <code>Document</code>'s location.
	 * 
	 * @param file The <code>File</code> that contains
	 * this <code>Document</code>'s location
	 */
	void setFile(File file);

	/**
	 * Returns whether or not this <code>Document</code>
	 * has been edited.
	 * 
	 * @return true if this <code>Document</code> has been edited
	 */
	boolean isEdited();
	
	/**
	 * Sets whether or not this <code>Document</code> is edited.
	 * 
	 * @param edited Whether or not this <code>Document</code> is edited
	 */
	void setEdited(boolean edited);
	
	/**
	 * Registers an observer to be alerted when the document has been
	 * edited.
	 * 
	 * @param documentObserver The <code>DocumentObserver</code> to be
	 * registered
	 */
	void registerObserver(DocumentObserver documentObserver);
	
	/**
	 * Unregisters a <code>DocumentObserver</code> with this <code>Document</code>.
	 * 
	 * @param documentObserver The <code>DocumentObserver</code> to be
	 * unregistered
	 */
	void unregisterObserver(DocumentObserver documentObserver);
	
	/**
	 * Notifies all registered <code>DocumentObserver</code>s that
	 * this <code>Document</code> has been edited.
	 */
	void notifyObservers();
	
	/**
	 * Returns a description of the elements within this <code>Document</code>.
	 * 
	 * @return A description of the elements within this <code>Document</code>
	 */
	String getDescription();
	
}