package com.anchorwatchstudios.chaos.math;

/**
 * A four-dimensional vector with double precision.
 * 
 * @author Anthony Atella
 */
public class Vec4d implements Cloneable, Interpolation<Vec4d> {

	protected double[] data;
	
	/**
	 * Constructs a new zero vector.
	 */
	public Vec4d() {
		
		data = new double[4];
		
	}
	
	/**
	 * Constructs a new vector with the given values.
	 * 
	 * @param x The x component
	 * @param y The y component
	 * @param z The z component
	 * @param w The w component
	 */
	public Vec4d(double x, double y, double z, double w) {
		
		data = new double[] { x, y, z, w };
		
	}
	
	/**
	 * Returns the x component of this vector.
	 * 
	 * @return The x component of this vector
	 */
	public double getX() {
		
		return data[0];
		
	}
	
	/**
	 * Sets the x component of this vector.
	 * 
	 * @param x The new x component
	 */
	public void setX(double x) {
		
		data[0] = x;
		
	}
	
	/**
	 * Returns the y component of this vector.
	 * 
	 * @return The y component of this vector
	 */
	public double getY() {
		
		return data[1];
		
	}
	
	/**
	 * Sets the y component of this vector.
	 * 
	 * @param y The new y component
	 */
	public void setY(double y) {
		
		data[1] = y;
		
	}
	
	/**
	 * Returns the z component of this vector.
	 * 
	 * @return The z component of this vector
	 */
	public double getZ() {
		
		return data[2];
		
	}
	
	/**
	 * Sets the z component of this vector.
	 * 
	 * @param z The new z component
	 */
	public void setZ(double z) {
		
		data[2] = z;
		
	}
	
	/**
	 * Returns the w component of this vector.
	 * 
	 * @return The w component of this vector
	 */
	public double getW() {
		
		return data[3];
		
	}
	
	/**
	 * Sets the w component of this vector.
	 * 
	 * @param w The new w component
	 */
	public void setW(double w) {
		
		data[3] = w;
		
	}
	
	/**
	 * Normalizes this vector
	 * 
	 * @return A normalized vector
	 */
	public Vec4d normalize() {
		
		double length = length();
		return new Vec4d(data[0] / length, data[1] / length, data[2] / length, data[3] / length);
		
	}
	
	/**
	 * Returns the dot product of this vector and the given vector.
	 * 
	 * @param vec The vector to be dotted with
	 * @return A new vector
	 */
	public double dot(Vec4d vec) {
		
		double result = 0;
		result += getX() * vec.getX();
		result += getY() * vec.getY();
		result += getZ() * vec.getZ();
		result += getW() * vec.getW();
		return result;
		
	}
	
	/**
	 * Returns the length of this vector.
	 * 
	 * @return The length of this vector
	 */
	public double length() {
		
		return Math.sqrt(dot(this));
		
	}
	
	/**
	 * Multiplies this vector by a scalar.
	 * 
	 * @param scale The scalar
	 * @return A new vector
	 */
	public Vec4d scalar(double scale) {
		
		return new Vec4d(data[0] * scale, data[1] * scale, data[2] * scale, data[3] * scale);
		
	}

	/**
	 * Adds the given vector to this vector.
	 * 
	 * @param vec A vector
	 * @return A new vector
	 */
	public Vec4d add(Vec4d vec) {

		Vec4d result = new Vec4d();
		result.setX(data[0] + vec.getX());
		result.setY(data[1] + vec.getY());
		result.setZ(data[2] + vec.getZ());
		result.setW(data[3] + vec.getW());
		return result;
		
	}

	/**
	 * Subtracts the given vector from this vector.
	 * 
	 * @param vec A vector
	 * @return A new vector
	 */
	public Vec4d subtract(Vec4d vec) {

		Vec4d result = new Vec4d();
		result.setX(data[0] - vec.getX());
		result.setY(data[1] - vec.getY());
		result.setZ(data[2] - vec.getZ());
		result.setW(data[3] - vec.getW());
		return result;
		
	}
	
	@Override
	public Vec4d clone() {
		
		Vec4d result = new Vec4d();
		result.setX(getX());
		result.setY(getY());
		result.setZ(getZ());
		result.setW(getW());
		return result;
		
	}

	@Override
	public Vec4d interpolate(Vec4d item, double percent) {
		
		Vec4d result = new Vec4d();
		result.setX(Interpolation.interpolate(getX(), item.getX(), percent));
		result.setY(Interpolation.interpolate(getY(), item.getY(), percent));
		result.setZ(Interpolation.interpolate(getZ(), item.getZ(), percent));
		result.setW(Interpolation.interpolate(getW(), item.getW(), percent));
		return result;

	}
	
	@Override
	public String toString() {
		
		return "<" + String.format("%.2f", data[0]) + ", " + String.format("%.2f", data[1]) + ", " + String.format("%.2f", data[2]) + ", " + String.format("%.2f", data[3]) + ">";
		
	}
	
	@Override
	public boolean equals(Object o) {
		
		if(o instanceof Vec4d) {
			
			Vec4d v = (Vec4d) o;
			
			if(data[0] == v.getX() && data[1] == v.getY() && data[2] == v.getZ() && data[3] == v.getW()) {
				
				return true;
				
			}
			
		}
		
		return false;
		
	}
	
}
