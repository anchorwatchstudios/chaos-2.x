package com.anchorwatchstudios.chaos.math;

/**
 * Represents a point in quaternion space. A quaternion has four numbers,
 * a, b, c, and d. It can be thought of as a combination of complex numbers,
 * or as a scalar component and a 3 component vector. Like the complex numbers,
 * quaternions have even numbers of real and imaginary numbers. This is why we
 * can think of them as a set of complex numbers.
 * 
 * @author Anthony Atella
 */
public class Quaternion implements Interpolation<Quaternion> {

	private double a, b, c, d;
	
	/**
	 * Constructs a new 0 <code>Quaternion</code>.
	 */
	public Quaternion() {
	
		a = 0;
		b = 0;
		c = 0;
		d = 0;
		
	}
	
	/**
	 * Constructs a new <code>Quaternion</code> with the given values.
	 * 
	 * @param a The a component
	 * @param b The b component
	 * @param c The c component
	 * @param d The d component
	 */
	public Quaternion(double a, double b, double c, double d) {
		
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	
	}
	
	/**
	 * Returns the a component of this <code>Quaternion</code>.
	 * 
	 * @return The a component of this <code>Quaternion</code>
	 */
	public double getA() {
		
		return a;
		
	}

	/**
	 * Sets the a component of this <code>Quaternion</code>.
	 * 
	 * @param a The a component of this <code>Quaternion</code>
	 */
	public void setA(double a) {
		
		this.a = a;
		
	}

	/**
	 * Returns the b component of this <code>Quaternion</code>.
	 * 
	 * @return The b component of this <code>Quaternion</code>
	 */
	public double getB() {
		
		return b;
		
	}

	/**
	 * Sets the b component of this <code>Quaternion</code>.
	 * 
	 * @param b The b component of this <code>Quaternion</code>
	 */
	public void setB(double b) {
		
		this.b = b;
		
	}

	/**
	 * Returns the c component of this <code>Quaternion</code>.
	 * 
	 * @return The c component of this <code>Quaternion</code>
	 */
	public double getC() {
		
		return c;
		
	}

	/**
	 * Sets the c component of this <code>Quaternion</code>.
	 * 
	 * @param c The c component of this <code>Quaternion</code>
	 */
	public void setC(double c) {
		
		this.c = c;
		
	}

	/**
	 * Returns the d component of this <code>Quaternion</code>.
	 * 
	 * @return The d component of this <code>Quaternion</code>
	 */
	public double getD() {
		
		return d;
		
	}

	/**
	 * Sets the d component of this <code>Quaternion</code>.
	 * 
	 * @param d The d component of this <code>Quaternion</code>
	 */
	public void setD(double d) {
		
		this.d = d;
		
	}

	/**
	 * Adds this <code>Quaternion</code> to the given <code>Quaternion</code>.
	 * 
	 * @param quat The <code>Quaternion</code> to be added
	 * @return A new <code>Quaternion</code>
	 */
	public Quaternion add(Quaternion quat) {
	
		return new Quaternion(a + quat.a, b + quat.b, c + quat.c, d + quat.d);
	
	}
	
	/**
	 * Subtracts the given <code>Quaternion</code> from this <code>Quaternion</code>.
	 * 
	 * @param quat The <code>Quaternion</code> to be subtracted
	 * @return A new <code>Quaternion</code>
	 */
	public Quaternion subtract(Quaternion quat) {
	
		return new Quaternion(a - quat.a, b - quat.b, c - quat.c, d - quat.d);
	
	}
	
	/**
	 * Multiplies this <code>Quaternion</code> by the given <code>Quaternion</code>.
	 * 
	 * @param quat The <code>Quaternion</code> to be multiplied
	 * @return A new <code>Quaternion</code>
	 */
	public Quaternion multiply(Quaternion quat) {
		
		double x = a * quat.a - b * quat.b - c * quat.c - d * quat.d;
		double y = a * quat.b + b * quat.a + c * quat.d - d * quat.c;
		double z = a * quat.c - b * quat.d + c * quat.a + d * quat.b;
		double w = a * quat.d + b * quat.c - c * quat.b + d * quat.a;
		return new Quaternion(x, y, z, w);
		
	}
	
	/**
	 * Divides this <code>Quaternion</code> by the given <code>Quaternion</code>.
	 * 
	 * @param quat The <code>Quaternion</code> to be divided by
	 * @return A new <code>Quaternion</code>
	 */
	public Quaternion divide(Quaternion quat) {
		
		return multiply(quat.pow(-1));
		
	}
	
	/**
	 * Raises this <code>Quaternion</code> to the given power.
	 * 
	 * @param n The power
	 * @return A new <code>Quaternion</code>
	 */
 	public Quaternion pow(int n) {
		
		Quaternion result = new Quaternion(a, b, c, d);
		
		for(int i = 1;i < n;i++) {
			
			result = result.multiply(this);
			
		}
		
		return result;
		
	}
	
 	/**
 	 * Returns the length of this <code>Quaternion</code>.
 	 * 
 	 * @return The length of this <code>Quaternion</code>
 	 */
	public double abs() {
		
		return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2) + Math.pow(c, 2) + Math.pow(d, 2));
		
	}
	
	@Override
    public String toString() {
    	
    	return a + " + " + b + "i" + " + " + c + "j" + " + " + d + "k";
    	
    }

	@Override
	public Quaternion interpolate(Quaternion quat, double percent) {
		
		Quaternion result = new Quaternion();
		result.setA(Interpolation.interpolate(a, quat.getA(), percent));
		result.setB(Interpolation.interpolate(b, quat.getB(), percent));
		result.setC(Interpolation.interpolate(c, quat.getC(), percent));
		result.setD(Interpolation.interpolate(d, quat.getD(), percent));
		return result;
		
	}
	
}
