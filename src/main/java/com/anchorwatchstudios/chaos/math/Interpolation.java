package com.anchorwatchstudios.chaos.math;

/**
 * Interpolation interface for objects and static convenience methods for
 * linear interpolation of primitives.
 * 
 * @author Anthony Atella
 *
 * @param <T> The type to interpolate with and return
 */
public interface Interpolation<T> {

	/**
	 * Interpolates this object to another by a given
	 * percent.
	 * 
	 * @param item The object to interpolate to
	 * @param percent The amount to interpolate, from 0 to 1
	 * @return A new interpolated object
	 */
	T interpolate(T item, double percent);
	
	/**
	 * Linearly interpolates two integers by the given percent.
	 * 
	 * @param item1 The starting integer
	 * @param item2 The ending integer
	 * @param percent The percent to interpolate
	 * @return A new interpolated integer
	 */
	public static int interpolate(int item1, int item2, double percent) {
		
		int difference = item2 - item1;
		double offset = percent * difference;
		return (int) (item1 + offset);
		
	}
	
	/**
	 * Linearly interpolates two float by the given percent.
	 * 
	 * @param item1 The starting float
	 * @param item2 The ending float
	 * @param percent The percent to interpolate
	 * @return A new interpolated float
	 */
	public static float interpolate(float item1, float item2, double percent) {
		
		float difference = item2 - item1;
		double offset = percent * difference;
		return (float) (item1 + offset);
		
	}
	
	/**
	 * Linearly interpolates two doubles by the given percent.
	 * 
	 * @param item1 The starting double
	 * @param item2 The ending double
	 * @param percent The percent to interpolate
	 * @return A new interpolated double
	 */
	public static double interpolate(double item1, double item2, double percent) {
		
		double difference = item2 - item1;
		double offset = percent * difference;
		return item1 + offset;
		
	}
	
	/**
	 * Linearly interpolates two arrays of doubles by the given percent.
	 * 
	 * @param item1 The starting double array
	 * @param item2 The ending double array
	 * @param percent The percent to interpolate each element
	 * @return A new interpolated double array
	 */
	public static double[] interpolate(double[] item1, double[] item2, double percent) {
		
		double[] result = new double[item1.length];
		
		for(int i = 0;i < item1.length;i++) {
		
			double difference = item2[i] - item1[i];
			double offset = percent * difference;
			result[i] = item1[i] + offset;
		
		}
		
		return result;
		
	}
	
}