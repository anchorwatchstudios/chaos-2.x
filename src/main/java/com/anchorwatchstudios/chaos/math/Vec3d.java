package com.anchorwatchstudios.chaos.math;

/**
 * A three-dimensional vector with double precision.
 * 
 * @author Anthony Atella
 */
public class Vec3d implements Cloneable, Interpolation<Vec3d> {

	protected double[] data;
	
	/**
	 * Constructs a new zero vector.
	 */
	public Vec3d() {
		
		data = new double[3];
		
	}
	
	/**
	 * Constructs a new vector with the given values.
	 * 
	 * @param x The x component
	 * @param y The y component
	 * @param z The z component
	 */
	public Vec3d(double x, double y, double z) {
		
		data = new double[] { x, y, z };
		
	}
	
	/**
	 * Returns the x component of this vector.
	 * 
	 * @return The x component of this vector
	 */
	public double getX() {
		
		return data[0];
		
	}
	
	/**
	 * Sets the x component of this vector.
	 * 
	 * @param x The new x component
	 */
	public void setX(double x) {
		
		data[0] = x;
		
	}
	
	/**
	 * Returns the y component of this vector.
	 * 
	 * @return The y component of this vector
	 */
	public double getY() {
		
		return data[1];
		
	}
	
	/**
	 * Sets the y component of this vector.
	 * 
	 * @param y The new y component
	 */
	public void setY(double y) {
		
		data[1] = y;
		
	}
	
	/**
	 * Returns the z component of this vector.
	 * 
	 * @return The z component of this vector
	 */
	public double getZ() {
		
		return data[2];
		
	}
	
	/**
	 * Sets the z component of this vector.
	 * 
	 * @param z The new z component
	 */
	public void setZ(double z) {
		
		data[2] = z;
		
	}
	
	/**
	 * Normalizes this vector
	 * 
	 * @return A normalized vector
	 */
	public Vec3d normalize() {
		
		double length = length();
		return new Vec3d(data[0] / length, data[1] / length, data[2] / length);
		
	}
	
	/**
	 * Crosses this vector with the given vector.
	 * 
	 * @param vec The vector to be crossed with
	 * @return A new vector
	 */
	public Vec3d cross(Vec3d vec) {
		
		Vec3d result = new Vec3d();
		result.setX(getY() * vec.getZ() - vec.getY() * getZ());
		result.setY(getZ() * vec.getX() - vec.getZ() * getX());
		result.setZ(getX() * vec.getY() - vec.getX() * getY());
		return result;
		
	}
	
	/**
	 * Returns the dot product of this vector and the given vector.
	 * 
	 * @param vec The vector to be dotted with
	 * @return A new vector
	 */
	public double dot(Vec3d vec) {
		
		double result = 0;
		result += getX() * vec.getX();
		result += getY() * vec.getY();
		result += getZ() * vec.getZ();
		return result;
		
	}
	
	/**
	 * Returns the length of this vector.
	 * 
	 * @return The length of this vector
	 */
	public double length() {
		
		return Math.sqrt(dot(this));
		
	}
	
	/**
	 * Multiplies this vector by a scalar.
	 * 
	 * @param scale The scalar
	 * @return A new vector
	 */
	public Vec3d scalar(double scale) {
		
		return new Vec3d(data[0] * scale, data[1] * scale, data[2] * scale);
		
	}

	/**
	 * Adds the given vector to this vector.
	 * 
	 * @param vec A vector
	 * @return A new vector
	 */
	public Vec3d add(Vec3d vec) {

		Vec3d result = new Vec3d();
		result.setX(data[0] + vec.getX());
		result.setY(data[1] + vec.getY());
		result.setZ(data[2] + vec.getZ());
		return result;
		
	}

	/**
	 * Subtracts the given vector from this vector.
	 * 
	 * @param vec A vector
	 * @return A new vector
	 */
	public Vec3d subtract(Vec3d vec) {

		Vec3d result = new Vec3d();
		result.setX(data[0] - vec.getX());
		result.setY(data[1] - vec.getY());
		result.setZ(data[2] - vec.getZ());
		return result;
		
	}
	
	@Override
	public Vec3d clone() {
		
		Vec3d result = new Vec3d();
		result.setX(getX());
		result.setY(getY());
		result.setZ(getZ());
		return result;
		
	}

	@Override
	public Vec3d interpolate(Vec3d item, double percent) {
		
		Vec3d result = new Vec3d();
		result.setX(Interpolation.interpolate(getX(), item.getX(), percent));
		result.setY(Interpolation.interpolate(getY(), item.getY(), percent));
		result.setZ(Interpolation.interpolate(getZ(), item.getZ(), percent));
		return result;

	}
	
	@Override
	public String toString() {
		
		return "<" + String.format("%.2f", data[0]) + ", " + String.format("%.2f", data[1]) + ", " + String.format("%.2f", data[2]) + ">";
		
	}
	
	@Override
	public boolean equals(Object o) {
		
		if(o instanceof Vec3d) {
			
			Vec3d v = (Vec3d) o;
			
			if(data[0] == v.getX() && data[1] == v.getY() && data[2] == v.getZ()) {
				
				return true;
				
			}
			
		}
		
		return false;
		
	}
	
}
