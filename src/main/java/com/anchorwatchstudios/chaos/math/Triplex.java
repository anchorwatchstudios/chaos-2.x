package com.anchorwatchstudios.chaos.math;

/**
 * Represents a hypercomplex number in three dimensions.
 * 
 * @author Anthony Atella
 */
public class Triplex implements Interpolation<Triplex>{

	private double a, b, c;
	
	/**
	 * Constructs a new 0 <code>Triplex</code>.
	 */
	public Triplex() {
		
		a = 0;
		b = 0;
		c = 0;
		
	}
	
	/**
	 * Constructs a new <code>Triplex</code> with the given
	 * values.
	 * 
	 * @param a The a component
	 * @param b The b component
	 * @param c The c component
	 */
	public Triplex(double a, double b, double c) {
		
		this.a = a;
		this.b = b;
		this.c = c;
		
	}
	
	/**
	 * Returns the a component of this <code>Triplex</code>.
	 * 
	 * @return The a component of this <code>Triplex</code>
	 */
	public double getA() {
		
		return a;
		
	}

	/**
	 * Sets the a component of this <code>Triplex</code>.
	 * 
	 * @param a The a component of this <code>Triplex</code>
	 */
	public void setA(double a) {
		
		this.a = a;
		
	}

	/**
	 * Returns the b component of this <code>Triplex</code>.
	 * 
	 * @return The b component of this <code>Triplex</code>
	 */
	public double getB() {
		
		return b;
		
	}

	/**
	 * Sets the b component of this <code>Triplex</code>.
	 * 
	 * @param b The b component of this <code>Triplex</code>
	 */
	public void setB(double b) {
		
		this.b = b;
		
	}

	/**
	 * Returns the c component of this <code>Triplex</code>.
	 * 
	 * @return The c component of this <code>Triplex</code>
	 */
	public double getC() {
		
		return c;
		
	}

	/**
	 * Sets the c component of this <code>Triplex</code>.
	 * 
	 * @param c The c component of this <code>Triplex</code>
	 */
	public void setC(double c) {
		
		this.c = c;
		
	}

 	/**
 	 * Returns the length of this <code>Triplex</code>.
 	 * 
 	 * @return The length of this <code>Triplex</code>
 	 */
	public double abs() {
	
		return Math.sqrt(a * a + b * b + c * c);
		
	}
	
	/**
	 * Adds this <code>Triplex</code> to the given <code>Quaternion</code>.
	 * 
	 * @param t The <code>Triplex</code> to be added
	 * @return A new <code>Triplex</code>
	 */
	public Triplex add(Triplex t) {
	
		return new Triplex(a + t.getA(), b + t.getB(), c + t.getC());
		
	}
	
	/**
	 * Subtracts the given <code>Triplex</code> from this <code>Triplex</code>.
	 * 
	 * @param t The <code>Triplex</code> to be subtracted
	 * @return A new <code>Triplex</code>
	 */
	public Triplex subtract(Triplex t) {
		
		return new Triplex(a - t.getA(), b - t.getB(), c - t.getC());
		
	}
	
	/**
	 * Multiplies this triplex number by another.
	 * 
	 * @param t A triplex number
	 * @return The product of this triplex and the given triplex
	 */
	public Triplex multiply(Triplex t) {
		
		double rho1 = abs();
		double theta1 = Math.asin(c / rho1);
		double phi1 = Math.atan2(b, a);

		double rho2 = t.abs();
		double theta2 = Math.asin(t.getC() / rho2);
		double phi2 = Math.atan2(t.getB(), t.getA());

		double thetaSum = theta1 + theta2;
		double phiSum = phi1 + phi2;
		double cosThetaSum = Math.cos(thetaSum);
		
		double x = Math.cos(phiSum) * cosThetaSum;
		double y = Math.sin(phiSum) * cosThetaSum;
		double z = Math.sin(thetaSum);
		Triplex result = new Triplex(x, y, z);
		result.scalar(rho1 * rho2);
		return result;
		
	}
	
	/**
	 * Divides this triplex number by another.
	 * 
	 * @param t The denominator
	 * @return The quotient of this triplex and a given triplex
	 */
	public Triplex divide(Triplex t) {
		
		return multiply(t.reciprocal());
		
	}
	
	public Triplex reciprocal() {
		
		return new Triplex(1.0 / a, 1.0 / b, 1.0 / c);
		
	}
	
	/**
	 * Multiplies this <code>Triplex</code> by a scalar value.
	 * 
	 * @param s The scalar
	 * @return A new <code>Triplex</code> number scaled to the given
	 * value
	 */
	public Triplex scalar(double s) {
		
		return new Triplex(a * s, b * s, c * s);
		
	}
	
	/**
	 * Raises this <code>Triplex</code> to the given power.
	 * 
	 * @param n The power
	 * @return A new <code>Triplex</code>
	 */
	public Triplex pow(double n) {
		
		// Convert to spherical coordinates
		double rho = abs();
		double theta = Math.asin(c / rho);
		double phi = Math.atan2(b, a);
	    // Scale and rotate the point
	    rho = Math.pow(rho, n);
	    theta *= n;
	    phi *= n;
	    // Raise the vector t to the nth power using White and Nylander's formula
	    double cosTheta = Math.cos(theta);
	    return new Triplex(cosTheta * Math.cos(phi), cosTheta * Math.sin(phi), Math.sin(theta)).scalar(rho);
	    
	}
	
	/**
	 * Raises this <code>Triplex</code> to the given power.
	 * Length is a parameter here because it is convenient
	 * to calculate it elsewhere sometimes.
	 * 
	 * @param n The power
	 * @param r The length of this <code>Triplex</code>
	 * @return A new <code>Triplex</code>
	 */
	public Triplex pow(double n, double r) {
		
		// Convert to spherical coordinates
		double theta = Math.asin(c / r);
		double phi = Math.atan2(b, a);
	    // Scale and rotate the point
	    double rho = Math.pow(r, n);
	    theta *= n;
	    phi *= n;
	    // Raise the vector t to the nth power using White and Nylander's formula
	    double cosTheta = Math.cos(theta);
	    return new Triplex(cosTheta * Math.cos(phi), cosTheta * Math.sin(phi), Math.sin(theta)).scalar(rho);
	    
	}

	@Override
	public Triplex interpolate(Triplex item, double percent) {

		double a = Interpolation.interpolate(this.a, item.getA(), percent);
		double b = Interpolation.interpolate(this.b, item.getB(), percent);
		double c = Interpolation.interpolate(this.c, item.getC(), percent);
		return new Triplex(a, b, c);

	}
	
	@Override
	public String toString() {
		
		return a + " + " + b + "i + " + c + "j";
		
	}
	
}
