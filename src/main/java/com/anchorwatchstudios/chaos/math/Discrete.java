package com.anchorwatchstudios.chaos.math;

/**
 * Represents something that can be either
 * discrete or continuous.
 * 
 * @author Anthony Atella
 */
public interface Discrete {

	static final boolean DEFAULT = false;
	
	/**
	 * Returns whether or not this object
	 * is discrete.
	 * 
	 * @return true if the object is discrete
	 */
	boolean getDiscrete();
	
	/**
	 * Sets whether or not this object
	 * is discrete.
	 * 
	 * @param discrete Whether or not this object
	 * is discrete.
	 */
	void setDiscrete(boolean discrete);
	
}
