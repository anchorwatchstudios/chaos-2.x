package com.anchorwatchstudios.chaos.task;

import java.util.ArrayList;

/**
 * A <code>Task</code> with priority that can be compared to others.
 * 
 * @author Anthony Atella
 */
public abstract class PriorityTask implements Task, Priority {

	private int priority;
	private ArrayList<TaskObserver> observers;

	/**
	 * Constructs a new <code>PriorityTask</code> with priority 0.
	 */
	public PriorityTask() {

		priority = 0;
		observers = new ArrayList<TaskObserver>();

	}

	@Override
	public int getPriority() {

		return priority;

	}

	@Override
	public void setPriority(int priority) {

		this.priority = priority;

	}

	@Override
	public int compareTo(Priority o) {

		if(priority < o.getPriority()) {

			return -1;

		}
		else if(priority > o.getPriority()) {

			return 1;

		}
		else {

			return 0;

		}

	}

	@Override
	public void registerObserver(TaskObserver observer) {

		observers.add(observer);
		
	}

	@Override
	public void unregisterObserver(TaskObserver observer) {

		observers.remove(observer);
		
	}

	@Override
	public void notifyObservers() {

		for(int i = 0;i < observers.size();i++) {
			
			observers.get(i).notify(this);
			
		}
		
	}
	
	/**
	 * Compares two <code>task</code>s and returns the higher priority
	 * <code>Task</code>.
	 * 
	 * @param task1 The first <code>Task</code>
	 * @param task2 The second <code>Task</code>
	 * @return The higher priority <code>Task</code>
	 */
	public static Task compare(Task task1, Task task2) {

		Task result = task2;

		if(task2 instanceof PriorityTask) {

			if(task1 instanceof PriorityTask) {

				PriorityTask a = (PriorityTask) task2;
				PriorityTask b = (PriorityTask) task1;

				if(a.compareTo(b) == 1) {

					result = task2;

				}

			}
			else {

				result = task2;

			}

		}
		else if(!(task1 instanceof PriorityTask)) {

			result = task2;

		}

		return result;

	}
	
}