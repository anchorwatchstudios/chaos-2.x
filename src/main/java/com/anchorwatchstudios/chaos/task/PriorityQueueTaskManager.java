package com.anchorwatchstudios.chaos.task;

import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * An abstract <code>TaskManager</code> that implements a <code>PriorityQueue</code>.
 * 
 * @author Anthony Atella
 */
public abstract class PriorityQueueTaskManager implements TaskManager {

	protected PriorityQueue<Task> tasks;
	private ArrayList<TaskManagerObserver> observers;
	
	/**
	 * Constructs a new <code>PriorityQueueTaskManager</code>.
	 */
	public PriorityQueueTaskManager() {
		
		tasks = new PriorityQueue<Task>();
		observers = new ArrayList<TaskManagerObserver>();
		
	}
	
	@Override
	public void addTask(Task task) {

		tasks.add(task);
		task.registerObserver(this);
		
	}

	@Override
	public void removeTask(Task task) {

		tasks.remove(task);
		task.stop();
		task.unregisterObserver(this);
		
	}

	@Override
	public void removeAll() {

		for(int i = 0;i < tasks.size();i++) {
			
			Task task = tasks.remove();
			task.stop();
			task.unregisterObserver(this);
			
		}
		
	}
	
	@Override
	public void registerObserver(TaskManagerObserver observer) {

		observers.add(observer);
		
	}

	@Override
	public void unregisterObserver(TaskManagerObserver observer) {

		observers.remove(observer);
		
	}

	@Override
	public void notifyObservers() {

		for(int i = 0;i < observers.size();i++) {
			
			observers.get(i).notify(this);
			
		}
		
	}

}
