package com.anchorwatchstudios.chaos.task;

import java.util.ArrayList;

/**
 * Manages <code>Task</code>s in multiple ways. Capable of single, synchronous, and asynchronous
 * <code>Task</code> handling simultaneously.
 * 
 * @author Anthony Atella
 *
 */
public class MultiTaskManager implements TaskManager, TaskManagerObserver {

	public static final int SINGLE = 0;
	public static final int SYNC = 1;
	public static final int ASYNC = 2;
	private SyncTaskManager syncManager;
	private AsyncTaskManager asyncManager;
	private SingleTaskManager singleManager;
	private ArrayList<TaskManagerObserver> observers;
	
	/**
	 * Constructs a new <code>MultiTaskManager</code>.
	 */
	public MultiTaskManager() {
		
		syncManager = new SyncTaskManager();
		asyncManager = new AsyncTaskManager();
		singleManager = new SingleTaskManager();
		syncManager.registerObserver(this);
		asyncManager.registerObserver(this);
		singleManager.registerObserver(this);
		observers = new ArrayList<TaskManagerObserver>();
		
	}

	/**
	 * Adds a <code>Task</code> of the given type.
	 * 
	 * @param task The <code>Task</code> to be added
	 * @param type The type of task. Possible values are SINGLE,
	 * SYNC, and ASYNC.
	 */
	public void addTask(Task task, int type) {
		
		notifyObservers();
		
		switch(type) {
		
		case SINGLE:
			
			singleManager.addTask(task);
			break;
			
		case SYNC:
			
			syncManager.addTask(task);
			break;
			
		case ASYNC:
			
			asyncManager.addTask(task);
			break;
			
		default:
			
			singleManager.addTask(task);
			break;
		
		}
		
	}
	
	@Override
	public void addTask(Task task) {

		singleManager.addTask(task);
		notifyObservers();
		
	}

	@Override
	public void removeTask(Task task) {

		syncManager.removeTask(task);
		asyncManager.removeTask(task);
		singleManager.removeTask(task);
		
	}

	@Override
	public void removeAll() {

		syncManager.removeAll();
		asyncManager.removeAll();
		singleManager.removeAll();
		
	}

	@Override
	public void notify(Task task) {
		
		notifyObservers();
		
	}

	@Override
	public Task getCurrent() {

		Task result = null;
		Task s = syncManager.getCurrent();
		Task a = asyncManager.getCurrent();
		Task single = asyncManager.getCurrent();
		
		if(s != null) {
			
			result = s;
			
			if(a != null) {
				
				result = PriorityTask.compare(s, a);
				
			}
			
			if(single != null) {
				
				result = PriorityTask.compare(result, single);
				
			}
			
		}
		
		return result;

	}

	@Override
	public void registerObserver(TaskManagerObserver observer) {

		observers.add(observer);
		
	}

	@Override
	public void unregisterObserver(TaskManagerObserver observer) {

		observers.remove(observer);
		
	}

	@Override
	public void notifyObservers() {

		for(int i = 0;i < observers.size();i++) {
			
			observers.get(i).notify(this);
			
		}
		
	}

	@Override
	public void notify(TaskManager taskManager) {

		notify(taskManager.getCurrent());
		
	}
	
}
