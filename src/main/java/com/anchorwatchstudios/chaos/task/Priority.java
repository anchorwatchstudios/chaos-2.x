package com.anchorwatchstudios.chaos.task;

/**
 * A comparable integer interface.
 * 
 * @author Anthony Atella
 *
 */
public interface Priority extends Comparable<Priority> {

	/**
	 * Returns the priority.
	 * 
	 * @return The priority
	 */
	int getPriority();
	
	/**
	 * Sets the priority
	 * 
	 * @param priority The priority
	 */
	void setPriority(int priority);
	
}
