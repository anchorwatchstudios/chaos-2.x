package com.anchorwatchstudios.chaos.task;

/**
 * Manages tasks synchronously. Each <code>Task</code> is run on a thread.
 * When a <code>Task</code> completes, a new <code>Task</code> is run until
 * no more are left.
 * 
 * @author Anthony Atella
 *
 */
public class SyncTaskManager extends PriorityQueueTaskManager {

	private Task current;
	
	/**
	 * Constructs a new <code>SyncTaskManager</code>.
	 */
	public SyncTaskManager() {
		
		current = null;
		
	}

	@Override
	public Task getCurrent() {

		return current;
		
	}

	@Override
	public void addTask(Task task) {
		
		super.addTask(task);

		if(current == null) {
			
			spawnThread();
			
		}
		
	}
	
	@Override
	public void removeTask(Task task) {
		
		super.removeTask(task);
		
		if(current != null && current.equals(task)) {
			
			current.stop();
			current.unregisterObserver(this);
			spawnThread();
			
		}
		
	}
	
	@Override
	public void removeAll() {
		
		super.removeAll();
		current.stop();
		current.unregisterObserver(this);
		current = null;
		
	}
	
	@Override
	public void notify(Task task) {

		notifyObservers();

		if(task.getProgress() == 1.0) {
			
			task.unregisterObserver(this);
			spawnThread();
			
		}
				
	}
	
	/**
	 * Spawns a single thread to run a <code>Task</code>.
	 */
	private void spawnThread() {
		
		if(tasks.size() > 0) {
			
			current = tasks.remove();
			new Thread(current).start();
			
		}
		else {
			
			current = null;

		}
		
	}
	
}