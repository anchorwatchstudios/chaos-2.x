package com.anchorwatchstudios.chaos.task;

/**
 * Observes a <code>Task</code> for progress.
 * 
 * @author Anthony Atella
 */
public interface TaskObserver {

	/**
	 * Notifies this <code>TaskObserver</code> that the
	 * given <code>Task</code> has made progress.
	 * 
	 * @param task The task that made progress
	 */
	void notify(Task task);
	
}
