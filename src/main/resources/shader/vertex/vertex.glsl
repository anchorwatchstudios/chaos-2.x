#version 410

/**
 * Simple pass-through vertex shader.
 *
 * @author Anthony Atella
 */
attribute vec3 position;

/**
 * The main entry point. Passes the input position.
 */
void main() {
    gl_Position = vec4(position, 1.0);
}
