/**
 * Quaternion number operations.
 *
 * @author Anthony Atella
 */

/**
 * Returns the product of two given quaternions.
 *
 * @param q1 A quaternion number
 * @param q2 A quaternion number
 * @return The product of the two given quaternions
 */
vec4 quaternionMultiply(vec4 q1, vec4 q2) {
	float x = q1.x * q2.x - q1.y * q2.y - q1.z * q2.z - q1.w * q2.w;
	float y = q1.x * q2.y + q1.y * q2.x + q1.z * q2.w - q1.w * q2.z;
	float z = q1.x * q2.z - q1.y * q2.w + q1.z * q2.x + q1.w * q2.y;
	float w = q1.x * q2.w + q1.y * q2.z - q1.z * q2.y + q1.w * q2.x;
	return vec4(x, y, z, w);
}
