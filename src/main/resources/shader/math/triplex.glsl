/**
 * Triplex number operations.
 *
 * @author Anthony Atella
 */

/**
 * Multiplies a given triplex number by another.
 *
 * @param t1 The first triplex number
 * @param t2 The second triplex number
 * @return The product of the two given triplex numbers
 */
vec3 triplexMultiply(vec3 t1, vec3 t2) {
	float rho1 = length(t1);
    float theta1 = asin(t1.z / rho1);
    float phi1 = atan(t1.y, t1.x);

	float rho2 = length(t2);
    float theta2 = asin(t2.z / rho2);
    float phi2 = atan(t2.y, t2.x);

	float thetaSum = theta1 + theta2;
	float phiSum = phi1 + phi2;
	float cosThetaSum = cos(thetaSum);

	float x = cos(phiSum) * cosThetaSum;
	float y = sin(phiSum) * cosThetaSum;
	float z = sin(thetaSum);
	return rho1 * rho2 * vec3(x, y, z);
}

/**
 * Divides a given triplex number by another.
 *
 * @param t1 The numerator
 * @param t2 The denominator
 * @return The quotient of the two given triplex numbers
 */
vec3 triplexDivide(vec3 t1, vec3 t2) {
	return triplexMultiply(t1, vec3(1.0) / t2);
}

/**
 * Raises a given triplex number to the given power.
 * Based on the White and Nylander formula.
 *
 * @param t The triplex number
 * @param n The power to raise the triplex to
 * @return A new triplex number
 */
vec3 triplexPower(vec3 t, float n) {
	float rho = length(t);
    float theta = asin(t.z / rho);
    float phi = atan(t.y, t.x);

    rho = pow(rho, n);
    theta *= n;
    phi *= n;

    float cosTheta = cos(theta);
    return rho * vec3(cosTheta * cos(phi), cosTheta * sin(phi), sin(theta));
}

/**
 * Raises a given triplex number to the given power.
 * Based on the White and Nylander formula. The length
 * is supplied as an argument here even though we can
 * derive it from the given triplex because it is
 * convenient to compute it elsewhere.
 *
 * @param t The triplex number
 * @param n The power to raise the triplex to
 * @param r The length of the triplex number (radius from origin)
 * @return A new triplex number
 */
vec3 triplexPower(vec3 t, float n, float r) {
    float theta = asin(t.z / r);
    float phi = atan(t.y, t.x);

    float rho = pow(r, n);
    theta *= n;
    phi *= n;

    float cosTheta = cos(theta);
    return rho * vec3(cosTheta * cos(phi), cosTheta * sin(phi), sin(theta));
}
