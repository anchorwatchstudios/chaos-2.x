/**
 * Renders the Mandelbox.
 *
 * @author Anthony Atella
 */
#define FRACTAL_ITERATIONS 25
uniform vec3 c;
uniform float fractalThreshold = 2.0;
uniform float fixedRadius = 1.0;
uniform float minRadius = 0.5;
uniform float foldLimit = 1.0;
uniform float scale = 2.0;

vec3 boxFold(vec3 z, float foldLimit) {
	return clamp(z, -foldLimit, foldLimit) * (2.0 * foldLimit) - z;
}

vec3 sphereFold(vec3 z, float fixedRadius, float minRadius, inout float dz) {
	float r2 = dot(z, z);
	if (r2 < minRadius) {
		// linear inner scaling
		float temp = (fixedRadius / minRadius);
		z *= temp;
		dz *= temp;
	} else if (r2 < fixedRadius) {
		// this is the actual sphere inversion
		float temp = (fixedRadius / r2);
		z *= temp;
		dz *= temp;
	}
	return z;
}

vec4 mandelbox(vec3 c, float fixedRadius, float minRadius, float foldLimit, float scale, float threshold) {
	float dz = 1.0;
	vec3 z = vec3(0.0);
	float thresholdSquared = threshold * threshold;
	float minDistance = 1000.0;
	float iterations = 0.0;

    for (int i = 0; i < int(FRACTAL_ITERATIONS); i++) {
    	iterations = i;
    	z = scale * sphereFold(boxFold(z, foldLimit), fixedRadius, minRadius, dz) + c;
    	dz = dz * abs(scale) + 1.0;
    	minDistance = min(minDistance, dot(z, z));

    	if(length(z) > thresholdSquared) {
    		break;
    	}
    }

	return vec4(iterations / float(FRACTAL_ITERATIONS), length(z) / abs(dz), minDistance, 0.33 * log(dot(z, z)) + 1.0);
}

/**
 * Returns the distance a ray has to travel to reach a bounding geometry
 * at the origin.
 *
 * @param position The position of the point
 * @return The distance from the given point to the geometry
 */
vec4 getDistance(vec3 position) {
	return mandelbox(position, fixedRadius, minRadius, foldLimit, scale, fractalThreshold);
}
