/**
 * Renders the Newton knot.
 *
 * @author Anthony Atella
 */
#define FRACTAL_ITERATIONS 25
uniform vec3 alpha;
uniform vec3 n;
uniform float fractalThreshold;

vec4 newtonKnot(vec3 z, vec3 alpha, vec3 n, float threshold) {
	float thresholdSquared = threshold * threshold;
	float result = 0.0;
	vec3 one = vec3(1.0, 0.0, 0.0);
	float minDistance = 1000.0;

	for (int i = 0; i < int(FRACTAL_ITERATIONS); i++) {
		vec3 numerator = triplexPower(z, n.x);
		numerator += one;
		numerator = triplexMultiply(numerator, alpha);
		vec3 denominator = triplexPower(z, (n - one).x);
		denominator = triplexMultiply(denominator, n);
		vec3 oldZ = z;
		z = triplexDivide(numerator, denominator);
		float l = length(z - oldZ);
		minDistance = min(minDistance, l);

		if(l <= thresholdSquared) {
			break;
		}

		result++;
	}
	return vec4(result / float(FRACTAL_ITERATIONS), length(z), minDistance, 0.33 * log(dot(z, z)) + 1.0);
}

/**
 * Returns the distance a ray has to travel to reach a bounding geometry
 * at the origin.
 *
 * @param position The position of the point
 * @return The distance from the given point to the geometry
 */
vec4 getDistance(vec3 position) {
	return newtonKnot(position, alpha, n, fractalThreshold);
}
