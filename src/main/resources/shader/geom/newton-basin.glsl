/**
 * Renders the Newton basin.
 *
 * @author Anthony Atella
 */
#define FRACTAL_ITERATIONS 25
uniform vec2 alpha;
uniform vec2 n;
uniform float fractalThreshold;

/**
 * Returns a vector containing data about the point.
 *
 * @param point The complex point to check
 * @return A vector: vec4(iterations, distance, minDistance, magnitude)
 */
vec4 isInSet(vec2 z) {

	float result = 0.0;
	vec2 one = vec2(1.0, 0.0);
	float minDistance = 1000.0;

	for(int i = 0;i < int(FRACTAL_ITERATIONS);i++) {

		vec2 numerator = complexPower(z, n.x);
		numerator += one;
		numerator = complexMultiply(numerator, alpha);
		vec2 denominator = complexPower(z, (n - one).x);
		denominator = complexMultiply(denominator, n);
		vec2 oldZ = z;
		z = complexDivide(numerator, denominator);
		float l = length(z - oldZ);
		minDistance = min(minDistance, l);

		if(l <= pow(fractalThreshold, 2.0)) {

			break;

		}

		result++;

	}

	float length = length(z);
	return vec4(result / float(FRACTAL_ITERATIONS), length, minDistance, 0.33 * log(length * length) + 1.0);

}
