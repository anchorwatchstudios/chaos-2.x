/**
 * Renders the Mandelbulb.
 *
 * @author Anthony Atella
 */
#define FRACTAL_ITERATIONS 25
uniform vec3 z;
uniform float n = 8.0;
uniform float fractalThreshold;
uniform bool discrete;

/**
 * The Mandelbulb distance function. This method uses the
 * White and Nylander formula to calculate the square of
 * a triplex number, and calculates the derivative at each
 * step so it can be compared to the radius at the end of
 * the algorithm. Returns a number between 0 and 1 that
 * represents a percentage of how many iterations it took
 * to get the answer.
 *
 * @param z A triplex constant
 * @param c The triplex point
 * @param n The power to raise z to
 * @param threshold The maximum of the length of z before
 * it is considered out of the set
 * @return A number between 0 and 1
 */
vec4 mandelbulb(vec3 z, vec3 c, float n, float threshold) {
	float dr = 1.0;
	float r  = length(z);
	float iterations = 0.0;
	float minDistance = 1000.0;
	for(int i = 0;i < int(FRACTAL_ITERATIONS); i++) {
		iterations++;
	    dr *= pow(r, n - 1.0) * n + 1.0;
		z = triplexPower(z, n, r);
		z += c;
		r = length(z);
		minDistance = min(minDistance, r);
		if(r >= pow(threshold, 2.0)) {
			break;
		}
	}

	return vec4(iterations / float(FRACTAL_ITERATIONS), 0.5 * log(r) * r / dr, minDistance, 0.33 * log(dot(z, z)) + 1.0);
}

/**
 * Returns the distance a ray has to travel to reach a bounding geometry
 * at the origin.
 *
 * @param position The position of the point
 * @return The distance from the given point to the geometry
 */
vec4 getDistance(vec3 position) {
	return mandelbulb(z, position, n, fractalThreshold);
}
