/**
 * Renders the Julia set.
 *
 * @author Anthony Atella
 */
#define FRACTAL_ITERATIONS 25
uniform bool discrete;
uniform vec2 c;
uniform float fractalThreshold;

/**
 * Returns the precise number of iterations from
 * a point and a discrete result.
 *
 * @param c The point
 * @param result The discrete iteration results
 * @return The precise iterations
 */
float getPreciseIterations(vec2 c, float result) {
	float log_zn = log(length(c)) / 2.0;
	float nu = log(log_zn / log(2.0)) / log(2.0);
	return result + 1.0 - nu;
}

/**
 * Returns a vector containing data about the point.
 *
 * @param point The complex point to check
 * @return A vector: vec4(iterations, distance, minDistance, magnitude)
 */
vec4 isInSet(vec2 z) {
	float result = 0.0;
	float minDistance = 1000.0;

	for(int i = 0;i < int(FRACTAL_ITERATIONS); i++) {
		z = complexPower(z, 2.0) + c;
		float l = length(z);
		minDistance = min(minDistance, l);

		if(l > pow(fractalThreshold, 2.0)) {
			break;
		}

		result++;
	}

	float length = length(z);

	if(!discrete && result < float(FRACTAL_ITERATIONS)) {
		result = getPreciseIterations(z, result);
	}

	return vec4(result / float(FRACTAL_ITERATIONS), length, minDistance, 0.33 * log(length * length) + 1.0);
}
