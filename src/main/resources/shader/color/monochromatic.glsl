/**
 * Monochromatic fragment shader. Returns the
 * color times the given value.
 *
 * @author Anthony Atella
 */
uniform vec4 color1;
uniform bool inverted;

/**
 * Returns a color from a vec4(iterations, distance, minDistance, magnitude).
 *
 * @param value A vector: vec4(iterations, distance, minDistance, magnitude)
 * @return vec4 A color
 */
vec4 getColor(vec4 value) {
	if(inverted) {
		value.x = 1.0 - value.x;
	}
	return color1 * value.x;
}
