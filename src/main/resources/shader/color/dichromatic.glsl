/**
 * Dichromatic fragment shader. Returns the
 * first color if the value is 0, otherwise
 * returns the second color times the given
 * value.
 *
 * @author Anthony Atella
 */
uniform vec4 color1;
uniform vec4 color2;
uniform bool inverted;

/**
 * Returns a color from a vec4(iterations, distance, minDistance, magnitude).
 *
 * @param value A vector: vec4(iterations, distance, minDistance, magnitude)
 * @return vec4 A color
 */
vec4 getColor(vec4 value) {
	if(inverted) {
		value.x = 1.0 - value.x;
	}
	if(value.x < 1.0) {
		return vec4(color1.rgb * value.x, color1.a);
	}
	return color2;
}
