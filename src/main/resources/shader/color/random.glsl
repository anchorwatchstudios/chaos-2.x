/**
 * Random fragment shader. Returns a
 * pseudo-random color for each fragment.
 *
 * @author Anthony Atella
 */
uniform bool inverted;

/**
 * Returns a pseudo-random number from the
 * given gl_FragCoord.
 *
 * @param fragCoord The gl_FragCoord
 * @return A pseudo-random number
 */
float rand(vec2 fragCoord) {
    highp float a = 12.9898;
    highp float b = 78.233;
    highp float c = 43758.5453;
    highp float dt = dot(fragCoord.xy ,vec2(a,b));
    highp float sn = mod(dt,3.14);
    return fract(sin(sn) * c);
}

/**
 * Returns a color from a vec4(iterations, distance, minDistance, magnitude).
 *
 * @param value A vector: vec4(iterations, distance, minDistance, magnitude)
 * @return vec4 A color
 */
vec4 getColor(vec4 value) {
	if(inverted) {
		value.x = 1.0 - value.x;
	}
	float seed1 = rand(gl_FragCoord.xy);
	float seed2 = rand(vec2(value.x, seed1));
	float seed3 = rand(vec2(seed1, seed2));
	return vec4(value.x * seed1, value.x * seed2, value.x * seed3, 1.0);
}
