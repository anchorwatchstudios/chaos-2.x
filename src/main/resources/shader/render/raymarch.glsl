#version 410
precision highp float;

/**
 * Raymarcher. Renders geometry using raymarching.
 *
 * @author Anthony Atella
 */
#define MARCH_ITERATIONS 25
uniform float rayThreshold;
uniform vec2 resolution;
uniform vec3 cameraOrigin;
uniform vec3 cameraLookAt;
uniform vec3 cameraUp;

/**
 * Returns the distance a ray has to travel to reach a bounding geometry
 * at the origin.
 *
 * @param position The position of the point
 * @return A vector: vec4(iterations, distance, minDistance, magnitude)
 */
vec4 getDistance(vec3 position);

/**
 * Returns a color from a vec4(iterations, distance, minDistance, magnitude).
 *
 * @param value A vector: vec4(iterations, distance, minDistance, magnitude)
 * @return vec4 A color
 */
vec4 getColor(vec4 value);

/**
 * Shoots a ray through the current pixel from the position of the camera and
 * returns the inverse of the percentage of steps it took to reach the surface of the scene
 * geometry. The ray "marches" by growing a little each step.
 *
 * @param vec3 position The position of the camera
 * @param vec3 direction The direction of the ray
 * @param float threshold The minimum distance a ray can be from the geometry before touching
 * @return vec4 A vector: vec4(iterations, distance, minDistance, magnitude)
 */
vec4 rayMarch(vec3 position, vec3 direction, float threshold) {
	int result = 0;
	vec4 dist;
	for(int i = 0;i < int(MARCH_ITERATIONS);i++) {
		dist = getDistance(position);
		if(dist.y < threshold) {
			break;
		}
		position += dist.y * direction;
		result++;
	}
	return vec4(1.0 - float(result) / float(MARCH_ITERATIONS), dist.y, dist.z, dist.w);
}

/**
 * Returns the normalized right facing vector from the
 * camera.
 *
 * @param vec3 lookAt The camera look at vector
 * @param vec3 up The camera up direction
 * @return vec3 The normalized right facing vector from the camera
 */
vec3 getCameraRight(vec3 lookAt, vec3 up) {
	return normalize(cross(lookAt, up));
}

/**
 * Returns the direction of a ray shot from the camera through a given pixel.
 *
 * @param vec2 cameraSpaceCoords The pixel coordinates in camera space
 * @param vec3 cameraOrigin The location of the camera
 * @param vec3 cameraLookAt The target of the camera
 * @param vec3 cameraUp The up direction of the camera
 * @return vec3 The normalized direction of the ray to be shot through the given pixel
 */
vec3 getDirection(vec2 cameraSpaceCoords, vec3 cameraOrigin, vec3 cameraLookAt, vec3 cameraUp) {
	vec3 rightVector = getCameraRight(cameraLookAt, cameraUp);
	vec3 cameraDirection = normalize(cameraLookAt);
	vec3 upVector = normalize(cameraUp);
	return normalize(rightVector * cameraSpaceCoords.x + upVector * cameraSpaceCoords.y + cameraDirection);
}

/**
 * Returns the current fragments coordinates in screen coordinates,
 * respecting aspect ratio.
 *
 * @param vec2 fragCoords The current fragment
 * @param vec2 resolution The image resolution
 * @return vec2 The fragment, in screen coordinates
 */
vec2 getScreenCoords(vec2 fragCoords, vec2 resolution) {
	vec2 result = fragCoords / resolution;
	result.x *= resolution.x / resolution.y;
	return result - vec2(0.5 * (resolution.x / resolution.y), 0.5);
}

/**
 * The main entry point. Renders geometry with raymarching.
 */
void main() {
	vec2 screenCoords = getScreenCoords(gl_FragCoord.xy, resolution);
	vec3 rayDirection = getDirection(screenCoords, cameraOrigin, cameraLookAt, cameraUp);
	vec4 result = rayMarch(cameraOrigin, rayDirection, rayThreshold);
	gl_FragColor = getColor(result);
}
