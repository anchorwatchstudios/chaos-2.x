/**
 * Renders a box with a sphere inside of it.
 */
float sphere(vec3 pos, float radius) {
	return length(pos) - radius;
}

float box(vec3 pos, vec3 size) {
	return length(max(abs(pos) - size, 0.0));
}

float gizmo(vec3 pos, vec3 size) {
	float boxx = box(pos, size);
	float spheree = sphere(pos, size.x + 0.3);
	return min(boxx, spheree);
}

float getDistance(vec3 position, float size) {
	return gizmo(position, vec3(size));
}
